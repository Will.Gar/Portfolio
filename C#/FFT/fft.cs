﻿using System;
using System.Numerics;
using System.Globalization;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra;
using System.Drawing;
using System.Windows;
using System.Drawing.Imaging;
using System.Windows.Forms;

namespace fft
{
	class MainClass : System.Windows.Forms.Form
	{
		private PictureBox picture;
		private PictureBox picturePad;
		private PictureBox picturePhase;
		private PictureBox pictureMag;
		private TextBox textBox;
		private Bitmap bmp;
		private Bitmap bmpPad;
		private Bitmap bmpFFTMag;
		private Bitmap bmpFFTPhase;
		private Button button1;
		private Button button2;
		private Button button3;
		private Button button4;
		private Button button5;
		public static Vector<Complex> FFT(double[] data)
		{
			//Vector<Complex> FFTVar;
			//x = np.asarray(x, dtype = complex)
			Complex[] data2 = ToC32(data);
			Vector<Complex> x = Vector<Complex>.Build.DenseOfArray(data2);
			//N = x.shape[0]
			int N = x.Count;

			//if np.log2(N) % 1 > 0:
			if (Math.Log(N, 2) % 1 > 0)
			{

				//    raise ValueError("size of x must be a power of 2")
			}


			//#N_min is stopping condition for eq

			//N_min = min(N, 32)
			int N_min = Math.Min(N, 32);
			//n = np.arange(N_min)
			//k = n[:, None]
			Complex[,] M_Data = new Complex[N_min, N_min];
			for (int i = 0; i < N_min; i++)
			{
				for (int k = 0; k < N_min; k++)
				{
					M_Data[i, k] = (Complex)i * k;
				}
			}
			Matrix<Complex> M = Matrix<Complex>.Build.DenseOfArray(M_Data);
			M *= -2 * Complex.ImaginaryOne * (Complex)Math.PI;
			M /= N_min;
			M = M.PointwiseExp();
			//M = np.exp(-2j * np.pi * n * k / N_min)
			//X = np.dot(M, x.reshape((N_min, -1)))
			//Vector<Complex> X = M.Multiply(x);
			//Complex[,] xArr = new Complex[N_min, (N / N_min)];
			Complex[,] xArr = Reshape(data2, N_min);

			Matrix<Complex> xMat = Matrix<Complex>.Build.DenseOfArray(xArr);
			Matrix<Complex> X = M.Multiply(xMat);
			//Console.WriteLine(X);
			Complex[] z_data;
			Vector<Complex> Z = Vector<Complex>.Build.Dense(1, (Complex)0);
			Matrix<Complex> XLeft;
			Matrix<Complex> XRight;
			Matrix<Complex> XTop, XBottom, factor;
			while (X.RowCount < N)
			{
				//Console.WriteLine(X.RowCount);
				XLeft = GetEven(X, 0);
				XRight = GetEven(X, 1);
				z_data = new Complex[X.RowCount];
				for (int i = 0; i < X.RowCount; i++)
				{
					z_data[i] = i;
				}
				Z = Vector<Complex>.Build.DenseOfArray(z_data);
				factor = Matrix<Complex>.Build.DenseOfColumnVectors(Z);
				for (int i = 0; i < XLeft.ColumnCount - 1; i++)
				{
					factor = factor.InsertColumn(i, Z);
				}
				factor *= -1 * Complex.ImaginaryOne * (Complex)Math.PI;
				factor /= X.RowCount;
				factor = factor.PointwiseExp();
				//Console.WriteLine(XRight);
				//Console.WriteLine(factor);
				XTop = XLeft + (XRight.PointwiseMultiply(factor));
				XBottom = XLeft - (XRight.PointwiseMultiply(factor));
				X = XTop.Stack(XBottom);
				//if (X.RowCount == 128)
				//{
				//  Console.WriteLine(XLeft + XRight.PointwiseMultiply(factor));
				//Console.Write();
				//      Console.Write("Here");
				//  }
			}
			Z = X.Column(0);


			//while X.shape[0] < N:

			//    #print ((X.shape[1] /2))
			//    X_even = X[:,:X.shape[1]//2]
			//    X_odd = X[:, X.shape[1] // 2:]
			//    factor = np.exp(-1j * np.pi * np.arange(X.shape[0]) / X.shape[0])[:, None]
			//    X = np.vstack([X_even + factor * X_odd, X_even - factor * X_odd])
			//return X.ravel()
			return Z;
		}
		public static Vector<Complex> FFT(Complex[] data2)
		{
			//Vector<Complex> FFTVar;
			//x = np.asarray(x, dtype = complex)
			//Complex[] data2 = ToC32(data);
			Vector<Complex> x = Vector<Complex>.Build.DenseOfArray(data2);
			//N = x.shape[0]
			int N = x.Count;

			//if np.log2(N) % 1 > 0:
			if (Math.Log(N, 2) % 1 > 0)
			{

				//    raise ValueError("size of x must be a power of 2")
			}


			//#N_min is stopping condition for eq

			//N_min = min(N, 32)
			int N_min = Math.Min(N, 32);
			//n = np.arange(N_min)
			//k = n[:, None]
			Complex[,] M_Data = new Complex[N_min, N_min];
			for (int i = 0; i < N_min; i++)
			{
				for (int k = 0; k < N_min; k++)
				{
					M_Data[i, k] = (Complex)i * k;
				}
			}
			Matrix<Complex> M = Matrix<Complex>.Build.DenseOfArray(M_Data);
			M *= -2 * Complex.ImaginaryOne * (Complex)Math.PI;
			M /= N_min;
			M = M.PointwiseExp();
			//M = np.exp(-2j * np.pi * n * k / N_min)
			//X = np.dot(M, x.reshape((N_min, -1)))
			//Vector<Complex> X = M.Multiply(x);
			//Complex[,] xArr = new Complex[N_min, (N / N_min)];
			Complex[,] xArr = Reshape(data2, N_min);

			Matrix<Complex> xMat = Matrix<Complex>.Build.DenseOfArray(xArr);
			Matrix<Complex> X = M.Multiply(xMat);
			//Console.WriteLine(X);
			Complex[] z_data;
			Vector<Complex> Z = Vector<Complex>.Build.Dense(1, (Complex)0);
			Matrix<Complex> XLeft;
			Matrix<Complex> XRight;
			Matrix<Complex> XTop, XBottom, factor;
			while (X.RowCount < N)
			{
				//Console.WriteLine(X.RowCount);
				XLeft = GetEven(X, 0);
				XRight = GetEven(X, 1);
				z_data = new Complex[X.RowCount];
				for (int i = 0; i < X.RowCount; i++)
				{
					z_data[i] = i;
				}
				Z = Vector<Complex>.Build.DenseOfArray(z_data);
				factor = Matrix<Complex>.Build.DenseOfColumnVectors(Z);
				for (int i = 0; i < XLeft.ColumnCount - 1; i++)
				{
					factor = factor.InsertColumn(i, Z);
				}
				factor *= -1 * Complex.ImaginaryOne * (Complex)Math.PI;
				factor /= X.RowCount;
				factor = factor.PointwiseExp();
				//Console.WriteLine(XRight);
				//Console.WriteLine(factor);
				XTop = XLeft + (XRight.PointwiseMultiply(factor));
				XBottom = XLeft - (XRight.PointwiseMultiply(factor));
				X = XTop.Stack(XBottom);
				//if (X.RowCount == 128)
				//{
				//  Console.WriteLine(XLeft + XRight.PointwiseMultiply(factor));
				//Console.Write();
				//      Console.Write("Here");
				//  }
			}
			Z = X.Column(0);


			//while X.shape[0] < N:

			//    #print ((X.shape[1] /2))
			//    X_even = X[:,:X.shape[1]//2]
			//    X_odd = X[:, X.shape[1] // 2:]
			//    factor = np.exp(-1j * np.pi * np.arange(X.shape[0]) / X.shape[0])[:, None]
			//    X = np.vstack([X_even + factor * X_odd, X_even - factor * X_odd])
			//return X.ravel()
			return Z;
		}
		//get Left(0) or Right Side(1) of Half of 2d Matrix that is divisible by 2
		public static Complex[,] FFT2D(Double[,] data)
		{
			Complex[,] dataC = ToC32(data);
			Matrix<Complex> matrD = Matrix<Complex>.Build.DenseOfArray(dataC);
			//FFT Pass1
			Matrix<Complex> matrDFFTP1 = Matrix<Complex>.Build.DenseOfRowVectors(FFT(matrD.Row(0).ToArray()));
			for (int i = 0; i < data.GetLength(0) - 1; i++)
			{
				matrDFFTP1 = matrDFFTP1.InsertRow(i + 1, FFT(matrD.Row(i + 1).ToArray()));
			}
			//FFT Pass2
			Matrix<Complex> matrDFFTP2 = Matrix<Complex>.Build.DenseOfColumnVectors(FFT(matrDFFTP1.Column(0).ToArray()));
			for (int i = 0; i < data.GetLength(1) - 1; i++)
			{
				matrDFFTP2 = matrDFFTP2.InsertColumn(i + 1, FFT(matrDFFTP1.Column((i + 1)).ToArray()));
			}
			return matrDFFTP2.ToArray();
		}
		public static Complex[,] FFT2D(Complex[,] dataC)
		{
			Matrix<Complex> matrD = Matrix<Complex>.Build.DenseOfArray(dataC);
			//FFT Pass1
			Matrix<Complex> matrDFFTP1 = Matrix<Complex>.Build.DenseOfRowVectors(FFT(matrD.Row(0).ToArray()));
			for (int i = 0; i < dataC.GetLength(0) - 1; i++)
			{
				matrDFFTP1 = matrDFFTP1.InsertRow(i + 1, FFT(matrD.Row(i + 1).ToArray()));
			}
			//FFT Pass2

			Matrix<Complex> matrDFFTP2 = Matrix<Complex>.Build.DenseOfColumnVectors(FFT(matrDFFTP1.Column(0).ToArray()));
			for (int i = 0; i < dataC.GetLength(1) - 1; i++)
			{
				matrDFFTP2 = matrDFFTP2.InsertColumn(i + 1, FFT(matrDFFTP1.Column(i + 1).ToArray()));
			}
			return matrDFFTP2.ToArray();
		}

		public static Matrix<Complex> GetEven(Matrix<Complex> data, int soide)
		{
			Complex[,] dataArr = data.ToArray();
			Complex[,] temp = new Complex[dataArr.GetLength(0), dataArr.GetLength(1) / 2];
			for (int i = 0; i < dataArr.GetLength(0); i++)
			{
				for (int k = 0; k < (dataArr.GetLength(1) / 2); k++)
				{
					temp[i, k] = dataArr[i, k + (soide * (dataArr.GetLength(1) / 2))];
				}
			}
			return Matrix<Complex>.Build.DenseOfArray(temp);
		}
		// Reshape array of  size x
		public static Complex[,] Reshape(Complex[] data, int x)
		{
			Complex[,] reshaped = new Complex[x, (data.Length / x)];
			int i = 0, k = 0;
			bool firstpass = true;
			if (x < data.Length)
			{
				while (i < data.Length)
				{
					if (!firstpass && i % (data.Length / x) == 0)
					{
						k++;
					}
					else if (firstpass)
					{
						firstpass = false;
					}
					reshaped[k, i % (data.Length / x)] = data[i];
					i++;
				}
			}
			else
			{
				for (int j = 0; j < data.Length; j++)
				{
					reshaped[j, 0] = data[j];
				}
			}

			return reshaped;
		}
		public static double[,] Reshape(double[] data, int x)
		{
			double[,] reshaped = new double[x, (data.Length / x)];
			int i = 0, k = 0;
			bool firstpass = true;
			if (x < data.Length)
			{
				while (i < data.Length)
				{
					if (!firstpass && i % (data.Length / x) == 0)
					{
						k++;
					}
					else if (firstpass)
					{
						firstpass = false;
					}
					reshaped[k, i % (data.Length / x)] = data[i];
					i++;
				}
			}
			else
			{
				for (int j = 0; j < data.Length; j++)
				{
					reshaped[j, 0] = data[j];
				}
			}

			return reshaped;
		}

		public static Complex[] ToC32(double[] data)
		{
			Complex[] data2 = new Complex[data.Length];

			for (int i = 0; i < data.Length; i++)
			{
				data2[i] = (Complex)data[i];
			}
			return data2;
		}

		public static Complex[,] ToC32(double[,] data)
		{
			Complex[,] data2 = new Complex[data.GetLength(0), data.GetLength(1)];

			for (uint i = 0; i < data.GetLength(0); i++)
			{
				for (uint k = 0; k < data.GetLength(1); k++)
				{


					data2[i, k] = (Complex)data[i, k];
				}
			}
			return data2;

		}
		//get image greyval
		public static double[,] GetImageGreyVal(Bitmap bmp1)
		{
			unsafe
			{
				double[,] greyVal = new double[bmp1.Width, bmp1.Height];
				int pixelSize = 3;
				BitmapData bmpData = bmp1.LockBits(new Rectangle(0, 0, bmp1.Width, bmp1.Height), ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
				for (int y = 0; y < bmp1.Height; y++)
				{
					byte* orow = (byte*)bmpData.Scan0 + (y * bmpData.Stride);
					for (int x = 0; x < bmp1.Width; x++)
					{
						greyVal[y, x] = System.Convert.ToDouble(orow[x * pixelSize]);
					}
				}



				bmp1.UnlockBits(bmpData);
				return greyVal;
			}
		}

		public static Bitmap makeImageFromGrey(double[,] data)
		{
			unsafe
			{
				Bitmap bmp1 = new Bitmap(data.GetLength(1), data.GetLength(0));
				//double[,] greyVal = new double[bmp.Width, bmp.Height];
				int pixelSize = 3;
				BitmapData bmpData = bmp1.LockBits(new Rectangle(0, 0, data.GetLength(1), data.GetLength(0)), ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
				for (int y = 0; y < bmp1.Height; y++)
				{
					byte* nrow = (byte*)bmpData.Scan0 + (y * bmpData.Stride);
					for (int x = 0; x < bmp1.Width; x++)
					{
						try
						{
							nrow[x * pixelSize] = System.Convert.ToByte(data[y, x]);
                            nrow[x * pixelSize + 1] = System.Convert.ToByte(data[y, x]);
                            nrow[x * pixelSize + 2] = System.Convert.ToByte(data[y, x]);
							
						}catch(Exception)
						{
							nrow[x * pixelSize] = System.Convert.ToByte(255);
                            nrow[x * pixelSize + 1] = System.Convert.ToByte(255);
							nrow[x * pixelSize + 2] = System.Convert.ToByte(255);
						}
					}
				}

				bmp1.UnlockBits(bmpData);
				return bmp1;
			}
		}
		//make enlarge image to base2 average fill sides
		public static double[,] nLogGreyPad(double[,] data)
		{
			int newB = (int)Math.Pow(2, (int)Math.Ceiling(Math.Log(Math.Max(data.GetLength(0), data.GetLength(1)), 2)));
			int avg = getAverage(data);
			double[,] newD = new double[newB, newB];
			for (int i = 0; i < newB; i++)
			{
				for (int k = 0; k < newB; k++)
				{
					if ((i < data.GetLength(1)) && (k < data.GetLength(0)))
					{
						newD[k, i] = data[k, i];
					}
					else
					{
						newD[k, i] = avg;
					}
				}
			}
			return newD;
		}




		//find average of array values
		public static int getAverage(double[,] data)
		{
			double pool = 0, count = 0;
			for (int i = 0; i < data.GetLength(0); i++)
			{
				for (int k = 0; k < data.GetLength(1); k++)
				{
					pool += data[i, k];
					count++;
				}
			}
			return (int)(pool / count);
		}


		//fftimage

		//fftshift
		public static Complex[,] fftShift(Complex[,] data)
		{
			Complex[,] sData = new Complex[data.GetLength(0), data.GetLength(1)];
			for (int i = 0; i < data.GetLength(0); i++)
			{
				int iM = (i + (data.GetLength(0) / 2)) % data.GetLength(0);
				for (int k = 0; k < data.GetLength(1); k++)
				{
					int kM = (k + data.GetLength(1) / 2) % data.GetLength(1);
						sData[iM, kM] = data[i, k];
                    
				}
			}
			return sData;
		}

		public static double[,] scaleData(double[,]data,double sMin,double sMax)
		{
			double min = getMin(data);
			double max = getMax(data);
            
			for (int i = 0; i < data.GetLength(0);i++)
			{
				for (int k = 0; k < data.GetLength(1);k++)
				{
					data[i, k] = (data[i, k] - min) * 1 / (max - min) * (sMax - sMin) + sMin;
				}
			}
			return data;
		}
		//display fft of image as bitmap
		public void theWorks()
		{
			double[,] data = GetImageGreyVal(bmpPad);
			Complex[,] dataC = fftShift(FFT2D(data));
			//Complex[,] dataC = (FFT2D(data));
			double[,] dataP = phaseCalc(dataC);
			double[,] dataM = scaleData(magCalc(dataC),0,255);

			textBox.Text = Matrix<Double>.Build.DenseOfArray(dataM).ToString();
			Console.WriteLine(textBox.Text);
			bmpFFTMag = makeImageFromGrey(dataM);

			bmpFFTPhase = makeImageFromGrey(dataP);
		}



		public static double[,] phaseCalc(Complex[,] data)
		{
			double[,] dataP = new double[data.GetLength(0), data.GetLength(1)];
			for (int i = 0; i < data.GetLength(0);i++)
			{
				for (int k = 0; k < data.GetLength(0);k++)
				{
					dataP[i, k] = Math.Atan2(data[i, k].Imaginary, data[i, k].Real)*180/Math.PI;
				}
                
			}
			return dataP;
		}
        public static double[,] magCalc(Complex[,] data)
		{
			double[,] dataM = new double[data.GetLength(0), data.GetLength(1)];
			for (int i = 0; i < data.GetLength(0);i++)
			{
				for (int k = 0; k < data.GetLength(1);k++)
				{
					dataM[i, k] = Math.Log(1+Math.Sqrt(Math.Pow(data[i, k].Real, 2) + Math.Pow(data[i,k].Imaginary,2)));

				}
			}
			return dataM;
		}
		public static double getMin(Double[,] data)
        {
            double min = data[0, 0];
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int k = 0; k < data.GetLength(1); k++)
                {
                    min = Math.Min(min, data[i, k]);
                }

            }
            return min;
        }
		public static double getMax(double[,] data)
        {
            double max = data[0, 0];
            for (int i = 0; i < data.GetLength(0); i++)
            {
                for (int k = 0; k < data.GetLength(1); k++)
                {
                    max = Math.Max(max, data[i, k]);
                }

            }
            return max;
        }

        public MainClass()
        {
            InitializeComponent();
        }


        //private int imageX, imageY;
        public void InitializeComponent()
        {

            //GreyScale Picture ------------
            //picture;
            bmp = new Bitmap(Image.FromFile("/home/northbot/Docs/imageProcAndVision/fft/banana.jpg"));
            MakeGrayscale3(bmp);
            //bmp.Palette = new System.Drawing.Imaging.ColorPalette()
            //Image bmp = Image.FromFile("/home/northbot/Docs/imageProcAndVision/fft/banana.jpg");
            //Form form = new Form();
            picture = new PictureBox
            {
                Image = bmp,
                Name = "POOP",
                Size = new System.Drawing.Size(bmp.Width, bmp.Height),
                Location = new System.Drawing.Point(200, 0),
                Visible = true
            };
            
            
            picture.MouseHover += new EventHandler(inPicture);
            picture.MouseEnter += new EventHandler(inPicture);
            picture.MouseCaptureChanged += new EventHandler(inPicture);

			//picture2sect




			//TextBox Block
			textBox = new TextBox
			{
				Parent = this,
				//Dock = DockStyle.Bottom,
				Multiline = true,
				Location = new System.Drawing.Point(0, 250),
				Size =new System.Drawing.Size(200,100),
                Text = bmp.GetPixel(100, 100).ToString()
            };
            
            
            //picture.Dock = DockStyle.Fill;
            
            //Button Block ---------------
            button1 = new Button
            {
                //Dock = DockStyle.Left,
				Location = new System.Drawing.Point(0, 0),
                Name = "button1",
                Size = new System.Drawing.Size(200, 50),
                Text = "Exit",
            };
            button1.Click += new EventHandler(onClick);
            
			//Button2 Block --------------
			button2 = new Button
            {
                //Dock = DockStyle.Left,
                Location = new System.Drawing.Point(0, 50),
                Name = "button2",
                Size = new System.Drawing.Size(200, 50),
                Text = "GetGreyValues",
            };
			button2.Click += new EventHandler(onClick2);

			button3 = new Button
            {
                //Dock = DockStyle.Left,
                Location = new System.Drawing.Point(0, 100),
                Name = "button3",
                Size = new System.Drawing.Size(200, 50),
                Text = "GetAverage",
            };
            button3.Click += new EventHandler(onClick3);
			button4 = new Button
            {
                //Dock = DockStyle.Left,
                Location = new System.Drawing.Point(0, 150),
                Name = "button4",
                Size = new System.Drawing.Size(200, 50),
				Text = "Log2Pad",
            };
            button4.Click += new EventHandler(onClick4);
			button5 = new Button
            {
                //Dock = DockStyle.Left,
				Location = new System.Drawing.Point(0, 200),
                Name = "button5",
                Size = new System.Drawing.Size(200,50),
                Text = "TheWorks",
            };
			button5.Click += new EventHandler(onClick5);


            this.Controls.Add(picture);
            this.Controls.Add(button1);
            this.Controls.Add(textBox);
			this.Controls.Add(button2);
			this.Controls.Add(button3);
			this.Controls.Add(button4);
			this.Controls.Add(button5);
			//form.Show();
        }

        void inPicture(object sender,EventArgs e)
        {

			textBox.Text = bmp.GetPixel(picture.PointToClient(Cursor.Position).X, picture.PointToClient(Cursor.Position).Y).ToString();
        }


        

        void onClick(object sender, EventArgs e)
        {
            Close();
        }
        void onClick2(object sender, EventArgs e)
		{
			textBox.Text = Matrix<Double>.Build.DenseOfArray(GetImageGreyVal(bmp)).Row(186).ToString();
		}
        void onClick3(object sender, EventArgs e)
		{
			textBox.Text = "AVERAGE:" + getAverage(GetImageGreyVal(bmp)).ToString();	
		}
		void onClick4(object sender, EventArgs e)
        {
            textBox.Text = "Log2Pad:" + getAverage(GetImageGreyVal(bmp)).ToString();
			bmpPad = makeImageFromGrey(nLogGreyPad(GetImageGreyVal(bmp)));
            picturePad = new PictureBox
            {
                Image = bmpPad,
                Name = "Biff",
                Size = new System.Drawing.Size(bmpPad.Width, bmpPad.Height),
				Location = new System.Drawing.Point((bmp.Width + picture.Location.X), picture.Location.Y),
                Visible = true
            };
			this.Controls.Add(picturePad);
        }
		void onClick5(object sender,EventArgs e)
		{
			theWorks();
			picturePhase = new PictureBox
			{
				Image = bmpFFTPhase,
				Name = "Splash",
				Size = new System.Drawing.Size(bmpFFTPhase.Width, bmpFFTPhase.Height),
				Location = new System.Drawing.Point(picture.Location.X, picture.Location.Y + bmpPad.Size.Height),
                Visible = true
            };
			this.Controls.Add(picturePhase);
			pictureMag = new PictureBox
            {
				Image = bmpFFTMag,
                Name = "Slam!",
				Size = new System.Drawing.Size(bmpFFTMag.Width, bmpFFTMag.Height),
				Location = new System.Drawing.Point((bmpFFTPhase.Width + picturePhase.Location.X), picturePhase.Location.Y),
                Visible = true
            };
			this.Controls.Add(pictureMag);
		}

        public static Bitmap MakeGrayscale3(Bitmap orig)
        {
            Bitmap newBt = new Bitmap(orig.Width, orig.Height);
            Graphics g = Graphics.FromImage(orig);
            ColorMatrix colorMatrix = new ColorMatrix(
                new float[][]{
                new float[] {.3f,.3f,.3f,0,0},
                new float[] {.59f,.59f,.59f,0,0},
                new float[] {.11f,.11f,.11f,0,0},
                new float[] {0,0,0,1,0},
                new float[] {0,0,0,0,1}
            });
            ImageAttributes attributes = new ImageAttributes();
            attributes.SetColorMatrix(colorMatrix);
            g.DrawImage(orig, new Rectangle(0, 0, orig.Width, orig.Height), 0, 0, orig.Width, orig.Height, GraphicsUnit.Pixel, attributes);
            g.Dispose();
            return newBt;
            
        }

        public static void Main(string[] args)
        {
            Application.Run(new MainClass());
        

            //double[,] data = { { 1.0, 2.0 }, { 3.0, 5.0 } };
            //Complex[] testy = { 1.0, 2.0, 3.0, 5.0 };
            //Complex[] testy = new Complex[256];
            //for (int i = 0; i < 256; i++)
            //{
            //  testy[i] = (Complex)i;
            //}
            //Double[] dannyTrejo = new double[256];
            //for (int i = 0; i < 256; i++)
            //{
            //  dannyTrejo[i] = i;
            //}
            //Complex[,] testo = Reshape(testy, 16);
            //double[,] dannyTrejo2= Reshape(dannyTrejo, 16);
            //Console.WriteLine(testo.GetLength(0));
            //Console.WriteLine(testo.GetLength(1));
            //Complex[,] data2 = ToC32(data);
            //Complex[] testo = ToC32(testy);
            //Vector<Complex> TestationData = Vector<Complex>.Build.Random(128);
            //Matrix<Complex> data3 = Matrix<Complex>.Build.DenseOfArray(data2);
            //Console.WriteLine(data3.ToArray());

            //Console.WriteLine(TestationData * data3);
            //Console.WriteLine(data3.PointwiseExp());
            //Console.WriteLine(FFT(testy));
            //Console.WriteLine(Matrix<Complex>.Build.DenseOfArray(FFT2D(testo)));
            //Console.WriteLine(Matrix<Complex>.Build.DenseOfArray(FFT2D(dannyTrejo2)));

        }
    }
}
