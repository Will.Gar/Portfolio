
//------------------------------------------------------------------------
//Sequin.cs - reads and translates binary sequencer input files to csv.file
//------------------------------------------------------------------------

using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

class Sequin
{
	public class Entry
		{
		public string Name {get;set;}
		public string Datatype {get;set;}
		public int Length {get;set;}
		}
	public class Field
		{
		public string Name {get;set;}
		public string Datatype {get;set;}
		public int ValInt {get;set;}
		public string ValStr {get;set;}
		public long ValLong {get;set;}
		}
	public static List<Entry> recordStruct = new List<Entry>();
	
	
	public static void recordEntry(string name,string datatype,int length)
	{
		recordStruct.Add(new Entry{Name = name,Datatype = datatype,Length = length});
	}
	
	public static void sentry(string nDL)
	{
		string[] temp = nDL.Split(',');
		recordEntry(temp[0],temp[1],Convert.ToInt32(temp[2]));
	}
	
	public static void runEntries()
	{
		string[] text = System.IO.File.ReadAllLines(@"struct\input.txt");
		foreach(string s1 in text)
		{
		sentry(s1);
		}
	}
	public static void Main()
	{
		runEntries();
		FileStream f = new FileStream(@"Input.cqin", FileMode.Open);
		BinaryReader r = new BinaryReader(f);
		
		List<List<Field>> records = new List<List<Field>>();
		
		while(true)
		{
			List<Field> record = new List<Field>();
			foreach(Entry e1 in recordStruct)
			{
				try
				{
					if(e1.Datatype == "int")
					{
						switch(e1.Length)
						{
						case 1:
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(r.Read())});;
							break;
						case 2:
							
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(r.ReadInt16())});
							//Console.WriteLine(record[(record.Count-1)].ValInt);
							//Console.ReadLine();
							break;
						case 4:
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(r.ReadInt32())});
							//Console.WriteLine(record[(record.Count-1)].ValInt);
							//Console.ReadLine();
							break;	
						default:
							Console.WriteLine("ERROR");
							break;	
						}
					}
					if(e1.Datatype == "string1")
					{	
						//byte[] by = new Byte[r.ReadByte()];
						record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype,ValInt = 0,ValStr = Convert.ToString(r.ReadChar())});
					}
					
					if(e1.Datatype == "long")
					{	
						byte[] by = r.ReadBytes(e1.Length);
						record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValLong= BitConverter.ToInt64(by,0)});
					}
					
					if(e1.Datatype == "string2")
					{	
						byte[] by = r.ReadBytes(8);
						record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype,ValInt = 0,ValStr = System.Text.Encoding.Default.GetString(by)});
					}
				}
				catch(EndOfStreamException)
				{
					records.Add(record);
					goto writeLoop;
				}	
			}
			records.Add(record);
		}
writeLoop:	
		StreamWriter sw = new StreamWriter("test.csv");
		string header = "";
		foreach(Entry e1 in recordStruct)
		{
			header += (e1.Name + ",");
		}
		sw.WriteLine(header);
		foreach(List<Field> rec in records)
		{ 
			foreach(Field f1 in rec){
				switch(f1.Datatype)
				{
					case "int":
						sw.Write(f1.ValInt+",");
						break;
					case "string1":
						sw.Write(f1.ValStr+",");
						break;
					case "string2":
						sw.Write(f1.ValStr+",");
						break;
					case "long":
						sw.Write(f1.ValLong+",");
						break;
					default:
						Console.WriteLine("Error");
						break;
				}
			}
			sw.WriteLine();
		}
	}
}
