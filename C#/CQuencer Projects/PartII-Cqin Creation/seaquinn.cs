//SeaQuinn- Convert Csv to cqin file
using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;


	

class SeaQuinn
{

	public class Entry
		{
		public string Name {get;set;}
		public string Datatype {get;set;}
		public int Length {get;set;}
		}
	public class Field
		{
		public string Name {get;set;}
		public string Datatype {get;set;}
		public int ValInt {get;set;}
		public string ValStr {get;set;}
		public long ValLong {get;set;}
		public short ValShort{get;set;}
		public byte ValByte{get;set;}
		}
	public class Record
	{
		private List <Field> _Fields = new List<Field>();
		public List <Field> Fields {get{return _Fields;}set{_Fields = value;}}
		private List <byte[]> _RecBytes = new List<byte[]>();
		public List <byte[]> RecBytes {get{return _RecBytes;}set{_RecBytes = value;}}
	}
	public static List<Entry> recordStruct = new List<Entry>();
	public static List<string> csvList = new List<string>();
	public static List<Record> records = new List<Record>();
	public static string csvPath = "Connor.csv";
	public static string cqinPath = "input.cqin";
	
	public static void Main()
	{
		entropy();
		popuRec();
		rec2Bytes();
		bytes2Qin();
	}
	
	public static void rec2Bytes()
	{
		int count =0;
		foreach(Record r1 in records)
		{
			count = 0;
			foreach(Entry e1 in recordStruct)
			{
				int int1 = e1.Length;
				var array = new byte[int1];
				switch(r1.Fields[count].Datatype)
				{
					
					case "int":
						switch (int1)
						{
							case 1:
								r1.RecBytes.Add(new byte[]{r1.Fields[count].ValByte});
								break;
							case 2:
								r1.Fields[count].ValShort.CopyToByteArray(array,0);
								Array.Reverse(array);
								r1.RecBytes.Add(array);
								break;
							case 4:
								r1.Fields[count].ValInt.CopyToByteArray(array,0);
								Array.Reverse(array);
								r1.RecBytes.Add(array);
								break;	
							case 8:
								r1.Fields[count].ValLong.CopyToByteArray(array,0);
								Array.Reverse(array);
								r1.RecBytes.Add(array);
								break;
						}
						
						break;
					case "string1":
					case "string2":
						r1.RecBytes.Add(byteString(r1.Fields[count].ValStr,int1));
						break;
					case "reserve":
						Array.Clear(array , 0, array.Length);
						r1.RecBytes.Add(array);
						break;
				}
				count++;	
			}
		
		}
	}
	
	public static byte[] byteString(string s1,int num1)
	{
	return Encoding.Default.GetBytes(s1.PadRight(num1, ' '));
	}
	public static void recordEntry(string name,string datatype,int length)
	{
		recordStruct.Add(new Entry{Name = name,Datatype = datatype,Length = length});
	}
	
	public static void sentry(string nDL)
	{
		string[] temp = nDL.Split(',');
		recordEntry(temp[0],temp[1],Convert.ToInt32(temp[2]));
	}
	
	public static void entropy()
	{
		string[] text = System.IO.File.ReadAllLines(@"struct\input.txt");
		foreach(string s1 in text)
		{
			sentry(s1);
		}
	}
	
	public static byte[] StringToByteArray(string str, int length) 
	{
		return Encoding.ASCII.GetBytes(str.PadRight(length, ' '));
	}  
	
	public static void popuRec()
	{
		StreamReader reader = new StreamReader(File.OpenRead(@csvPath));
	
		while(!reader.EndOfStream)
		{
			string s1 = reader.ReadLine();
			string[] s2 = s1.Split(',');
			int count = 0;
			Record rec = new Record();
			foreach(Entry e in recordStruct)
			{
				Field seinfield = new Field();
				seinfield.Name = e.Name;
				seinfield.Datatype = e.Datatype;
				switch(e.Datatype)
				{
					case "int":
						switch(e.Length)
						{
							case 1:
								seinfield.ValByte=(byte)Convert.ToInt32(s2[count]);
								break;
							case 2:
								seinfield.ValShort=(short)Convert.ToInt32(s2[count]);
								break;
							case 4:
								seinfield.ValInt=(int)Convert.ToInt32(s2[count]);
								break;
							case 8:
								seinfield.ValLong=Convert.ToInt64(s2[count]);
								break;
						}
						break;
					case "string1":
					case "string2":
						seinfield.ValStr=s2[count];
						break;
					case "reserve":
					default:
						break;
				}
				rec.Fields.Add(seinfield);
				count++;
			}
			records.Add(rec);
		}
	}
	
	
	public static void bytes2Qin()
	{
		 FileStream cqin = new FileStream("input.cqin", FileMode.OpenOrCreate,
            FileAccess.Write, FileShare.ReadWrite);
		BinaryWriter writer = new BinaryWriter(cqin);
		foreach(Record r1 in records)
		{
			foreach(byte[] b1 in r1.RecBytes)
			{
			writer.Write(b1);
			}
		}
		writer.Close();
	}
	
}

public static class Int16Extension
		{
			public static void CopyToByteArray(this short source, byte[] destination, int offset)
			{
				int pad = 2;
				if (destination == null)
					throw new ArgumentException("Destination array cannot be null");

				// check if there is enough space for all the 2 bytes we will copy
				if (destination.Length < offset + pad)
					throw new ArgumentException("Not enough room in the destination array");
				int x = (destination.Length-1)*8;
				
				for (int i = 0; i < pad; i++){
				destination[offset+i] = (byte)(source >> x-(8*i)); // byte iteration
				}
			}
		}
		
public static class Int32Extension
		{
			public static void CopyToByteArray(this int source, byte[] destination, int offset)
			{
				int pad = 4;
				if (destination == null)
					throw new ArgumentException("Destination array cannot be null");

				// check if there is enough space for all the 4 bytes we will copy
				if (destination.Length < offset + pad)
					throw new ArgumentException("Not enough room in the destination array");
				int x = (destination.Length-1)*8;
				
				for (int i = 0; i < pad; i++){
				destination[offset+i] = (byte)(source >> x-(8*i)); // byte iteration
				}
			}
		}
public static class Int64Extension
		{
			public static void CopyToByteArray(this long source, byte[] destination, int offset)
			{
				int pad = 8;
				if (destination == null)
					throw new ArgumentException("Destination array cannot be null");

				// check if there is enough space for all the 8 bytes we will copy
				if (destination.Length < offset + pad)
					throw new ArgumentException("Not enough room in the destination array");
				int x = (destination.Length-1)*8;
				
				for (int i = 0; i < pad; i++){
				destination[offset+i] = (byte)(source >> x-(8*i)); // byte iteration
				}
			}
		}
		