
//------------------------------------------------------------------------
//Seaquote.cs - reads and translates binary sequencer Output files to csv.file
//------------------------------------------------------------------------

using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

class Sequote
{
	public class Entry
		{
		public string Name {get;set;}
		public string Datatype {get;set;}
		public int Length {get;set;}
		}
	public class Field
		{
		public string Name {get;set;}
		public string Datatype {get;set;}
		public int ValInt {get;set;}
		public string ValStr {get;set;}
		public long ValLong {get;set;}
		}
	public static List<Entry> recordStruct = new List<Entry>();
	
	
	public static void recordEntry(string name,string datatype,int length)
	{
		recordStruct.Add(new Entry{Name = name,Datatype = datatype,Length = length});
	}
	
	public static void sentry(string nDL)
	{
		string[] temp = nDL.Split(',');
		recordEntry(temp[0],temp[1],Convert.ToInt32(temp[2]));
	}
	
	public static void runEntries()
	{
		string[] text = System.IO.File.ReadAllLines(@"struct\input.txt");
		foreach(string s1 in text)
		{
		sentry(s1);
		}
	}
	public static void Main()
	{
		runEntries();
		FileStream f = new FileStream(@"Output.cqout", FileMode.Open);
		BinaryReader r = new BinaryReader(f);
		
		List<List<Field>> records = new List<List<Field>>();
		
		while(true)
		{
			List<Field> record = new List<Field>();
			foreach(Entry e1 in recordStruct)
			{
			
				try
				{
					if(e1.Datatype == "int")
					{
						switch(e1.Length)
						{
						case 1:
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(r.Read())});;
							break;
						case 2:
							
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(r.ReadInt16())});
							//Console.WriteLine(record[(record.Count-1)].ValInt);
							//Console.ReadLine();
							break;
						case 4:
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(r.ReadInt32())});
							//Console.WriteLine(record[(record.Count-1)].ValInt);
							//Console.ReadLine();
							break;	
						default:
							Console.WriteLine("ERROR");
							break;	
						}
					}
					if(e1.Datatype == "intRev")
					{
						byte[] by = r.ReadBytes(e1.Length);
						byte[] by1 = {by[0],by[1]};
						short sh1 = BitConverter.ToInt16(by1,0);
						if (sh1 < 1000)
						{
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(sh1)});
						}else
						{
							int fiveDig = BitConverter.ToInt32(by,0);
							record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValInt= Convert.ToInt32(fiveDig)});
						}
					}
					if(e1.Datatype == "string1")
					{	
						//byte[] by = new Byte[r.ReadByte()];
						record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype,ValInt = 0,ValStr = Convert.ToString(r.ReadChar())});
					}
					
					if(e1.Datatype == "long")
					{	
						byte[] by = r.ReadBytes(e1.Length);
						record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype, ValLong= BitConverter.ToInt64(by,0)});
					}
					
					if(e1.Datatype == "string2")
					{	
						byte[] by = r.ReadBytes(e1.Length);
						record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype,ValInt = 0,ValStr = BitConverter.ToString(by,0)});
					}
					if(e1.Datatype == "ref")
					{	
						byte[] by = r.ReadBytes(e1.Length);
						String temp = "";
						record.Add(new Field() {Name = e1.Name, Datatype = e1.Datatype,ValInt = 0,ValStr = BitConverter.ToString(by,0)});
						temp = System.Text.Encoding.UTF8.GetString(by).Replace("\n","\\n").Replace("\r","\\r").Replace("\t","\\t");
						record.Add(new Field() {Name = "RefUtf8", Datatype = "RefUtf8",ValInt = 0,ValStr = temp});
						temp = (System.Text.Encoding.ASCII.GetString(by)).Replace("\n","\\n").Replace("\r","\\r").Replace("\t","\\t");
						record.Add(new Field() {Name = "RefASCII", Datatype = "RefAscii",ValInt = 0,ValStr = temp});
					}
				}
				catch(EndOfStreamException)
				{
					records.Add(record);
					goto writeLoop;
				}	
			}
			records.Add(record);
		}
writeLoop:	
		StreamWriter sw = new StreamWriter("test.csv");
		string header = "";
		foreach(Entry e1 in recordStruct)
		{
			header += (e1.Name + ",");
		}
		header += ("RefUtf8,RefASCII");
		sw.WriteLine(header);
		foreach(List<Field> rec in records)
		{
			foreach(Field f1 in rec){
				switch(f1.Datatype)
				{
					case "intRev":
					case "int":
						sw.Write(f1.ValInt+",");
						break;
					case "string1":
						sw.Write(f1.ValStr+",");
						break;
					case "string2":
						sw.Write(f1.ValStr+",");
						break;
					case "long":
						sw.Write(f1.ValLong+",");
						break;
					case "ref":
					case "RefAscii":
					case "RefUtf8":
						sw.Write(f1.ValStr+",");
						break;
					default:
						Console.WriteLine("Error");
						break;
				}
			}
			
			sw.WriteLine();
		}
	}
}
