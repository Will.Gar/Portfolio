/** suitors.cpp - vector bases calculation of suitor loop to survive elimination
 * Will Gardner 10/19/2017
 * CSC223100
*/
#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;

class Suitor
{
	public:
		int suitorID;
		Suitor();
		Suitor(int id);
};
int gInt(string promptTxt);


int main()
{
	int suitors;
	int suitorCount = 0;
	vector<Suitor> v; 
	vector<int> deletionQueue;
	cout << "---Suitor Survivor---" << endl;
	suitors = gInt("Enter the number of suitors:");
	
	for (int i= 0;i < suitors;i++)
	{
		Suitor suit = Suitor((i+1));
		v.push_back(suit);
	}
	do
	{
			
		deletionQueue.clear();
		//cout << "[";
		//for (int i =0; i < v.size();i++)
		//{
		//	cout << v[i].suitorID << ",";
		//}
		//cout << "]"<<endl;
		for (int i = 0;(i < v.size())&&(v.size() > 1);i++)
		{
			++suitorCount;
			if (suitorCount == 3)
			{
				//cout << v[i].suitorID << endl;
				deletionQueue.push_back(i);
				suitorCount=0;
			}
		
		}
		for (int i = 0; i < deletionQueue.size();i++){
			//cout << "deleting " << deletionQueue[i]<<endl;
			v.erase((v.begin()+deletionQueue[i]));
		}
	
	}while (v.size() != 1);
	cout << "If you would like to live albiet married, be suitor " << v[0].suitorID;
	
}

Suitor::Suitor()
{
	suitorID = 0;
}
Suitor::Suitor(int id)
{
	suitorID = id;
}
/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
