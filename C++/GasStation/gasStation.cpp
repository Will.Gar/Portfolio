/* gasStation.cpp - simulates a gas station pump
 * Will Gardner 10/14/17
 * CSC 223
 */
#include <cmath>
#include <ctime>
#include <iomanip>
#include <iostream>
using namespace std;

class GasEntry
{

	public:
		//constructors 
		GasEntry();
		GasEntry(double cRate,double cCost);
		//functions
		void startClock();
		void endClock();
		void displayCurrentFlow();
		void setEnd();
		void calcGallon();
		void calcCharge();
		//vars
		bool startFlag;
		time_t start,end;
		double gallons;
		double rate;
		double chargeAmount;
		double cost;
		
};


int menu(const string menuArr[],int size);
int gInt(string promptTxt);
void printAllArrayWithCount(const string arr1[],int size);
void printArrayElem(const string arr1[],int index);

int main()
{
	int choice;
	const double rate = 13.0/60.0;
	const double cost = 4.00;
	const string menuArr[5]={"Start pump","Stop Pump","Display Total","Display Last Entry","Quit"};
	bool quit=false;
	GasEntry currentGas = GasEntry(rate,cost);
	GasEntry pastGas = GasEntry(rate,cost);
	do
	{
		cout << "Gas Station Simulator" << endl;
		choice = menu(menuArr,5);
		switch(choice)
		{
			//Start pump
			case 0:
				currentGas.startClock();
				break;
				//Stop Pump
			case 1:
				currentGas.endClock();
				pastGas = currentGas;
				break;
				//Display Total
			case 2:
				if (currentGas.startFlag)
					currentGas.setEnd();
				currentGas.displayCurrentFlow();
				break;
				//Display Last Entry
			case 3:
				pastGas. displayCurrentFlow();
				break;
			case 4:
				quit=true;
				break;
				//Quit
		}
	}while (!quit);
	return 0;
}
	//constructors
	//------------	
GasEntry::GasEntry()
{
	startFlag=false;
	chargeAmount=0.0;
	gallons=0.0;
	rate=(13.0/60.0);//US average of gallon flow rate
	cost=2.00;
}
		//functions

GasEntry::GasEntry(double cRate,double cCost)
{
	startFlag=false;
	chargeAmount=0.0;
	gallons=0.0;
	rate=cRate;
	cost=cCost;

}
	//-------------
//startClock - starts the gas station pump
void GasEntry::startClock()
{
	if (startFlag)
	{
		cout << "clock already started."<<endl;
	}	
	else 
	{
		startFlag = true;
		time(&start);
	}
		
}
//endClock - ends the gas station pump
void GasEntry::endClock()
{
	if (!startFlag)
	{
		cout << "You have not yet begun a pump session you can end." << endl;
	}else
	{

		startFlag = false;
		time(&end);
	}

}
//displayCurrentFlow -- displays the Number of gallons,the cost per gallon, and the total.
void GasEntry::displayCurrentFlow()
{
	calcGallon();
	calcCharge();
	cout.precision(4);
	cout << "Number of Gallons in tank:" << gallons<<endl;
	cout.precision(4);
	cout << "Cost per Gallon $" << cost << endl;
	cout.precision(4);
	cout << "Total: $" << chargeAmount << endl;
	
}
// setEnd -- sets the endTime
void GasEntry::setEnd()
{
	time(&end);
}
//calcGallon calculates the number of gallons
void GasEntry::calcGallon()
{
	gallons=difftime(end,start) * rate;
}
//calcCharge -- calculate the charge
void GasEntry::calcCharge()
{
	chargeAmount= cost * gallons;
}

/*printArrayElem -- prints an element from an array
 *input-	arr1 - array from which an element will be printed
 *			index - index of item to be printed
 */
void printArrayElem(const string arr1[],int index)
{
	cout << arr1[index] << endl;
}	


//printAllArrayWithCount - loops through array and prints each element
//input		arr1-array to print 
void printAllArrayWithCount(const string arr1[],int size)
{
	for(int i = 0;i < size;i++)
	{
		cout << i <<". ";
		printArrayElem(arr1,i);
	}
}
//menu-simple menu entry display with int input
//input		menuArr - string array of menu items
//output	user input int value
int menu(const string menuArr[],int size)
{
	int input=0;
	do
	{
		printAllArrayWithCount(menuArr,size);
		input = gInt("Enter Choice: ");
		if (input<0||input>=size)
			cout << "invalid option, please reenter:" << endl;
	}while(input<0||input>=size);
	return input;
}


/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
