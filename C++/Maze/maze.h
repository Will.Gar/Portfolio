#pragma once
#ifndef MAZE_H
#define MAZE_H
//Maze.h - head for main class
//By Will Gardner
#include <iostream>
#include <string>
#include <vector>
#include <iterator>

using namespace std;
namespace maz{ 
	
	class Maze
	{
		public:
			
			Maze(ifstream& inStream); 
			void printMaze();
			void solveMaze();
			friend ostream& operator<<(ostream& outputStream, Maze& maize);
		
		private:
			vector<string> maze;
			bool solve(size_t x,size_t y);
			int checkForChar(size_t x,size_t y,char check);
			size_t startX,startY;


	};		
}
#endif //MAZE_H
