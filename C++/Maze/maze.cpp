//Maze.cpp - Import a maze text file into a two dimension
//By Will Gardner
#include <iostream>
#include <string>
#include <fstream>
#include "maze.h"
#include <iterator>
namespace maz
{

	Maze::Maze(ifstream& inStream)
	{	
		string temp;

		inStream >> temp;
		inStream >> temp;
		inStream >> startY;
		inStream >> startX;
		vector<string> tempVector(istream_iterator<string>(inStream),{});
		maze=tempVector;
		
	}
	void Maze::printMaze()
	{
		int size = maze.size();
		for(size_t i = 0;i < size;i++)
			cout << maze[i] << endl;
	}
	
	ostream& operator<<(ostream& outputStream, Maze& maize)
	{
		int size = maize.maze.size();
		for(size_t i = 0;i < size;i++)
			outputStream << maize.maze[i] << "\n";
		return outputStream;

	}
	void Maze::solveMaze()
	{
	
		bool mazeSolve=solve(startX,startY);
		if(mazeSolve==true)
		{
			cout << "MAZE Solved!"<<endl;
		}
	}

	bool Maze::solve(size_t x,size_t y)
	{
		bool pathBool=false;
		maze[y][x]='P';
		int tempNum;
		if(checkForChar(x,y,'E') > 0)
		{
			return true;
		}
		tempNum = checkForChar(x,y,'O');
		while((tempNum > 0)&&(!pathBool))
		{
			if(tempNum == 1)
				pathBool=solve((x+1),y);
			else if(tempNum == 2)
				pathBool=solve(x,(y+1));
			else if(tempNum == 3)
				pathBool=solve((x-1),y);
			else if(tempNum == 4)
				pathBool=solve(x,(y-1));
			tempNum = checkForChar(x,y,'O');
		}
		if (pathBool)
		{
			return true;
		}
		else
		{
			maze[y][x]='X';
			return false;
		}
	}
	int Maze::checkForChar(size_t x,size_t y,char check)
	{
		if (((x+1)<maze[y].size())&&(maze[y][(x+1)]==check))
			return 1;
		else if (((y+1)<maze.size())&&(maze[y+1][x]==check))
			return 2;
		else if ((((int)x-1)>=0)&&(maze[y][x-1]==check))
			return 3;
		else if (((int)y-1 >= 0)&&(maze[y-1][x]==check))
			return 4;
		else
			return 0;
	}

	
}
