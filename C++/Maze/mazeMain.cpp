//mazeMain.cpp - main controller for the main clase
//By Will Gardner
#include <iostream>
#include <fstream>
#include "maze.h"
using namespace std;
using namespace maz;
int main()
{
	string path="maze.txt";
	ifstream input;
	input.open(path);
	Maze maize(input);
	cout << maize;
	maize.solveMaze();
	cout << maize;
	return 0;
}
