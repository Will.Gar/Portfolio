//loveHate.cpp - program that doesn't like the word hate. replaces all instance of have with love in input file.
//Will Gardner CSC 202 
#include <iostream>
#include <fstream>
#include <string>

using namespace std;
int main()
{
	string text;
	string line;
	fstream inputStream;
	inputStream.open("doYouLoveCPlusPlus.txt");
	while (inputStream >> text)
	{
		if (text=="hate")
		{
			text = "love";
		}
		line += (text+" ");	
	}
	inputStream.close();
	cout << line;
	return 0;
}
