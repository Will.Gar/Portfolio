//scoreHandler.cpp - score reference for highscores but with classes
//Will Gardner 10/12/17
//CSC223

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Player
{
	public: 
		void input();
		void output();
		string name;
		int score;
		Player();
		Player(string name1,int score1);
};


string gString(string promptTxt);
void printAllArray(Player arr1[],int size);
void printStats(Player playerArr[],int index);
void printAllStats(Player playerArr[],int size);
void printAllArrayWithCount(Player playerArr,int size);
int searchName(Player playerArr[],string searchString,int size);
int menu(string menuArr[],int size);
void removeElement(Player arr1[],int index,int size);
bool addToPlayerArray(Player playerArray[],Player itemToAdd,int size);
bool checkArrSpace(Player playerArr[],int size);
int gInt(string promptTxt);
int indexOfEmpty(Player arr1[],int size);

int main()
{
	int size=10;
	string filePath="scores.txt";
	Player players[size]={Player()};
	string menuArr[5]={"print all players and scores","choose a player and score to print","add a player","remove a player","Quit"};
	bool quit=false;
	int choice;
	string tempName;
	int tempIndex;
	Player tempPlayer = Player();
	do
	{
		
		cout << "Score Manager"<<endl;
		choice = menu(menuArr,5);
		switch (choice)
		{
			// print all players
			case 0:
				printAllStats(players,size);
				break;
			//print single player
			case 1:
				tempName=gString("enter a Name to Search for:");
				tempIndex = searchName(players,tempName,size);
				if (tempIndex > -1)
					printStats(players,tempIndex);
				else
					cout << "Player name does not exist, try again."<<endl;
				break;	
			//add a player
			case 2:
				
				if (checkArrSpace(players,size))
				{
					tempPlayer.input();
					addToPlayerArray(players,tempPlayer,size);
				}
				else
					cout << "there is no room left"<< endl;
				break;
			case 3:
				tempName=gString("enter a Name to remove:");
				tempIndex = searchName(players,tempName,size);
				if (tempIndex > -1)
				{
					removeElement(players,tempIndex,size);
				}
				else
					cout << "Player name does not exist, try again."<<endl;
				break;	
			case 4:
				quit=true;
				break;
		}
		//cout << choice << endl;
	}while (!quit);
	cout << "thanks for playin'!"<<endl;

}



Player::Player()
{
	name="";
	score=0;
}
Player::Player(string name1,int score1)
{
	name=name1;
	score=score1;
}

//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}

/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}

void Player::input()
{
	name=gString("Enter Name:");
	score=gInt("Enter Score:");
}

void Player::output()
{
	cout << name << "\t" << score << endl;
;

}

void removeElement(Player arr1[],int index,int size)
{	
	Player tempArr[size]= {Player()};
	int i =0;
	int k =0;
	while (i < size)
	{
		if(i != index)
			tempArr[k]=arr1[i];
			k++;
		i++;
	}
	for (i=0;i<size;i++)	
		arr1[i]=tempArr[i];
}

//searchStrArr - searches string array for matching string
//input 	stringArr - string array to search
//		searchString - string to search for
//output - index of match, -1 if not found
int searchName(Player playerArr[],string searchString,int size)
{
	for(int i = 0; i < size;i++)
	{
		if (playerArr[i].name==searchString)
			return i;
	}
	return -1;
}

void printStats(Player playerArr[],int index)
{
	playerArr[index].output();
}


void printAllStats(Player playerArr[],int size)
{
	for(int i=0;i < size;i++)
	{
		printStats(playerArr,i);
	}
}
void printArrayElem(const string arr1[],int index)
{
	cout << arr1[index] << endl;
}	


//printAllArrayWithCount - loops through array and prints each element
//input		arr1-array to print 
void printAllArrayWithCount(const string arr1[],int size)
{
	for(int i = 0;i < size;i++)
	{
		cout << i <<". ";
		printArrayElem(arr1,i);
	}
}

int menu(string menuArr[],int size)
{
	int input=0;
	do
	{
		printAllArrayWithCount(menuArr,size);
		input = gInt("Enter Choice: ");
		if (input<0||input>=size)
			cout << "invalid option, please reenter:" << endl;
	}while(input<0||input>=size);
	return input;
}


//addToPlayerArray - search for a null value and add item to it returning true,unless array is full in which case false is returned
//input - 	intArray - array of ints
//		itemToAdd - int to add to array
//output 	true - if item added 
//		false - item not added
bool addToPlayerArray(Player playerArr[],Player itemToAdd,int size)
{
	int i = 0; //index value
	while (i < size)
	{
		if(playerArr[i].name=="")
		{
			playerArr[i]=itemToAdd;
			return true;
		}
		i++;
	}
	return false;
}

/**checkArrSpace - check that there is empty space in array
 * input	intArr - array of ints
 * output	true if space left;false if not.
 */
bool checkArrSpace(Player playerArr[],int size)
{
	for(int i = 0; i < size;i++)
	{
		if (playerArr[i].name=="")
			return true;
	}
	return false;
}

//indexOfEmpty - return index of first empty value
//input		arr1- array to check
//		size- size of array
//output	index of first empty {array element
int indexOfEmpty(Player arr1[],int size)
{
	for(int i=0; i<size;i++)
	{
		if (arr1[i].name=="")
			return i;
	}
	return -1;
}
