//scoreHandler.cpp - score reference for highscores
//Will Gardner 10/1/17
//CSC223

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;





void getData(ifstream& file,string& playerName, int& score);
int fNxInt(ifstream& file);
string fNxString(ifstream& file);
void addToAveragePool(double value, long long& pool,int& counter);
double calcAverage(long long pool, int counter);
void compareToAverage(int score, double average);
double fAverage(string filePath);
int getHighScore(string filepath,string playerName);
string gString(string promptTxt);
void addFileArrayValues(string filePath,string stringArr[],int intArr[],int size);
void addScore(string stringArr[],int intArr[],int size);
int searchStrArr(const string stringArr[],string searchString,int size);
void printAllArray(const string arr1[],int size);
void printStats(const string stringArr[],const int intArr[],int index);
void printAllStats(const string stringArr[],const int intArr[],int size);
void printAllArrayWithCount(const string arr1[],int size);
void printAllArray(const int arr1[],int size);
void printArrayElem(const string arr1[],int index);
void printArrayElem(const int arr1[],int index);
int menu(const string menuArr[],int size);
void removeElement(string arr1[],int index,int size);
void removeElement(int arr1[],int index,int size);
bool addToIntArray(int intArr[],int itemToAdd,int size);
bool checkArrSpace(string strArr[],int size);
bool checkArrSpace(int intArr[],int size);
bool addToStringArray(string stringArr[],string itemToAdd,int size);
int gInt(string promptTxt);
int indexOfEmpty(int arr1[],int size);
int indexOfEmpty(string arr1[],int size);
int main()
{
	const int size=10;
	string filePath="scores.txt";
	string playerNames[size]={""};
	const string menuArr[5]={"print all players and scores","choose a player and score to print","add a player","remove a player","Quit"};
	int scores[size]={0};
	bool quit=false;
	int choice;
	string tempName;
	int tempIndex;
	do
	{
		
		cout << "Score Manager"<<endl;
		choice = menu(menuArr,5);
		switch (choice)
		{
			// print all players
			case 0:
				printAllStats(playerNames,scores,size);
				break;
			//print single player
			case 1:
				tempName=gString("enter a Name to Search for:");
				tempIndex = searchStrArr(playerNames,tempName,size);
				if (tempIndex > -1)
					printStats(playerNames,scores,tempIndex);
				else
					cout << "Player name does not exist, try again."<<endl;
				break;	
			//add a player
			case 2:
				addScore(playerNames,scores,size);
				break;
			case 3:
				tempName=gString("enter a Name to remove:");
				tempIndex = searchStrArr(playerNames,tempName,size);
				if (tempIndex > -1)
				{
					removeElement(playerNames,tempIndex,size);
					removeElement(scores,tempIndex,size);
				}
				else
					cout << "Player name does not exist, try again."<<endl;
				break;	
			case 4:
				quit=true;
				break;
		}
		cout << choice << endl;
	}while (!quit);
	cout << "thanks for playin'!"<<endl;

}

//getData -- set data as next data set
//input-	playerName - name of player and first in dataset
//		score - int representing score and second in data set
void getData(ifstream& file,string& playerName, int& score)
{	
	playerName=fNxString(file);
	score=fNxInt(file);
}
//indexOfEmpty - return index of first empty value
//input		arr1- array to check
//		size- size of array
//output	index of first empty array element
int indexOfEmpty(int arr1[],int size)
{
	for(int i=0; i<size;i++)
	{
		if (arr1[i]==0)
			return i;
	}
	return -1;
}

//indexOfEmpty - return index of first empty value
//input		arr1- array to check
//		size- size of array
//output	index of first empty {array element
int indexOfEmpty(const string arr1[],int size)
{
	for(int i=0; i<size;i++)
	{
		if (arr1[i]=="")
			return i;
	}
	return -1;
}	
//fNxInt- read next ifstream data as int
// input	file - stream of file
// output 	int value read from file
int fNxInt(ifstream& file)
{
	int returnInt;
	file >> returnInt;
	return returnInt;
}

//fNxString-read next fstream data as string
// input	file - stream of file
// output 	string value read from file
string fNxString(ifstream& file)
{
	string returnString;
	file >> returnString;
	return returnString;
}

//adds value to pool and sets counter
//input		value - value to add
//		pool - pool of data
//		counter - counter of data in pool
void addToAveragePool(double value, long long& pool,int& counter)
{
	pool += value;
	++counter;
}

//calculateAverage - calculates Average
//input -	pool - pool of data
//		counter - counter of data in pool
//output	average   
double calcAverage(long long pool, int counter)
{
	//cout <<pool << endl << counter;
	//cout<< endl<<counter<<endl;
	return (pool/(double)counter);
}

//loop through file and average of second string of data as an average
//input-	filepath - pathof file
//output-	double value of average
double fAverage(string filePath)
{
	ifstream inf(filePath);
	string playerName;
	int score=0;
	int counter=0;
	long long pool=0;
	double average;

	while (inf)
	{
		getData(inf,playerName,score);
		addToAveragePool(score,pool,counter);	
	}
	average=calcAverage(pool,counter);
	return average;
}

//compareToAverage compare to value to average to score and display a result of that comparison
//input		score-value of score
//		average - value of average
void compareToAverage(int score, double average)
{
	if (score > average)
		cout << "This score is above average." << endl;
	else if (score == average)
		cout << "This score is average." << endl;
	else if (score < average)
		cout << "This score is sadly below the average." << endl;	
}

//search for playerName and return score of corresponding player
//input 	playerName nameOfPlayer
//		filepath - file of scores path
//outut 	player score
int getHighScore(string filePath,string playerName)
{
	ifstream inf(filePath);
	string tempPlayer;
	int score;
	while (inf)
	{
		getData(inf,tempPlayer,score);
		if(tempPlayer==playerName)
			return score;
	}
	return 0;
}


/** addFIleArray- fills array with score from file
 * input -	filePath - file of score path
 * 		stringArr - array of player names
 * 		intArr - array of ints
 */
void addFileArrayValues(string filePath,string stringArr[],int intArr[],int size)
{
	ifstream inf(filePath);
	string tempPlayer;
	int score;
	bool flag1=true;
	while (inf && flag1)
	{
		getData(inf,tempPlayer,score);
		flag1= addToStringArray(stringArr,tempPlayer,size);
		if (flag1)
			flag1= addToIntArray(intArr,score,size);

	}
}
/**addScore-prompts user for player string and player score, then adds each to the string and int arrays, respectively * input - 	stringArr - array of strings
 * 		intArr - array of ints
 */
void addScore(string stringArr[],int intArr[],int size)
{
	if (checkArrSpace(stringArr,size))
	{
		string tempPlayer = gString("Enter Player Name:");
		int tempScore = gInt("Enter Player Score:");
		if (addToStringArray(stringArr,tempPlayer,size))
			addToIntArray(intArr,tempScore,size);
		else
			cout << "Array is Full; try again after make some room." << endl;
	}
}

//searchStrArr - searches string array for matching string
//input 	stringArr - string array to search
//		searchString - string to search for
//output - index of match, -1 if not found
int searchStrArr(const string stringArr[],string searchString,int size)
{
	for(int i = 0; i < size;i++)
	{
		if (stringArr[i]==searchString)
			return i;
	}
	return -1;
}

//printStats - using an index value,print the player name and score
//input 	stringArr - string of player names
//		intArr - set of player scores
//		index - index of arrays to print
void printStats(const string stringArr[],const int intArr[],int index)
{
	cout << stringArr[index] << "\t" << intArr[index] << endl;
}

//printAllStats - print all stats
//input		stringArr - string of player names
//		intArr - set of player scores
void printAllStats(const string stringArr[],const int intArr[],int size)
{
	for(int i=0;i < size;i++)
	{
		printStats(stringArr,intArr,i);
	}
}

//printAllArray - loops through array and prints each element
//input		arr1-array to print 
void printAllArray(const string arr1[],int size)
{
	for(int i = 0;i < size;i++)
	{
		printArrayElem(arr1,i);
	}
}


//printAllArrayWithCount - loops through array and prints each element
//input		arr1-array to print 
void printAllArrayWithCount(const string arr1[],int size)
{
	for(int i = 0;i < size;i++)
	{
		cout << i <<". ";
		printArrayElem(arr1,i);
	}
}

//printAllArray - loops through array and prints each element
//input		arr1-array to print 
void printAllArray(const int arr1[],int size)
{
	for(int i = 0;i < size;i++)
	{
		printArrayElem(arr1,i);
	}
}

//printArrayElem- prints array element at index
//input		arr1-array to print
//		index - index to print
void printArrayElem(const string arr1[],int index)
{
	cout << arr1[index] << endl;
}	



//printArrayElem- prints array element at index
//input		arr1-array to print
//		index - index to print
void printArrayElem(const int arr1[],int index)
{
	cout << arr1[index] << endl;
}	

//menu-simple menu entry display with int input
//input		menuArr - string array of menu items
//output	user input int value
int menu(const string menuArr[],int size)
{
	int input=0;
	do
	{
		printAllArrayWithCount(menuArr,size);
		input = gInt("Enter Choice: ");
		if (input<0||input>=size)
			cout << "invalid option, please reenter:" << endl;
	}while(input<0||input>=size);
	return input;
}

//removeelement - removes an element from an array at a given index
//input		arr1- array to remove element from
//		index - index of array at which to remove element
void removeElement(string arr1[],int index,int size)
{
	string tempArr[size]={""};
	int i =0;
	int k =0;
	while (i < size)
	{
		if(i != index)
			tempArr[k]=arr1[i];
			//cout<<tempArr[k];
			k++;
		i++;
			
	}
	for (i=0;i<size;i++)
		arr1[i]=tempArr[i];
}

//removeelement - removes an element from an array at a given index
//input		arr1- array to remove element from
//		index - index of array at which to remove element
void removeElement(int arr1[],int index,int size)
{	
	int tempArr[size]={0};
	int i =0;
	int k =0;
	while (i < size)
	{
		if(i != index)
			tempArr[k]=arr1[i];
			k++;
		i++;
	}
	for (i=0;i<size;i++)	
		arr1[i]=tempArr[i];
}


//addToIntArray - search for a null value and add item to it returning true,unless array is full in which case false is returned
//input - 	intArray - array of ints
//		itemToAdd - int to add to array
//output 	true - if item added 
//		false - item not added
bool addToIntArray(int intArr[],int itemToAdd,int size)
{
	int i = 0; //index value
	while (i < size)
	{
		if(intArr[i]==0)
		{
			intArr[i]=itemToAdd;
			return true;
		}
		i++;
	}
	return false;
}

/**checkArrSpace - check that there is empty space in array
 * input	strArr - array of strings
 * output	true if space left;false if not.
 */
bool checkArrSpace(string strArr[],int size)
{
	for(int i = 0; i < size;i++)
	{
		if (strArr[i]=="")
			return true;
	}
	return false;
}

/**checkArrSpace - check that there is empty space in array
 * input	intArr - array of ints
 * output	true if space left;false if not.
 */
bool checkArrSpace(int intArr[],int size)
{
	for(int i = 0; i < size;i++)
	{
		if (intArr[i]==0)
			return true;
	}
	return false;
}

//addToStringArray - search for a null value and add item to it returning true,unless array is full in which case false is returned
//input - 	String Array - array of ints
//		itemToAdd - string to add to array
//output 	true - if item added 
//		false - item not added
bool addToStringArray(string stringArr[],string itemToAdd,int size)
{
	int i = 0; //index value
	while (i < size)
	{
		if(stringArr[i]=="")
		{
			stringArr[i]=itemToAdd;
			return true;
		}
		i++;
	}
	return false;

}
//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}

/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
