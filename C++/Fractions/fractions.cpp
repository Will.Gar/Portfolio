/**fractions.cpp - allows for the manipulation of a fraction object
 *Will Gardner
 * 10/10/17*/

#include <iostream>
using namespace std;


class Fraction
{
	public:
		void input();
		void output();	
		void reduce();
		Fraction multiply(Fraction frac);	
		int numerator;
		int denominator;
		Fraction division(Fraction frac);
		Fraction subtraction(Fraction frac);
		Fraction addition(Fraction frac);
		Fraction();
		Fraction(int num,int denom);
		Fraction(int num);
		bool greaterThan(Fraction frac);
		bool lessThan(Fraction frac);
		bool equals(Fraction frac);
		
		bool operator<(Fraction frac);
		bool operator>(Fraction frac);
		bool operator==(Fraction frac);
		
		
		friend ostream& operator<<(ostream& outputStream,Fraction frac);
		
		Fraction operator*(Fraction frac);
		Fraction operator/(Fraction frac);
		Fraction operator+(Fraction frac);
		Fraction operator-(Fraction frac);
};




//Add
//Subtract
//Mux
//Divide
//GCF
//Decimal to fraction conversion
//Simplify
//Mixed



void Fraction::reduce()
{
	for(int x=numerator;x>1;x--)
	{
		if(numerator%x==0 && denominator%x==0)
		{
			numerator/=x;
			denominator/=x;
			break;
		}
	}
}

void Fraction::input()
{
	cout << "enter numerator:";
	cin >> numerator;
	cout << "enter denominator:";
	cin >> denominator;
}

void Fraction::output()
{
	cout << numerator <<"/" << denominator << endl;
}

Fraction::Fraction()
{
	numerator = 0;
       	denominator = 1;	
}

Fraction::Fraction(int num,int denom)
{
	numerator=num;
	denominator = denom;
}

Fraction::Fraction(int num)
{
	numerator=num;
	denominator=1;
}

Fraction Fraction::operator*(Fraction frac)
{
	Fraction result;
	result.numerator=numerator*frac.numerator;
	result.denominator=denominator*frac.denominator;
	return result;
}

Fraction Fraction::multiply(Fraction frac)
{
	Fraction result;
	result.numerator=numerator*frac.numerator;
	result.denominator=denominator*frac.denominator;
	return result;
}

Fraction Fraction::operator/(Fraction frac)
{
	Fraction result;
	result.numerator=numerator*frac.denominator;
	result.denominator=denominator*frac.numerator;
	return result;
}

Fraction Fraction::division(Fraction frac)
{
	Fraction result;
	result.numerator=numerator*frac.denominator;
	result.denominator=denominator*frac.numerator;
	return result;
}

Fraction Fraction::operator-(Fraction frac)
{
	Fraction result;
	result.numerator=(numerator*frac.denominator)-(frac.numerator*denominator);
	result.denominator=(denominator*numerator);
	return result;

}
Fraction Fraction::subtraction(Fraction frac)
{
	Fraction result;
	result.numerator=(numerator*frac.denominator)-(frac.numerator*denominator);
	result.denominator=(denominator*numerator);
	return result;

}
Fraction Fraction::operator+(Fraction frac)
{
	Fraction result;
	result.numerator=(numerator*frac.denominator)+(frac.numerator*denominator);
	result.denominator=(denominator*numerator);
	return result;
}

Fraction Fraction::addition(Fraction frac)
{
	Fraction result;
	result.numerator=(numerator*frac.denominator)+(frac.numerator*denominator);
	result.denominator=(denominator*numerator);
	return result;
}

bool Fraction::operator<(Fraction frac)
{
	return (numerator*frac.denominator)<(denominator*frac.numerator);
}

bool Fraction::operator>(Fraction frac)
{
	return (numerator*frac.denominator)>(denominator*frac.numerator);
}

bool Fraction::operator==(Fraction frac)
{
	return (numerator*frac.denominator)==(denominator*frac.numerator);
}

bool Fraction::lessThan(Fraction frac)
{
	return (numerator*frac.denominator)<(denominator*frac.numerator);
}

bool Fraction::greaterThan(Fraction frac)
{
	return (numerator*frac.denominator)>(denominator*frac.numerator);
}

bool Fraction::equals(Fraction frac)
{
	return (numerator*frac.denominator)==(denominator*frac.numerator);
}

ostream& operator<<(ostream& outputStream,Fraction frac)
{
	outputStream << frac.numerator << "/" << frac.denominator << endl;
	return outputStream;
}

int main()
{

	Fraction fract;
	Fraction fract2;
	fract.input();
	fract2.input();
	cout << fract;
	fract.reduce();
	fract.output();
	Fraction ans= fract*fract2;
	ans.output();	
	ans=fract/fract2;	
	ans.output();
	ans = ans+4; 
	cout << ans;
	Fraction fracto = Fraction();
	Fraction fractonium = Fraction(69);
	Fraction fractsys = Fraction(59,492);
	fracto.output();
	fractonium.output();
	fractsys.output();
}


