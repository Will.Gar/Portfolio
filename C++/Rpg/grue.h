
//Grue.h - header for cyberdemon
#pragma once
#ifndef GRUE_H
#define GRUE_H
#include <iostream>
#include <string>
#include <vector>
#include "demon.h"
#include <random>
#include <algorithm>
using namespace std;
using namespace daemonicus;
namespace gru
{
	class Grue:public Demon
	{
		public:
			Grue();
			Grue(int streng,int hitpoints,string typ,string namey);
			int getDamage();
			friend ostream& operator<<(ostream& outputStream, Grue& elo);
	};
}
#endif //GRUE_H

