//Demon.h - header for demon
#pragma once
#ifndef DEMON_H
#define DEMON_H
#include <iostream>
#include <string>
#include <vector>
#include "creature.h"
#include <random>
#include <algorithm>
using namespace std;
using namespace creat;
namespace daemonicus
{
	class Demon:public Creature
	{
		public:
			Demon();
			Demon(int streng,int hitpoints,string typ,string namey);
			int getDamage();
			string getName();
			void setName(string namey);
			friend ostream& operator<<(ostream& outputStream, Demon& demo);
		private:
			string name;
	};
}
#endif //DEMON_H
