#include <iostream>
#include <string>
#include <vector>
#include "creature.h"
#include "demon.h"
#include "human.h"
#include "balrog.h"
#include "elf.h"
#include "cyberdemon.h"
#include "grue.h"
#include <random>
#include <algorithm>
using namespace std;
using namespace daemonicus;
using namespace elfonicus;
using namespace sapien;
using namespace balrogonicus;
using namespace cyberdaemononicus;
using namespace gru;

string gString(string promptTxt);
int menu(vector<string> menuArr);
void printAllArrayWithCount(vector<string> arr1,int size);
int gInt(string promptTxt);
int getRandoIntegeresian(int maxN);
int getRandoIntegeresianIndex(int maxN);
void battle(Human& p1, Human& enemy);
void battle(Human& p1, Cyberdemon& enemy);
void battle(Human& p1, Balrog& enemy);
void battle(Human& p1, Elf& enemy);
void battle(Human& p1, Grue& enemy);
void levelUp(Human& p1,int boost);

int main()
{

	const vector<string> forestElem = {"rock","stream","pond","noticeably large tree","clearing","view of a mountain in the distance"};
	const vector<string> enemyTypes = {"human","cyberdemon","grue","balrog","elf"};
	const vector<string> options = {"look","walk north","walk east","walk west", "walk south"};
	cout << "LOST FOREST: THE RPG/DRINKING GAME" << endl;
	int score=0;
	string tempName;
	int enemyType;
	tempName=gString("enter Name:");
	Human player(5,20,"human",tempName);
	vector<string> surroundings ={"path to a house."};
	int choice;
	do
	{
		cout << "What do you want to do?:"<<endl;
		choice = menu(options);
		if(choice==0)
		{
			cout << "You are in a forest. In the particular area in which you have found your self, there is a " << surroundings[0];
		}
		else
		{
			//surroundings change
			surroundings.push_back(forestElem[getRandoIntegeresianIndex(forestElem.size())]);
			surroundings.erase(surroundings.begin());
			//battle gen
			enemyType=getRandoIntegeresianIndex(enemyTypes.size());
			switch(enemyType)
			{
				case 0:
				{	
					//human
					Human hume(5,10,"Human","Some Random Creeper");
					battle(player,hume);
				}
					break;
				
				case 1: 
				{
					//"cyberdemon"
					Cyberdemon cylo(5,11,"Cyberdemon","BrosefStalin69");
					battle(player,cylo);
				}
					break;
				case 2: 
				{
					//"grue"
					Grue grue(15,15,"","a grue");
					battle(player,grue);
				}
					break;
				case 3: 
				{
					//"balrog":
					Balrog balo(7,6,"Balrog","GROK");
					battle(player,balo);
				}
					break;
				case 4: 
				{
					//"elf":
					Elf elo(20,4,"Elf","Gregolas, lesser cousin of Legolas");
					battle(player,elo);
				}
					break;
				default:;
			}
			if (player.getHP() > 0)
			{
				++score;
				cout << "Player has lived to see another day."<< endl;
			}
			//postbattle point add
		}
	}while(player.getHP() > 0);	
	cout<< player.getName() << " collapsed, bleeding out upon the mossy forest floor in view of a " << surroundings[0] << endl;
	cout << "SCORE:" << score << endl;	
	return 0;
}
void battle(Human& p1, Human& enemy)
{
	int choice,tempDamage;
	vector<string> options ={"attack","splash"};
	cout << enemy.getType()<< " " << enemy.getName() << " appeared.";
	do{
		//player.turn
		choice = menu(options);
		if (choice==0)
		{
			cout << p1.getName() << " ATTACKS!"<<endl;
			tempDamage = p1.getDamage();
			cout << p1.getName() << " does " << tempDamage << " damage!!" << endl;
			enemy.setHP(enemy.getHP()-tempDamage);
		}
		else
		{
			cout << p1.getName() << "uses splash. it does nothing." << endl;
		}
		cout << enemy<<endl;
		//enemy turn
				
		cout << enemy.getName() << " ATTACKS!"<<endl;
		tempDamage = enemy.getDamage();
		cout << enemy.getName() << " does " << tempDamage << " damage!!" << endl;
		p1.setHP(p1.getHP()-tempDamage);
	}while ((p1.getHP()>0)&&(enemy.getHP()>0));
	if(p1.getHP() > 0)
	{
		cout << p1.getName() << " won the battle" << endl;
		levelUp(p1,getRandoIntegeresianIndex(5));
		cout << p1 << endl;
	}
}
void battle(Human& p1, Cyberdemon& enemy)
{
	int choice,tempDamage;
	vector<string> options ={"attack","splash"};
	cout << enemy.getType()<< " " << enemy.getName() << " appeared.";
	do{
		//player.turn
		choice = menu(options);
		if (choice==0)
		{
			cout << p1.getName() << " ATTACKS!"<<endl;
			tempDamage = p1.getDamage();
			cout << p1.getName() << " does " << tempDamage << " damage!!" << endl;
			enemy.setHP(enemy.getHP()-tempDamage);
		}
		else
		{
			cout << p1.getName() << "uses splash. it does nothing." << endl;
		}

		//enemy turn
				
		cout << enemy<<endl;
		cout << enemy.getName() << " ATTACKS!"<<endl;
		tempDamage = enemy.getDamage();
		cout << enemy.getName() << " does " << tempDamage << " damage!!" << endl;
		p1.setHP(p1.getHP()-tempDamage);
	}while ((p1.getHP()>0)&&(enemy.getHP()>0));
	if(p1.getHP() > 0)
	{
		cout << p1.getName() << " won the battle" << endl;
		levelUp(p1,getRandoIntegeresianIndex(5));
		cout << p1 << endl;
	}
}
void battle(Human& p1, Grue& enemy)
{
	int choice,tempDamage;
	vector<string> options ={"attack","splash"};
	cout << enemy.getType()<< " " << enemy.getName() << " appeared.";
	do{
		//player.turn
		choice = menu(options);
		if (choice==0)
		{
			cout << p1.getName() << " ATTACKS!"<<endl;
			tempDamage = p1.getDamage();
			cout << p1.getName() << " does " << tempDamage << " damage!!" << endl;
			enemy.setHP(enemy.getHP()-tempDamage);
		}
		else
		{
			cout << p1.getName() << "uses splash. it does nothing." << endl;
		}

		//enemy turn
				
		cout << enemy<<endl;
		cout << enemy.getName() << " ATTACKS!"<<endl;
		tempDamage = enemy.getDamage();
		cout << enemy.getName() << " does " << tempDamage << " damage!!" << endl;
		p1.setHP(p1.getHP()-tempDamage);
	}while ((p1.getHP()>0)&&(enemy.getHP()>0));
	if(p1.getHP() > 0)
	{
		cout << p1.getName() << " won the battle" << endl;
		levelUp(p1,getRandoIntegeresianIndex(5));
		cout << p1 << endl;
	}
}

void battle(Human& p1, Elf& enemy)
{
	int choice,tempDamage;
	vector<string> options ={"attack","splash"};
	cout << enemy.getType()<< " " << enemy.getName() << " appeared.";
	do{
		//player.turn
		choice = menu(options);
		if (choice==0)
		{
			cout << p1.getName() << " ATTACKS!"<<endl;
			tempDamage = p1.getDamage();
			cout << p1.getName() << " does " << tempDamage << " damage!!" << endl;
			enemy.setHP(enemy.getHP()-tempDamage);
		}
		else
		{
			cout << p1.getName() << "uses splash. it does nothing." << endl;
		}

		//enemy turn
				
		cout << enemy<<endl;
		cout << enemy.getName() << " ATTACKS!"<<endl;
		tempDamage = enemy.getDamage();
		cout << enemy.getName() << " does " << tempDamage << " damage!!" << endl;
		p1.setHP(p1.getHP()-tempDamage);
	}while ((p1.getHP()>0)&&(enemy.getHP()>0));
	if(p1.getHP() > 0)
	{
		cout << p1.getName() << " won the battle" << endl;
		levelUp(p1,getRandoIntegeresianIndex(5));
		cout << p1 << endl;
	}
}
void battle(Human& p1, Balrog& enemy)
{
	int choice,tempDamage;
	vector<string> options ={"attack","splash"};
	cout << enemy.getType()<< " " << enemy.getName() << " appeared.";
	do{
		//player.turn
		choice = menu(options);
		if (choice==0)
		{
			cout << p1.getName() << " ATTACKS!"<<endl;
			tempDamage = p1.getDamage();
			cout << p1.getName() << " does " << tempDamage << " damage!!" << endl;
			enemy.setHP(enemy.getHP()-tempDamage);
		}
		else
		{
			cout << p1.getName() << "uses splash. it does nothing." << endl;
		}

		//enemy turn
				
		cout << enemy<<endl;
		cout << enemy.getName() << " ATTACKS!"<<endl;
		tempDamage = enemy.getDamage();
		cout << enemy.getName() << " does " << tempDamage << " damage!!" << endl;
		p1.setHP(p1.getHP()-tempDamage);
	}while ((p1.getHP()>0)&&(enemy.getHP()>0));
	if(p1.getHP() > 0)
	{
		cout << p1.getName() << " won the battle" << endl;
		levelUp(p1,getRandoIntegeresianIndex(5));
		cout << p1 << endl;
	}
}
void levelUp(Human& p1,int boost)
{
	p1.setStrength(p1.getStrength()+boost);
	p1.setHP(p1.getHP()+boost*10);
}

//get random int
int getRandoIntegeresianIndex(int maxN)
{
	random_device rd;
	uniform_int_distribution<int> dist(0,maxN-1);
	return dist(rd);
}	
//get random int
int getRandoIntegeresian(int maxN)
{
	random_device rd;
	uniform_int_distribution<int> dist(0,maxN);
	return dist(rd);
}	

//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}

/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}

int menu(vector<string> menuArr)
{	
	int size=menuArr.size();
	int input=0;
	do
	{
		printAllArrayWithCount(menuArr,size);
		input = gInt("Enter Choice: ");
		if (input<0||input>=size)
			cout << "invalid option, please reenter:" << endl;
	}while(input<0||input>=size);
	return input;
}

void printAllArrayWithCount(vector<string> arr1,int size)
{
	for(int i = 0;i < size;i++)
	{
		cout << i <<". ";
		cout << arr1[i]<<endl;
	}
}
