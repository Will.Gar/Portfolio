
//Human.cpp - Human class
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include "creature.h"
#include "human.h"
using namespace std;
using namespace creat;

namespace
{
//get random int
	int getRandoIntegeresian(int maxN)
	{
		random_device rd;
		uniform_int_distribution<int> dist(0,maxN);
		return dist(rd);
	}
}

namespace sapien
{
	Human::Human():Creature()
	{
		name="UNKNOWN";
	}
	Human::Human(int streng,int hitpoints,string typ,string namey):Creature(streng,hitpoints,typ),name(namey)
	{
	}
	string Human::getName()
	{
		return name;
	}
	void Human::setName(string namey)
	{
		name=namey;
	}
	int Human::getDamage()
	{
		int damage;
		damage = Creature::getDamage();
		return damage;
	}
	
	ostream& operator<<(ostream& outputStream, Human& hume)
	{
		outputStream << "Name:" << hume.name << "\nHP:" << hume.getHP() <<"\nStrength:" << hume.getStrength() <<"\nType:" << hume.getType() << "\n";
		return outputStream;
	}
}
