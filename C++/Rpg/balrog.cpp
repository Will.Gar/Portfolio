//Balrog.cpp - Balrog class
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include "demon.h"
#include "balrog.h"
using namespace std;
using namespace daemonicus;

namespace
{
//get random int
	int getRandoIntegeresian(int maxN)
	{
		random_device rd;
		uniform_int_distribution<int> dist(0,maxN);
		return dist(rd);
	}	
}

namespace balrogonicus
{
	Balrog::Balrog():Demon()
	{
	}
	
	Balrog::Balrog(int streng,int hitpoints,string typ,string namey):Demon(streng,hitpoints,typ,namey)
	{
	}

	int Balrog::getDamage()
	{
		int damage;
		damage = Demon::getDamage();
		damage += getRandoIntegeresian(getStrength());
		return damage;
	}
	
	ostream& operator<<(ostream& outputStream, Balrog& balo)
	{
		outputStream << "Name:" << balo.getName() << "\nHP:" << balo.getHP() <<"\nStrength:" << balo.getStrength() <<"\nType:" << balo.getType() << "\n";
		return outputStream;
	}
}
