
//Balrog.h - header for balrog
#pragma once
#ifndef BALROG_H
#define BALROG_H
#include <iostream>
#include <string>
#include <vector>
#include "demon.h"
#include <random>
#include <algorithm>
using namespace std;
using namespace daemonicus;
namespace balrogonicus
{
	class Balrog:public Demon
	{
		public:
			Balrog();
			Balrog(int streng,int hitpoints,string typ,string namey);
			int getDamage();
			friend ostream& operator<<(ostream& outputStream, Balrog& elo);
	};
}
#endif //BALROG_H
