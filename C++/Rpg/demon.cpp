//Demon.cpp - Demon class
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include "creature.h"
#include "demon.h"
using namespace std;
using namespace creat;


//int getRandoIntegeresian(int maxN);
namespace
{
	
	//get random int
	int getRandoIntegeresian(int maxN)
	{
		random_device rd;
		uniform_int_distribution<int> dist(0,maxN);
		return dist(rd);
	}
}
namespace daemonicus
{
	Demon::Demon():Creature()
	{
		name="UNKNOWN";
	}
	Demon::Demon(int streng,int hitpoints,string typ,string namey):Creature(streng,hitpoints,typ),name(namey)
	{
	}
	string Demon::getName()
	{
		return name;
	}
	void Demon::setName(string namey)
	{
		name=namey;
	}
	int Demon::getDamage()
	{
		int damage;
		damage = Creature::getDamage();
		if (getRandoIntegeresian(100) < 5)
		{
			damage+=50;
		}
		return damage;
	}
	
	ostream& operator<<(ostream& outputStream, Demon& demo)
	{
		outputStream << "Name:" << demo.name << "\nHP:" << demo.getHP() <<"\nStrength:" << demo.getStrength() <<"\nType:" << demo.getType() << "\n";
		return outputStream;
	}
}

