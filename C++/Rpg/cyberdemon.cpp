
//cyberdemon.cpp - Cyberdemon class
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include "demon.h"
#include "cyberdemon.h"
using namespace std;
using namespace daemonicus;

namespace
{
//get random int
	int getRandoIntegeresian(int maxN)
	{
		random_device rd;
		uniform_int_distribution<int> dist(0,maxN);
		return dist(rd);
	}	
}

namespace cyberdaemononicus
{
	Cyberdemon::Cyberdemon():Demon()
	{
	}
	
	Cyberdemon::Cyberdemon(int streng,int hitpoints,string typ,string namey):Demon(streng,hitpoints,typ,namey)
	{
	}

	int Cyberdemon::getDamage()
	{
		int damage;
		damage = Demon::getDamage();
		return damage;
	}
	
	ostream& operator<<(ostream& outputStream, Cyberdemon& cylo)
	{
		outputStream << "Name:" << cylo.getName() << "\nHP:" << cylo.getHP() <<"\nStrength:" << cylo.getStrength() <<"\nType:" << cylo.getType() << "\n";
		return outputStream;
	}
}
