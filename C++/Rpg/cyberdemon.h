

//Cyberdemon.h - header for cyberdemon
#pragma once
#ifndef CYBERDEMON_H
#define CYBERDEMON_H
#include <iostream>
#include <string>
#include <vector>
#include "demon.h"
#include <random>
#include <algorithm>
using namespace std;
using namespace daemonicus;
namespace cyberdaemononicus
{
	class Cyberdemon:public Demon
	{
		public:
			Cyberdemon();
			Cyberdemon(int streng,int hitpoints,string typ,string namey);
			int getDamage();
			friend ostream& operator<<(ostream& outputStream, Cyberdemon& elo);
	};
}
#endif //CYBERDEMON_H
