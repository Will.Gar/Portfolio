//Creature.h"- Creature class for DND-esque game
//by Will Gardner
#pragma once
#ifndef CREATURE_H
#define CREATURE_H


#include <iostream>
#include <string>
#include <vector>

using namespace std;
namespace creat
{
	class Creature
	{
		public: 
			Creature();
			Creature(int streng,int hitpoints,string typ);
			int getStrength();
			int getHP();
			string getType();
			void setStrength(int stren);
			void setHP(int hitpoints);
			void setType(string typ);
			int getDamage();
		private:
			int strength;
			int harrypotter; // H.P.
			string type;
	
	};
} //end creat

#endif // CREATURE_H
