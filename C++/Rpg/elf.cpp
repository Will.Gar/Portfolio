//Elf.cpp - Elf class
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include "creature.h"
#include "elf.h"
using namespace std;
using namespace creat;

namespace
{
//get random int
	int getRandoIntegeresian(int maxN)
	{
		random_device rd;
		uniform_int_distribution<int> dist(0,maxN);
		return dist(rd);
	}
}

namespace elfonicus
{
	Elf::Elf():Creature()
	{
		name="UNKNOWN";
	}
	Elf::Elf(int streng,int hitpoints,string typ,string namey):Creature(streng,hitpoints,typ),name(namey)
	{
	}
	string Elf::getName()
	{
		return name;
	}
	void Elf::setName(string namey)
	{
		name=namey;
	}
	int Elf::getDamage()
	{
		int damage;
		damage = Creature::getDamage();
		if (getRandoIntegeresian(9) == 0)
		{
			cout << "DOUBLE DAMAGE TIME!"<<endl;
			damage *= 2;
	
		}
		return damage;
	}
	
	ostream& operator<<(ostream& outputStream, Elf& elo)
	{
		outputStream << "Name:" << elo.name << "\nHP:" << elo.getHP() <<"\nStrength:" << elo.getStrength() <<"\nType:" << elo.getType() << "\n";
		return outputStream;
	}
}
