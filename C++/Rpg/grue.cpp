
//cyberdemon.cpp - Grue class
#include <iostream>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include "demon.h"
#include "grue.h"
using namespace std;
using namespace daemonicus;

namespace
{
//get random int
	int getRandoIntegeresian(int maxN)
	{
		random_device rd;
		uniform_int_distribution<int> dist(0,maxN);
		return dist(rd);
	}	
}

namespace gru
{
	Grue::Grue():Demon()
	{
	}
	
	Grue::Grue(int streng,int hitpoints,string typ,string namey):Demon(streng,hitpoints,typ,namey)
	{
	}

	int Grue::getDamage()
	{
		int damage;
		damage = Demon::getDamage();
		if(getRandoIntegeresian(3))
		{
			cout << "ULTRA CRITICAL NETHER DIMENSION SCRATCH!";
			damage+=getRandoIntegeresian(10000);
		}
		return damage;
	}
	
	ostream& operator<<(ostream& outputStream, Grue& grue)
	{
		outputStream << "Name:" << grue.getName() << "\nHP:" << grue.getHP() <<"\nStrength:" << grue.getStrength() <<"\nType:" << grue.getType() << "\n";
		return outputStream;
	}
}
