
//Elf.h - header for elf
#pragma once
#ifndef ELF_H
#define ELF_H
#include <iostream>
#include <string>
#include <vector>
#include "creature.h"
#include <random>
#include <algorithm>
using namespace std;
using namespace creat;
namespace elfonicus
{
	class Elf:public Creature
	{
		public:
			Elf();
			Elf(int streng,int hitpoints,string typ,string namey);
			int getDamage();
			string getName();
			void setName(string namey);
			friend ostream& operator<<(ostream& outputStream, Elf& elo);
		private:
			string name;
	};
}
#endif //ELF_H
