

//Human.h - header for human
#pragma once
#ifndef HUMAN_H
#define HUMAN_H
#include <iostream>
#include <string>
#include <vector>
#include "creature.h"
#include <random>
#include <algorithm>
using namespace std;
using namespace creat;
namespace sapien
{
	class Human:public Creature
	{
		public:
			Human();
			Human(int streng,int hitpoints,string typ,string namey);
			int getDamage();
			string getName();
			void setName(string namey);
			friend ostream& operator<<(ostream& outputStream, Human& hume);
		private:
			string name;
	};
}
#endif //HUMAN_H
