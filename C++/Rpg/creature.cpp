//Creature.cpp- Creature class for DND-esque game
//by Will Gardner #pragma once

#include <iostream>
#include <string>
#include <vector>
#include "creature.h"
#include <random>
#include <algorithm>
using namespace std;

namespace
{
	
	//get random int
	int getRandoIntegeresian(int maxN)
	{
		random_device rd;
		uniform_int_distribution<int> dist(0,maxN);
		return dist(rd);
	}
}
namespace creat
{
	Creature::Creature()
	{
		strength=0;
		harrypotter=0;
		type="UNKNOWN";
	}
	Creature::Creature(int streng,int hitpoints,string typ)
	{
		strength=streng;
		harrypotter=hitpoints;	
		type=typ;
	}
	int Creature::getStrength()
	{
		return strength;
	}
	int Creature::getHP()
	{
		return harrypotter;
	}
	string Creature::getType()
	{
		return type;
	}
	void Creature::setHP(int hitpoints)
	{
		harrypotter=hitpoints;	
	}
	void Creature::setStrength(int stren)
	{
		strength=stren;
	}
	void Creature::setType(string typ)
	{
		type=typ;
	}
	int Creature::getDamage()
	{
		int damage;
		damage = getRandoIntegeresian(strength);
		return damage;
	}
}

