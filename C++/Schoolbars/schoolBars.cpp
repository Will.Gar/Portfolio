/**schoolbars.cpp - calculates the proceeds of candy sales based on the number of boxes sold and individual candy sales
 * Will Gardner
 * CPT 223 
 */

#include <iostream>
#include <iomanip>
#include <cmath>
using namespace std;



int gInt(string promptTxt);
double gDouble(string promptTxt);
void getData(int& numberOfBoxes, double& barPrice);
double calcRev(int numberOfBoxes,double barPrice,int barsInBox,double& sales,double stdntGovTax,double pricePerBox, int candyInBox);
void dispResults(double contributionToGov, double revenue,double sales);

int main ()
{
	//Vars
	const int CANDY_IN_BOX = 12;//number of candy bars in box
	const double PRICE_PER_BOX = 5; //price at cost of each box
	const double PORTION_STOLEN_BY_STUDENT_GOVERNMENT = .1; //amount donated to student government
	
	int numberOfBars; //number of candy bars overall
	int numberOfBoxes; //number of boxes sold
	double barPrice; //invidual bar price
	double sales; // sales overall
	double revenue;

	getData(numberOfBoxes,barPrice);
	revenue = calcRev(numberOfBoxes,barPrice,barPrice,sales,PORTION_STOLEN_BY_STUDENT_GOVERNMENT,PRICE_PER_BOX,CANDY_IN_BOX);
	dispResults(PORTION_STOLEN_BY_STUDENT_GOVERNMENT,revenue,sales);	
	
}


/**gDouble -- get double for invidual bar price
 * input -- prompt text requested user for double value
 * output -- double value parsed from input
 */
double gDouble(string promptTxt)
{
	double returnDbl;
	cout << promptTxt;
	cin >> returnDbl;
	return returnDbl;
}
/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
/**getData -- void that prompts for user data
 * input -- 	int numberOfBoxes - number of boxes altogether
 * 		double barPrice - bar price
 * output -- none
 */
void getData(int& numberOfBoxes, double& barPrice)
{
	numberOfBoxes = gInt("Enter the number of Boxes sold:");
	barPrice = gDouble("Enter the invidual price of each bar:");
}

/**calcRev -- calculates and returns revenue based on parameters given
 *  input -- 	int numberOfBoxes - number of boxes altogether
 *  		double barPrice - bar price
 *  		int barsInBox - number of bars in box
 *  		double sales - total sales 
 *  output -- 	revenue - overall revenue
 */

double calcRev(int numberOfBoxes,double barPrice,int barsInBox,double &sales,double stdntGovTax,double pricePerBox, int candyInBox)
{
	sales = (double)(numberOfBoxes * barsInBox) * barPrice;
	return (sales*(1.0-stdntGovTax)-(pricePerBox*numberOfBoxes));
	
}

/**dispResults -- displays results of calculations
 *input	--	double contributionToGov
 * 		double revenue
 *		double sales
 */
void dispResults(double contributionToGov, double revenue,double sales)
{
	cout.precision(2);
	cout << "TOTAL SALES: $" << fixed << sales << endl << "TOTAL CONTRIBUTION TO STUDENT GOVERMENT: $" << fixed << (sales*(contributionToGov)) << endl << "TOTAL REVENUE: $" << fixed << revenue << endl;	
}
