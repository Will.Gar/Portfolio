#pragma once
#include <iostream>
#include <cstdlib>

#ifndef SECURITY_H
#define SECURITY_H

namespace Sec
{
	using namespace std;
	class Security
	{
		public:
			Security();
			bool validate(string inpUser,string inpPass);
			int index(string inpUser,string inpPass);
			~Security();
		private:
			string **pwList = new string*[10];
			const size_t size = 10;
	};
}

#endif //SECURITY_H
