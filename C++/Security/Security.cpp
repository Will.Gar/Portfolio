//Security.cpp - Used to check a static set of unsafely stored usernames and passwords

#include <iostream>
#include "Security.h"



namespace Sec 
{
	using namespace std;
 	Security::Security()
	{
		for (size_t i = 0; i < size;i++)
		{
			*(pwList+i)=new string[2];
		}
		*(pwList) = new string[2]{"AzureDiamond","Hunter2"};
		*(pwList+1)=new string[2]{"MostSecureUser","Password"};
		*(pwList+2)=new string[2]{"Test","abc123"};
		*(pwList+3)=new string[2]{"Mr.Grieves","BelieveInMe"};
		*(pwList+4)=new string[2]{"Guybrush","Stump"};
		*(pwList+5)=new string[2]{"George","cats4ever"};
		*(pwList+6)=new string[2]{"Jeffrey","TheDudeAbidez"};
		*(pwList+7)=new string[2]{"Winter","ImOnMyWay"};
		*(pwList+8)=new string[2]{"Will","SpentWay2MuchTimeComingUpWithBadPasswords"};
		*(pwList+9)=new string[2]{"Chrono","JustWokeUp93"};
	}
	bool Security::validate(string inpUser,string inpPass)
	{
		for (size_t i = 0; i < size;i++) 
		{
			if((*(*(pwList+i))==inpUser)&&(*(*(pwList+i)+1)==inpPass))
				return true;
		}
		return false;
	}

	int Security::index(string inpUser,string inpPass)
	{
		for (size_t i = 0; i < size;i++) 
		{
			if((*(*(pwList+i))==inpUser)&&(*(*(pwList+i)+1)==inpPass))
				return i;
		}
		return -1;
	}
	
	Security::~Security()
	{
		delete [] pwList;
	}
}
