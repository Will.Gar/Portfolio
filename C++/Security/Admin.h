#pragma once

#ifndef ADMIN_H
#define ADMIN_H

namespace Administrator
{
	using namespace std;
	class Admin
	{
		public:
			bool isAdministrator(size_t index);
			Admin();
		private:
			bool *rights = new bool[10]{true,false,false,false,false,false,false,true,true,false};
;
			const size_t size = 10;
	};
}

#endif //ADMIN_H
