#pragma once

#ifndef USER_H
#define USER_H

#include <iostream>
#include "Admin.h"
#include "Security.h"

namespace userspace
{
	using namespace std;
	using namespace Administrator;
	using namespace Sec;
	class User
	{
		public:
			bool checkLoginValidity();
			User();
			void reentry();
			User(string inpUser,string inpPass);
			bool userType();
		private:
			string userName;
			string userPassword;
	};

}
#endif //USER_H
