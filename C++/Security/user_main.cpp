#include "User.h"
using namespace std;
using namespace userspace;
int main()
{
	User usr;
	while (not usr.checkLoginValidity())
	{
		cout << "LOGIN INVALID. TRY AGAIN" << endl;
		usr.reentry();
	}
	if(usr.userType())
	{
		cout << "ADMIN ACCESS GRANTED";
	}
	else
	{
		cout << "USER ACCESS GRANTED";
	}
	return 0;
}
