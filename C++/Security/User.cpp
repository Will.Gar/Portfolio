
#include "Security.h"
#include "Admin.h"
#include "User.h"
#include <iostream>


namespace
{
	using namespace std;
	//gString-get string value from user input
	//input-	promptTxt- prompt to request user input
	//output	user input
	string gString(string promptTxt)
	{
		string  returnStr;
		cout << promptTxt;
		cin >> returnStr;
		return returnStr;
	}
}

namespace userspace
{	
	using namespace std;
	using namespace Sec;
	using namespace Administrator;
	User::User()
	{
		userName=gString("Enter Username:");
		userPassword=gString("Enter Password:");
	}

	User::User(string inpUser,string inpPass)
	{
		userName=inpUser;
		userPassword=inpPass;
	}

	void User::reentry()
	{
		userName=gString("Enter Username:");
		userPassword=gString("Enter Password:");	
	}
	
	bool User::userType()
	{
		Admin adm;
		Security sec;
		int dex=sec.index(userName,userPassword);
		cout << adm.isAdministrator(0);
		if (dex >= 0)
		{
			return adm.isAdministrator((size_t)dex);
		}
		else
		{
			cout << "Login Invalid";
		}
		return false;
	}

	bool User::checkLoginValidity()
	{
		
		Security sec;
		return sec.validate(userName,userPassword);
	}
};
