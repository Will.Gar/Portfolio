/*
 *Will Gardner
 *CSC 202
 * BabylonianRoot.cpp - takes input of a number to be rooted, a best guess, and returns the roo
 *
*/
#include <iostream> // defines the standard input/output stream objects
#include <iomanip>// manipulates the output of c++ programs
using std::cin; // cin -- takes in input
using std::cout; // cout -- outputs data
using std::endl; // endl -- outputs endline 
using std::ios; // ios -- handles set precision

/*
 * was unclear on whether program itself was to handle the guessing and I decided to make it modular
*/

/* getGuessV1 - takes double value n and devides by 2
 * input n - number to be divided by 2 
 * output guess - return number
*/
double getGuessV1(double n){
	double guess;
	guess = n/2;
	return guess;
}
/*
 * getGuessV2 - takes double and requests input from user and returns a double casted from that input
 * input n - number displayed in input request
 * output guess - return value
*/
double getGuessV2(double n){
	double guess;
	cout << "enter your best guess of the root of "<< n <<" (Half is "<< (n/2) <<")" <<":";
	cin >> guess;
	return guess;
}

/*
 * testGuess -- tests if guess is the root of n. if not it returns half of ((n/guess)+guess)
 * input n - number that will ultimately be rooted
 * input guess - guess at the root of that number
 * output guess - either original output or (.5)*((n/guess)+guess)
 */
double testGuess(double n,double guess){
	if ((guess*guess)==n){
		return guess;
	}else
		return ((.5)*((n/guess)+guess));
		
}

int main(){
	double n; //root number
	double guess; //best guess

	cout << "Welcome to the Babylonian Square Root calculator! \n"; 
	cout << "Please enter a number to be square rooted:";
	cin >> n;
	guess = getGuessV2(n);
	guess = testGuess(n,guess);
	guess = testGuess(n,guess);	
	guess = testGuess(n,guess);	
	guess = testGuess(n,guess);	
	guess = testGuess(n,guess);
	
	cout << "the root is approximately ";
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);	
	cout.precision(2);
	cout << guess; 
	cout << "." <<endl;
	return 0;
}



