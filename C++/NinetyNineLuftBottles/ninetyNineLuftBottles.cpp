/** ninetynineLuftBottles.cpp - recursively sings ninety nine bottles of beer on the wall
 * Will Gardner
 * 9/22/17
 */

#include <iostream>
using namespace std;

//onesPlaceWord - takes num and returns word of number in ones place
//input-	num- number between 1 and 9
//output- 	converted number between zero and nine
string onesPlaceWord(int num)
{
	switch(num)
	{
		case 0:
			return "zero";
		case 1:
			return "one";
		case 2:
			return "two";
		case 3:
			return "three";
		case 4:
			return "four";
		case 5:
			return "five";
		case 6:
			return "six";
		case 7:
			return "seven";
		case 8:
			return "eight";
		case 9:
			return "nine";
	}
}
//tensPlaceWord - using number between 1 and 19 a word is returned
//input-	 a number between 1 and 10
//output-	a word representing that number
string tensPlaceWord(int num)
{
	string numStr="";
	switch(num)
	{
		case 2:
			return "twenty";
		case 3:
			return "thirty";
		case 4:
			return "forty";
		case 5:
			return "fifty";
		case 6:
			return "sixty";
		case 7:
			return "seventy";
		case 8:
			return "eighty";
		case 9:
			return "ninety";
		case 10:
			return "ten";
		case 11:
			return "eleven";
		case 12:
			return "twelve";
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
			if(num == 13)
				numStr="thir";
			else if(num  == 15)
				numStr="fif";
			else
				numStr=onesPlaceWord((num%10));
			numStr+="teen";
			return numStr;
	}
}
//num2word - takes int and returns word value between 99 and 0
// input-	num- number to be converted
// output-	convert number to word
string num2Word(int num)
{
	string numStr="";
	if  (num <= 9)
		return onesPlaceWord(num);
	else if ((num >= 10)&&(num <20))
		return tensPlaceWord(num);
	else if (num % 10 == 0)
		return tensPlaceWord(int(num/10));
	else
		numStr = tensPlaceWord(int(num/10)) + "-" + onesPlaceWord(int(num % 10));
		return numStr;
}

int main()
{
	string bottleStr=" bottles";
	for(int i = 99; i >= 0; i--)
	{
		if(i==0)
		{
			cout << num2Word(0) << " bottles of beer on the wall";
		}
		else
		{
			if(i == 1)
				bottleStr= " bottle";
			cout << num2Word(i) << bottleStr << " of beer on the wall." << endl;
			cout << num2Word(i) << bottleStr << " bottles of beer." << endl;
			cout << "take one down pass it around..." << endl;
		}	
	}
}
