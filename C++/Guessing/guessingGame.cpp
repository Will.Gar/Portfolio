//guessingGame.cpp--game in which players (both computer and human guess a number between 1 and 100)
//
#include <iostream>
#include <fstream>
#include <string>
#include "GuessingPlayer.h"
#include <random>
#include <algorithm>
#include "DumbComputerPlayer.h"
#include "SmartComputerPlayer.h"
#include "HumanPlayer.h"
#include <vector>
using namespace std;
using namespace gPlaya;
using namespace sCPlaya;
using namespace dCPlaya;
using namespace hPlaya;

//4 classes


//human
	//play
		//gets input via cin
		//cout suggested range of upper and lower with prompt but user can guess any number
	//commentOnLastGuess
	//playerDescription
		//if "simon" cout >> "hes from space"
//smartComputer
	//play
		// lastGuess=random number between upper and lower
	//commentOnLastGuess 
	//playerDescription
//dumbComputer
	//play	
		//tempNum random number between startUpper and startLower
	//commentOnLastGuess
	//playerDescription

//main
//cout >> "DYSANWARADTNMWARSNWTLIMATIS?:TCG v.1\n(*DO YOU STATE A NUMBER WITHIN A RANGE AND DOES THAT NUMBER MATCH WITH A RANDOMLY SELECTED NUMBER WHILE THE LATTER IS MISTAKENLY ASSUMED TO IMPLY SKILL: THE COMPUTER GAME)"
//menu
	//Player set
	//EXTRA number of players
		//enter number of players
		//tempvector=(sillyname,startupper,startlower)
		//add to vector of players
	//if computer is set
		//difficulty
	//set number range
	
//preplay
	//random number is set
	//comments on each contestant
		//players[i].name == "simon"
			//cout << "he's from space"
//play
	//vars
	//n=0
	//gameStillHappening=true
	//while{gameStillHappening)	
		//loop through each player run players[i].update(result)
		//run players[n].play()
		//if(players[n].getLastGuess != players[n].getAnswer())
			//comment on last guess
			//n++ 
			//if n == players.size()
				//n=0
		//else
			//gameStillHappening=false
	//cout >> player[n].wins
	//loop through each play and give golf score based distance from answer


string gString(string promptTxt);
int gInt(string promptTxt);
int getRandoIntegeresian(int maxN);
int menu(vector<string> menuArr);
void printAllArrayWithCount(vector<string> arr1,int size);

int main()
{
	//Vars
	int numberOfPlayers,choice=0;
	int answer;
	int lastGuess,n;
	int maxN;
	GuessingPlayer** players;
	bool gameStillHappening=true;
	string tempName;
	vector<string> playerTypes= {"Human","Dumb Computer","Smart Computer"};
	//Title
	cout <<"DYSANWARADTNMWARSNWTLIMATIS?:TCG v.1\n(*DO YOU STATE A NUMBER WITHIN A RANGE AND DOES THAT NUMBER MATCH WITH A RANDOMLY SELECTED NUMBER WHILE THE LATTER IS MISTAKENLY ASSUMED TO IMPLY SKILL: THE COMPUTER GAME)"<< endl;	
	
	//get number answer
	maxN=gInt("Enter max number in random range:");
	answer=getRandoIntegeresian(maxN);
	cout << answer << endl;

	//playerCreationLoop
	numberOfPlayers=gInt("Enter the number of players for this round:");
	players=new GuessingPlayer*[numberOfPlayers];
	for(int i =0; i < numberOfPlayers; i++)
	{
		cout << "Choose a Player type:"<<endl;
		choice = menu(playerTypes);
		switch (choice)
		{
			case 0:
				tempName=gString("Enter Name:");
				*(players+i)=new HumanPlayer(tempName,answer,maxN,0);
				break;
			case 1:
				tempName="Hal-"+to_string(i);
				*(players+i)=new DumbComputerPlayer(tempName,answer,maxN,0);
				break;
			case 2:
				tempName="Hal-"+to_string(i);
				*(players+i)=new SmartComputerPlayer(tempName,answer,maxN,0);
				break;
		}
	}
	//player description
	cout<< "Let's Meet our contestants." << endl;
	for (int i=0;i < numberOfPlayers;i++)
	{
		players[i]->playerDescription();
	}	
	n=0;
	lastGuess=0;
	while(gameStillHappening)
	{
		for(int i=0; i <numberOfPlayers;i++)
		{
			players[i]->update(lastGuess);
		}
		
	
		
		players[n]->play();
		lastGuess=players[n]->getLastGuess();
		cout << players[n]->getName() << " guesses " << lastGuess << endl;
		if (players[n]->checkWin())
		{
			cout << players[n]->getName() << " WINS!"<< endl;
			gameStillHappening=false;
		}	
		else
		{
			players[n]->commentOnLastGuess();
		}
		++n;
		if(n==numberOfPlayers)
		{
			n=0;
		}
	}	
	cout<<"thanks for playin!"<< endl;
	return 0;
}


//get random int
int getRandoIntegeresian(int maxN)
{
	random_device rd;
	uniform_int_distribution<int> dist(0,maxN);
	return dist(rd);
}	

//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}

/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}

int menu(vector<string> menuArr)
{	
	int size=menuArr.size();
	int input=0;
	do
	{
		printAllArrayWithCount(menuArr,size);
		input = gInt("Enter Choice: ");
		if (input<0||input>=size)
			cout << "invalid option, please reenter:" << endl;
	}while(input<0||input>=size);
	return input;
}

void printAllArrayWithCount(vector<string> arr1,int size)
{
	for(int i = 0;i < size;i++)
	{
		cout << i <<". ";
		cout << arr1[i]<<endl;
	}
}
