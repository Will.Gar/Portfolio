//DumbComputerPlayer.cpp - 
#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>
#include "DumbComputerPlayer.h"
#include "GuessingPlayer.h"
using namespace gPlaya;
using namespace std;

namespace
{

//get random int
int getRandoIntegeresian(int minN,int maxN)
{
	random_device rd;
	uniform_int_distribution<int> dist(minN,maxN);
	return dist(rd);
}
	
}

namespace dCPlaya
{
	void DumbComputerPlayer::play()
	{
		setLastGuess(getRandoIntegeresian(getSLow()+1,getSUp()-1));
	}


	void DumbComputerPlayer::commentOnLastGuess()
	{
		if ((getLastGuess() > getLow()) and (getLastGuess() < getUp()))
		{
			cout <<  "Wow, this is a actually a good guess, dumb computer " << getName() << "." << "GOLD STAR!"<<endl;
		}
		else if ((getLastGuess() > getSLow()) and (getLastGuess() < getSUp()))
		{
			cout << "Way to do you best, dumb computer " << getName() << ", as lackluster as it can sometimes be." << endl;
		}
			       	
	}

	void DumbComputerPlayer::playerDescription()
	{
		cout << getName() << ":" << "Not very good at playing but we let him play to make feel a part of something."<<endl;
	}
}
