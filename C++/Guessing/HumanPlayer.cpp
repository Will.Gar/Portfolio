#include <iostream>
#include <fstream>
#include <string>
#include "HumanPlayer.h"
using namespace std;

namespace
{

	/**gInt -- get int for the number of boxes sold
	 * input -- prompt text requested user for int value
	 * output -- int value parsed from input
	 */
	int gInt(string promptTxt)
	{
		int returnInt;
		cout << promptTxt;
		cin >> returnInt;
		return returnInt;
	}
}

namespace hPlaya
{
	void HumanPlayer::play()
	{
		string tempStr="Make a guess(might I suggest between " + to_string(getLow()) + " and " + to_string(getUp()) + ":";
		setLastGuess(gInt(tempStr));
	}


	void HumanPlayer::commentOnLastGuess()
	{
		if ((getLastGuess() > getLow()) and (getLastGuess() < getUp()))
		{
			cout <<  "An admirable guess, human player" << getName() << "." << endl;
		}
		else if ((getLastGuess() > getSLow()) and (getLastGuess() < getSUp()))
		{
			cout << "Really? I expect these kind of guesses from dumb computer players, not from you." << endl;
		}
		else
		{
			cout << "So you have just given up on actually trying haven't you?";
		}
			       	
	}

	void HumanPlayer::playerDescription()
	{
		if (getName()=="simon")
			cout << "he's from space.";
		else	
			cout << getName() << ":" << "hails from Earth. Frequently is seen breathing."<<endl;
	}		
}
