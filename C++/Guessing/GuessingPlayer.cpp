//GuessingPlayer.cpp-class cpp fill for parent GuessingPlayer class

#include <iostream>
#include <string>
#include <fstream>
#include "GuessingPlayer.h" //GuessingPlayer header file

using namespace std;


namespace gPlaya
{
//player
	//--Vars--
	//upper
	//lower
	//startUpper
	//startLower
	//last guess
	//answer
	//number of guesses
	//--FUNCTIONS
	//AccsessorsAndMutes
	//virtual play
	//virtual commentOnLastGuess
	//virtual playerDescription
	//update(string result)
		//checks if number is higher or lower than guess
			//check if the lower or higher than current bounds,respectively.
				//if yes set as upper or lower

	GuessingPlayer::GuessingPlayer(): name("noname"),upper(10),lower(0),answer(0),startUpper(upper),startLower(lower),lastGuess(-1),numberOfGuesses(0)
	{
	}
	
	GuessingPlayer::GuessingPlayer(string name,int answer): name(name),upper(100),lower(0),answer(answer),startUpper(upper),startLower(lower),lastGuess(-1),numberOfGuesses(0)
	{
	}

	GuessingPlayer::GuessingPlayer(string name,int answer,int up,int low): name(name),upper(up),lower(low),answer(answer),startUpper(upper),startLower(lower),lastGuess(-1),numberOfGuesses(0)
	{
	}

	
	int GuessingPlayer::getUp() //getUp - upperBound accessor
	{
		return upper;
	}
	
	int GuessingPlayer::getLow() //getLow -lowerBound accessor
	{
		return lower;
	}
	
	void GuessingPlayer::setUp(int up) //setUp - upperBound mutator
	{
		upper=up;
	}

	void GuessingPlayer::setLow(int low) //setLow -lowerBound mutator
	{
		lower=low;
	}

	int GuessingPlayer::getSUp()//getSUp - starting upperBound accessor
	{
		return startUpper;
	}
	
	int GuessingPlayer::getSLow() //getSlow -starting lowerBound accessor
	{
		return startLower;
	}

	int GuessingPlayer::getAnswer() //getAnswer - answer accessor
	{
		return answer;
	}
	int GuessingPlayer::getNOG() //getNOG - number of guesses accessor
	{
		return numberOfGuesses;
	}
	string GuessingPlayer::getName()	//getName - name accessor
	{
		return name;
	}

	void GuessingPlayer::play()
	{
	}
	void GuessingPlayer::commentOnLastGuess()
	{
	}
	void GuessingPlayer::playerDescription()
	{
	}
	//getLastGuess -- lastGuess accsessor
	int GuessingPlayer::getLastGuess()
	{
		return lastGuess;
	}
	void GuessingPlayer::setLastGuess(int guess)
	{
		lastGuess=guess;
	}
	void GuessingPlayer::update(int guess) //updates upper or lower based on guess
	{
		//cout << (answer)<< "|";
		//cout << (guess)<< "|";
		//cout << lower<<"|";
		//cout << upper<<"|";
		if (guess > lower)
		{	
			//cout << "here";
			if((guess > answer) && (guess < upper))
			{
				//cout << "Logical";
				upper=guess;
			}
			else if (guess < answer)
			{
				//cout << "LargicCats";
				lower = guess;
			}
		}

	}

	bool GuessingPlayer::checkWin() //checks if player won;
	{
		return (answer==lastGuess);
	}
	
	GuessingPlayer& GuessingPlayer::operator=(const GuessingPlayer& gp)
	{
		upper=gp.upper;
		lower=gp.lower;
		startUpper=gp.startUpper;
		startLower=gp.startLower;
		lastGuess=gp.lastGuess;
		answer=gp.numberOfGuesses;
		name=gp.name;
		return *this;
	}
}
