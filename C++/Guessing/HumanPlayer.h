//HumanPlayer.h--header file for guessing human player class
//Will Gardner
//CSC223
#pragma once
#ifndef HUMANPLAYER_H
#define HUMANPLAYER_H

#include <iostream>
#include <fstream>
#include <string>
#include "GuessingPlayer.h"
using namespace gPlaya;
using namespace std;
namespace hPlaya
{

	class HumanPlayer:public GuessingPlayer
	{
		public:
			
			HumanPlayer():GuessingPlayer(){};
			HumanPlayer(string name, int answer):GuessingPlayer(name,answer){};
			HumanPlayer(string name, int answer,int upper, int lower):GuessingPlayer(name,answer,upper,lower){};
			
			void play(); //play() -- prompts for input within a range
			void commentOnLastGuess(); //couts comments based on validity of guess
			void playerDescription(); //couts description of human player
	};
}
#endif //HUMANPLAYER_H
