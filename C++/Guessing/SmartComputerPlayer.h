

//SmartComputerPlayer.h--header file for on par guessing computer player class
//Will Gardner
//CSC223
#pragma once
#ifndef SMARTCOMPUTERPLAYER_H
#define SMARTCOMPUTERPLAYER_H

#include <iostream>
#include <fstream>
#include <string>
#include "GuessingPlayer.h"
using namespace gPlaya;
using namespace std;
namespace sCPlaya
{

	class SmartComputerPlayer:public GuessingPlayer
	{
		public:
			
			SmartComputerPlayer():GuessingPlayer(){};
			SmartComputerPlayer(string name, int answer):GuessingPlayer(name,answer){};
			SmartComputerPlayer(string name, int answer,int upper, int lower):GuessingPlayer(name,answer,upper,lower){};
			
			void play(); //play() -- prompts for input within a range
			void commentOnLastGuess(); //couts comments based on validity of guess
			void playerDescription(); //couts description of human player
	};
}
#endif //SMARTCOMPUTERPLAYER_H


