//guessingPlayer.h--header file for guessing player class
//Will Gardner
//CSC223
#pragma once
#ifndef GUESSINGPLAYER_H
#define GUESSINGPLAYER_H

#include <iostream>
#include <fstream>
#include <string>
using namespace std;
namespace gPlaya
{

	class GuessingPlayer
	{
//player
	//--Vars--
	//upper
	//lower
	//startUpper
	//startLower
	//last guess
	//answer
	//number of guesses
	//--FUNCTIONS
	//AccsessorsAndMutes
	//virtual play
	//virtual commentOnLastGuess
	//virtual playerDescription
	//update(string result)
		//checks if number is higher or lower than guess
			//check if the lower or higher than current bounds,respectively.
				//if yes set as upper or lower
		public:
			GuessingPlayer();
			GuessingPlayer(string name,int answer);
			GuessingPlayer(string name, int answer,int upper, int lower);
			
			GuessingPlayer& operator=(const GuessingPlayer& gp);
			int getUp(); //getUp - upperBound accessor
			int getLow(); //getLow -lowerBound accessor
			void setUp(int up); //setUp - upperBound mutator
			void setLow(int low); //setLow -lowerBound mutator
			int getSUp(); //getSUp - starting upperBound accessor
			int getSLow(); //getSlow -starting lowerBound accessor
			int getAnswer(); //getAnswer - answer accessor
			int getNOG(); //getNOG - number of guesses accessor
			string getName();	//getName - name accessor
			int getLastGuess(); //getLastGuess - lastGuess accsessor
			void setLastGuess(int guess); //setLastGuess - lastGuess Mutator
			bool checkWin();//checks if player won;	
			void update(int guess); //updates upper or lower based on guess
			virtual void play(); //child play function
			virtual void commentOnLastGuess(); //outputs based on guess and child
			virtual void playerDescription(); //descriptions based on guess and child
			
		private:
			int upper; //current upper bounds of guess range
			int lower; //current lower bounds of guess range
			int startUpper; //starting upper bounds of guess range
			int startLower; //starting lower bounds of guess range
			int lastGuess; //last and current guess
			int answer; //randomly selected number between startUpper and startLower
			int numberOfGuesses; //number of guesses
			string name; //name of player

	};
}



#endif //GUESSINGPLAYER_H
