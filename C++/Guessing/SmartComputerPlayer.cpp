//SmartComputerPlayer.cpp -
#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <algorithm>
#include "SmartComputerPlayer.h"
using namespace std;

namespace
{

//get random int
int getRandoIntegeresian(int minN,int maxN)
{
	random_device rd;
	uniform_int_distribution<int> dist(minN,maxN);
	return dist(rd);
}
	
}

namespace sCPlaya
{
	void SmartComputerPlayer::play()
	{
		setLastGuess(getRandoIntegeresian(getLow(),getUp()));
	}


	void SmartComputerPlayer::commentOnLastGuess()
	{
		cout << "Way to play exactly as expected computer " << getName() << "." << endl;
	}

	void SmartComputerPlayer::playerDescription()
	{
		cout << getName() << ":" << "Optimized for playing smart. Possibly too smart."<<endl;
	}
}
