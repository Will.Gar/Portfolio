
//DumbComputerPlayer.h--header file for subpar guessing computer player class
//Will Gardner
//CSC223
#pragma once
#ifndef DUMBCOMPUTERPLAYER_H
#define DUMBCOMPUTERPLAYER_H
#include "GuessingPlayer.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
using namespace gPlaya;
namespace dCPlaya
{

	class DumbComputerPlayer:public GuessingPlayer
	{
		public:
			
			DumbComputerPlayer():GuessingPlayer(){};
			DumbComputerPlayer(string name, int answer):GuessingPlayer(name,answer){};
			DumbComputerPlayer(string name, int answer,int upper, int lower):GuessingPlayer(name,answer,upper,lower){};
			
			void play(); //play() -- prompts for input within a range
			void commentOnLastGuess(); //couts comments based on validity of guess
			void playerDescription(); //couts description of human player
	};
}
#endif //DUMBCOMPUTERPLAYER_H


