//scoreHandler.cpp - score reference for highscores
//Will Gardner 10/1/17
//CSC223

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
using namespace std;





void getData(ifstream& file,string& playerName, int& score);
int fNxInt(ifstream& file);
string fNxString(ifstream& file);
void addToAveragePool(double value, long long& pool,int& counter);
double calcAverage(long long pool, int counter);
void compareToAverage(int score, double average);
double fAverage(string filePath);
int getHighScore(string filepath,string playerName);
string gString(string promptTxt);


int main()
{
	string filePath="scores.txt";
	string playerName;
	int score;
	cout << "Score Manager"<<endl;
	playerName = gString("Enter the name of player:");
	score = getHighScore(filePath,playerName);
	if(score == 0)
	{
		cout << "Either player is pro at being terrible at this game or has never played.";
	}
	else
	{
		cout << "Score:" << score<<endl;
		cout << "Average:"<<fAverage(filePath)<<endl;
		compareToAverage(score,fAverage(filePath));
	}
	//cout << fAverage(filePath)<<endl;
	//cout << fAverage(filePath);
}
//getData -- set data as next data set
//input-	playerName - name of player and first in dataset
//		score - int representing score and second in data set
void getData(ifstream& file,string& playerName, int& score)
{
	
	playerName=fNxString(file);
	score=fNxInt(file);
}

//fNxInt- read next ifstream data as int
// input	file - stream of file
// output 	int value read from file
int fNxInt(ifstream& file)
{
	int returnInt;
	file >> returnInt;
	return returnInt;
}

//fNxString-read next fstream data as string
// input	file - stream of file
// output 	string value read from file
string fNxString(ifstream& file)
{
	string returnString;
	file >> returnString;
	return returnString;
}
//adds value to pool and sets counter
//input		value - value to add
//		pool - pool of data
//		counter - counter of data in pool
void addToAveragePool(double value, long long& pool,int& counter)
{
	pool += value;
	++counter;
}
//calculateAverage - calculates Average
//input -	pool - pool of data
//		counter - counter of data in pool
//output	average   
double calcAverage(long long pool, int counter)
{
	//cout <<pool << endl << counter;
	//cout<< endl<<counter<<endl;
	return (pool/(double)counter);
}

//loop through file and average of second string of data as an average
//input-	filepath - pathof file
//output-	double value of average
double fAverage(string filePath)
{
	ifstream inf(filePath);
	string playerName;
	int score=0;
	int counter=0;
	long long pool=0;
	double average;

	while (inf)
	{
		getData(inf,playerName,score);
		addToAveragePool(score,pool,counter);	
	}	
	average=calcAverage(pool,counter);
	return average;
}

//compareToAverage compare to value to average to score and display a result of that comparison
//input		score-value of score
//		average - value of average
void compareToAverage(int score, double average)
{
	if (score > average)
		cout << "This score is above average." << endl;
	else if (score == average)
		cout << "This score is average." << endl;
	else if (score < average)
		cout << "This score is sadly below the average." << endl;	
}
//search for playerName and return score of corresponding player
//input 	playerName nameOfPlayer
//		filepath - file of scores path
//outut 	player score
int getHighScore(string filePath,string playerName)
{
	ifstream inf(filePath);
	string tempPlayer;
	int score;
	while (inf)
	{
		getData(inf,tempPlayer,score);
		if(tempPlayer==playerName)
			return score;
	}
	return 0;
}

//gString-get string value from user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}
