#pragma once
#ifndef HOLIDAY_H
#define HOLIDAY_H
#include "Date.h"

namespace HappyHolls
{
	using namespace std;
	
	class Holiday
	{
		public:
			Holiday();
			Holiday(string dat,string holidayName);
			Holiday(int year,string holidayName,bool thanksGivingBool);//Thanksgiving constructor
			Holiday::Holiday(const Holiday& hol);
			Holiday Holiday::operator=(const Holiday& hol);
			friend ostream& operator << (ostream &output,const Holiday);
			string thanksGivingCalc(year);
		private:
			Date date;
			string holiday;
			
		
	};
}
#endif //HOLIDAY_h
