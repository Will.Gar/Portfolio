//Holiday.cpp - holiday class
#include <iostream>
#include <cctype>
#include <cstdlib>
#include "Holiday.h"
#include "Date.h"
namespace HappyHolls 
{

	using namespace std;
	Holiday::Holiday()
	{ 
		holiday="Today";
		Date date();
	}
	
	Holiday::Holiday(string dat,string holidayName)
	{
		Date tempDat(dat);
		date=tempDat;
		holiday=holidayName;
	}
	
	Holiday::Holiday(int year,string holidayName,bool thanksGivingBool)
	{
		string dateString = thanksGivingCalc(year);
		Date tempDat(dateString);
		date=tempDat;
		holiday=holidayName;
	}

	Holiday::Holiday(const Holiday& hol)
	{
		date=hol.date;
		holiday=hol.holiday;
	}
	Holiday Holiday::operator=(const Holiday& hol)
	{
		date=hol.date;
		holiday=hol.holiday;
		return *this;
	}
	string Holiday::thanksGivingCalc(int year)
	{
		Date tempDate(("11/01/"+to_string(year)));
		//cout << tempDate.weekday();
		tempDate = (tempDate + ((4-tempDate.weekday())%7)+21);
		string retString = to_string(tempDate.getMonth())+"/"+to_string(tempDate.getDay())+"/"+to_string(year);
		//cout << retString;
		//cout << (tempDate + (((4-tempDate.weekday())%7)+21));
		//cout << tempDate;
		return retString;
	}	
	ostream& operator << (ostream &output,const Holiday day)
	{
		output << "Holiday: \n" << day.holiday << "\nDate:"<< day.date << "\n";
		return output;
	}
}
