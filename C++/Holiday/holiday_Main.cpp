#include "Holiday.h"
#include <string>
#include <vector>
using namespace std;
using namespace HappyHolls;

int gInt(string promptTxt);

int main()
{
	int year = gInt("Enter Year:");
	vector <Holiday> holidays;
	Holiday *tempHol;
	cout << "here";
	tempHol= new Holiday(("02/14/" + to_string(year)),"Valentine's Day");
	holidays.push_back(*tempHol);
	//delete [] tempHol;
	tempHol= new Holiday(("12/25/"+ to_string(year)),"Christmas");
	holidays.push_back(*tempHol);
	//delete [] tempHol;
	tempHol= new Holiday(("12/31/"+ to_string(year)),"New Year's Eve");
	holidays.push_back(*tempHol);	
	//delete [] tempHol;
	tempHol=new Holiday(("1/1/"+ to_string(year)),"New Year's Day");
	holidays.push_back(*tempHol);
	//delete [] tempHol;
	tempHol= new Holiday(year,"Thanksgibbons",true);
	holidays.push_back(*tempHol);
	//delete [] tempHol;
	int size = holidays.size();
	for(int i = 0; i < size;i++)
	{
		cout << holidays[i];
	}
	return 0;
}

/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
