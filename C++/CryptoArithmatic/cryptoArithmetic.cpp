/* cryptoArithmetic.cpp --Will Gardner 9/14/2017
 * Description: take two words, assign a unique value each letter and check if 4 times the sum of word1 is equal to the sum of word2 
 * Thanks to Pierre on Stackoverflow for uppercasing
 * Included sstream from 559,to cast int as string
 */
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
#include <array>
#include <cmath>
#include <sstream>
using namespace std;

// get value at specific decimal value
int gAtPlc(int value,int decPlc,int letterCount)
{
	stringstream ss;
	string strVal;
	int retVal;
	ss << value;
	strVal = ss.str();
	retVal = ((strVal+string(letterCount-strVal.length(),'0'))[decPlc])-48;
	return retVal;
}
bool arithCheck(string word1,string word2,int letterArr[])
{
	int word1Value;
	int word2Value;
	bool flag1=true;
	int i =0;
	while ((i < 26)&&(flag1))
	{
		if (letterArr[i] > -1)
		{
			for(int k=0;k < 26;k++)

			{
				if((i != k) and (letterArr[i]==letterArr[k]) and flag1)
					flag1 = false;
			}
		}
		i++;
	}
	for(int i=0; i < word1.length();i++)
		word1Value += letterArr[(int(toupper(word1[i]))-65)];
	word1Value *= 4;
	for(int i=0; i < word2.length();i++)
		word2Value += letterArr[(int(toupper(word2[i]))-65)];
	return ((word1Value == word2Value)and flag1);
}
int gLetterCount(int letterArr[])
{
	int value =0;
	for (int i =0; i < 26;i++)
		value += (letterArr[i]>-1) ? 1:0;
	return value;
}
int gLetterMax(int letterCount)
{
	int value=0;
	for (int i = 0;i < letterCount;i++)
		value += (9*(pow(10,i)));
	return value;	
}

int main()
{
	string word1; // first word to be used in cryptArithmic equation
	string word2; // second word in the same equation
	int letterCount; // number of unique letters
	int letterMax; // Max loop value
	int counter;  //counter to letter count loops
	int letterArr[26]; //array of 26 element representing letters
	int j; //counter main while loop
	bool flag1; // bool to main while loop
	//-1 fill loop
	for (int i=0;i < 26; i++){
		letterArr[i]=-1;
	}
	// get user input
	cout << "cryptArithmetic Tester" << endl << "Enter word1:";
	cin >> word1;
	cout << "Enter word2:";
	cin >> word2;
		
	//zero fill used letters
	for (int i=0;i < word1.length();i++)
	{
		letterArr[(int(toupper(word1[i]))-65)]=0;

	}
	for(int i=0;i < word2.length();i++)
	{
	letterArr[(int(toupper(word2[i]))-65)]=0;
	}	
	
	//getters for letterMax and letterCount
	letterCount = gLetterCount(letterArr);
	letterMax = gLetterMax(letterCount);

	
	//main cryptArithmetic check Loop
	j=0;
	while (!flag1)
	{
		
		//apply values to letters
		counter = 0;
		for (int k=0;k < 26;k++)
		{
			if ((counter < letterCount) && (letterArr[k]>-1))
			{
				letterArr[k] = gAtPlc(j,counter++,letterCount);
			}
		}
		if (arithCheck(word1,word2,letterArr))
		{
			flag1=true;
			cout << "this happens";
		}
		if (j == letterMax)
		{
			flag1=true;
		}
		else
		{
			j++;
		}
	}
	
	cout << "The values of the letters in \"" << word1 << "\" and \"" <<word2<<"\" are:" << endl;
	for (int i=0;i < 26;i++)
	{ 
		if (letterArr[i] > -1)
		{
			cout << char((i+65)) << "=" << letterArr[i] << endl;

		}
	}
	return 0;	
}
