/**complex.cpp - handles the manipulation of complex numbers
 * Will Gardner
 * 10/25/17
 **/

#include <iostream>
using namespace std;

class Complex{
	public: 	
		double real;
		double imaginary;
		bool operator<(Complex comp);
		bool operator>(Complex comp);
		bool operator==(Complex comp);		
		friend ostream& operator<<(ostream& outputStream,Complex& comp);	
		friend istream& operator>>(istream& inputStream,Complex& comp);
		Complex operator*(Complex comp);
		Complex operator/(Complex comp);
		Complex operator+(Complex comp);
		Complex operator-(Complex comp);

		Complex();
		Complex(double realpart);
		Complex(double realpart,double imaginaryPart);
};



int main()
{
	Complex i = Complex(0,1);
	Complex compUno = Complex();
	Complex compDos = Complex();
	Complex result;
	cout << "Complex Number Tester!";
	cin >> compUno;
	cin >> compDos;
	result=(compUno+compDos);
	cout<<"ADD: "<<result<<endl;	
	result=(compUno-compDos);
	cout<<"SUB: "<< result<<endl;
	result=(compUno*compDos);
	cout<<"MUX: "<<result<<endl;
	cout<<"i"<<i;
	
}

//Default Constructor
Complex::Complex(){
	real=0;
	imaginary=0;
}

Complex::Complex(double realpart)
{
	real=realpart;
}

Complex::Complex(double realpart,double imaginaryPart)
{
	real = realpart;
	imaginary = imaginaryPart;
}

Complex Complex::operator+(Complex comp)
{
	Complex result;
	result.real = real+comp.real;
	result.imaginary=imaginary + comp.imaginary;
	return result;
}
Complex Complex::operator-(Complex comp)
{	
	Complex result;
	result.real = real-comp.real;
	result.imaginary=imaginary - comp.imaginary;
	return result;
}

Complex Complex::operator*(Complex comp)
{
	Complex result;
	result.real = ((real*comp.real)-(imaginary*comp.imaginary));
	result.imaginary=(real*comp.imaginary)+(comp.real*imaginary);
	return result;
}

ostream& operator<<(ostream& outputStream,Complex& comp)
{
	outputStream << comp.real << " + " << comp.imaginary<<"i"<< endl;
	return outputStream;
}


istream& operator>>(istream& inputStream,Complex& comp)
{
	cout << "Enter real portion of complex number:";
	inputStream >> comp.real;
       	cout << "Enter Imaginary portion:";
	inputStream >> comp.imaginary;
	return inputStream;
}




