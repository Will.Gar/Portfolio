#include <iostream>
#include <cmath>
using namespace std;


double gDouble(string promptTxt);
double average(double * dbl,int size);

int main(){
	
	typedef double * DoublePtr;
	DoublePtr d;
	d=new double[5];
	double avg;
		
	for (int i=0;i<5;i++)
	{
		d[i]=gDouble("enter num:");
	}

	avg = average(d,5);
	
	cout << "Average:" << avg<< endl;
	for(int i = 0;i < 5;i++)
	{
		cout << "Element value:" << d[i] << endl << "Distance from mean:" << abs((avg-d[i]));
	}
}
/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
double gDouble(string promptTxt)
{
	double * returnDbl;
	cout << promptTxt;
	cin >> *returnDbl;
	return *returnDbl;
}

double average(double* dbl,int size)
{
	double pool=0;
	for (int i = 0; i < size; i++)
	{
		pool+=dbl[i];	
	}
	return pool/size;
}
