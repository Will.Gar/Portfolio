/**fractions.cpp - allows for the manipulation of a fraction object
 *Will Gardner
 * 10/10/17*/

#include <iostream>
using namespace std;


class Polynomial
{
	public:
		Polynomial();
		Polynomial(int i);
		Polynomial(int i,bool itDoesNotMatter);
		Polynomial(Polynomial& dyne);
		void sort();
		void insert(double coefficient,int pow);
		double evaluate(double i);
		
		Polynomial& operator=(Polynomial& ymxb);
		void clear();
		double getMterm(size_t index);
		double getPow(size_t index);
		friend ostream& operator<<(ostream& outputStream, Polynomial& poly);
		Polynomial& operator*(Polynomial& ymxb);
		Polynomial& operator+(Polynomial& ymxb);
		Polynomial& operator-(Polynomial& ymxb);	
		Polynomial& operator+(const double& muadDouble);
		Polynomial& operator*(const double& muadDouble);
		Polynomial& operator-(const double& muadDouble);
		size_t getSize();
		~Polynomial();
	private:
		double **polyMXB;
		size_t size;
};

	
	double gDouble(string promptTxt);
	int gInt(string promptTxt);
	double powLoop(double x,int pow);
	void here();
	
int main(){
	cout << "POLYNOMIAL CLASS USIN' THE OL' SPARSE TECHNIQUE" << endl;
	int size1,size2;
	double ab;
	size1=gInt("choose a size for polynomial 1:");
	Polynomial poly1(size1,true);
	cout << poly1;
	size2=gInt("choose a size for polynomial 2:");
	Polynomial poly2(size2,true);
	Polynomial tempPoPo(0);
	cout << "NOW FOR THE MAIN EVENT:" <<endl<< "Poly1:" << poly1 << endl << "Poly2:" << poly2;
	cout << "here";
	tempPoPo = (poly1+poly2);
	cout << "ADDITION:" << tempPoPo << endl;
	tempPoPo = (poly1-poly2);
	cout << "SUBTRACTION:" << tempPoPo << endl;
	tempPoPo = (poly1*poly2);
	cout << "MULTIPLICATION:" << tempPoPo << endl;
	ab = gDouble("Choose an arbitrary value:");
	cout << "ADDITION:";
	tempPoPo=(poly1+ab);
	cout << tempPoPo;
       	cout << endl;
	cout << "SUBTRACTION:";
	tempPoPo=(poly1-ab);
	cout << tempPoPo;
	cout << endl;
	cout << "MULTIPLICATION:" ;
	tempPoPo=(poly1*ab);
	cout << tempPoPo;
       	cout <<	endl;
	return 0;
}




double Polynomial::evaluate(double x)
{
	double retVal=0;
	for(size_t i=0;i<size;i++)
	{
		retVal+=this->polyMXB[(int)i][0]*powLoop(x,this->polyMXB[(int)i][1]);
	}
	return retVal;
}

double powLoop(double x,int pow)
{	
	double retX=1;;
	for(int i = 0;i <abs(pow);i++)
	{
		retX*=x;
	}
	if(pow < 0)
		retX/=1/retX;
	return retX;
}

//constructor
Polynomial::Polynomial()
{
	polyMXB=nullptr;
	size = 0;
}
//constructor with input loop
Polynomial::Polynomial(int x,bool placer)
{
	polyMXB= new double*[x];
	size= x;
	double pow,coef;
	for(size_t i = 0; i < x; i++)
	{
		pow=gInt("Enter Power of coefficient:");
		coef=gDouble("Enter M term:");
		polyMXB[i]=new double[2];
		*(*(polyMXB+i))=coef;
		*(*(polyMXB+i)+1)=pow;
	}
	sort();
	cout << *(*(polyMXB+0));
	cout << *(*(polyMXB+0)+1);

}

Polynomial::Polynomial(Polynomial& dyne)
{
	polyMXB = new double*[dyne.getSize()];
	size=dyne.getSize();
	
	for(size_t i=0;i < size;i++)
	{
		*(*(polyMXB+i))=dyne.getMterm(i);
		*(*(polyMXB+i)+1)=dyne.getPow(i);
		
	}
}


Polynomial::Polynomial(int x)
{
	polyMXB= new double*[x];
	size= x;
}

//insert coeficient
void Polynomial::insert(double mterm,int pow)
{
	//here();
	double** tempMX= new double*[size+1];
	for (size_t i=0;i<(size+1);i++)
	{
		*(tempMX+i)=new double(2);
		*(*(tempMX+i))=*(*(polyMXB+i));
		*(*(tempMX+i)+1)=*(*(polyMXB+i)+1);
	
		*(*(tempMX+size))=mterm;
		*(*(tempMX+size)+1)=pow;
		
		cout << "adding: " << *(*(tempMX)) << "|" << pow;
	}
	++size;
	//delete [] polyMXB;
	polyMXB=new double*[size];
	for(size_t i = 0;i < size;i++)
	{
		*(polyMXB+i)=new double(2);
		*(*(polyMXB+i))=*(*(tempMX+i));
		*(*(polyMXB+i)+1)=*(*(tempMX+i)+1);
	}
}

void here()
{
	cout << "here";
}
//addition
//check  for like terms, then loops through an adds coeficients or single coeficient and 0 coe
Polynomial& Polynomial::operator+(Polynomial& dyne)
{
	//cout << "here";
	//cout << dyne;
	Polynomial tempPoly(0);
	double mterm1,mterm2;
	bool exist=false;
	
	for(size_t i=0; i< size;i++)
	{
		mterm1=*(*(polyMXB+i));
		exist=false;
		//check for like pow term in second
		for(size_t j=0;j <dyne.getSize();j++)
		{
			if (*(*(polyMXB+i)+1)==dyne.getPow(i))
			{
				mterm2 = dyne.getMterm(i);
				exist=true;
				i=dyne.getSize();
			}
		}
		if(not exist)
		{	
			mterm2 = 0;
		}

		if((mterm1+mterm2)!=0)
		{
			tempPoly.insert((mterm1+mterm2),(int)(*(*polyMXB+i)+1));
		}

	}
	for(size_t i=0;i<dyne.getSize();i++)
	{
		exist=false;
		//loop tempPoly
		for(size_t j=0;j<tempPoly.getSize();j++)
		{
			if(dyne.getPow(i)==tempPoly.getPow(j))
			{
				exist=true;
				j=tempPoly.getSize();
			}
		}
		if(not exist)
		{	
			cout << dyne.getMterm(i) << dyne.getPow(i);
			tempPoly.insert(dyne.getMterm(i),dyne.getPow(i));
		}
	}
	cout << tempPoly;
	*this=tempPoly;
	return *this;

}

//subtraction
//check for like terms, levels size then loops through an substracts coefiecien or 0 and coeficient
Polynomial& Polynomial::operator-(Polynomial& dyne)
{
	Polynomial tempPoly1;
	double mterm1,mterm2;
	bool exist=false;
	for(size_t i=0; i< size;i++)
	{
		mterm1=polyMXB[i][0];
		exist=false;
		//check for like pow term in second
		for(size_t j=0;j <dyne.getSize();j++)
		{
			if (polyMXB[i][1]==dyne.getPow(i))
			{
				mterm2 = dyne.getMterm(i);
				exist=true;
				i=dyne.getSize();
			}
		}
		if(not exist)
		{	
			mterm2 = 0;
		}

		if((mterm1-mterm2)!=0)
		{
			tempPoly1.insert((mterm1-mterm2),this->polyMXB[i][1]);
		}

	}
	for(size_t i=0;i<dyne.getSize();i++)
	{
		exist=false;
		//loop tempPoly
		for(size_t j=0;j<tempPoly1.getSize();j++)
		{
			if(dyne.getPow(i)==tempPoly1.getPow(j))
			{
				exist=true;
				j=tempPoly1.getSize();
			}
		}
		if(exist)
		{
			tempPoly1.insert((-dyne.getMterm(i)),dyne.getPow(i));
		}
	}
	*this=tempPoly1;
	return *this;
}

void Polynomial::clear()
{
	this->polyMXB = new double*[0];
	size = 0;
}

size_t Polynomial::getSize()
{
	return size;
}

double Polynomial::getMterm(size_t index)
{
	return *(*(polyMXB+index));
}
double Polynomial::getPow(size_t index)
{
	return *(*(polyMXB+index)+1);
}
//multiplication
//loops through primary polynomial object, creates temporary polynomial with b terms muxed by b element and exponenent added by exponent element.
//This temp  polynomial is added(mathematical) to a temporary static polynomial(can use addition function)!
//
Polynomial& Polynomial::operator*(Polynomial& dyne)
{
	Polynomial tempPolynomial;
	Polynomial retPolynomial;
	for(size_t i=0;i<size;i++)
	{
		tempPolynomial.clear();
		
		for(size_t j=0;j<dyne.getSize();j++)
		{
			tempPolynomial.insert((dyne.getMterm(j)*polyMXB[i][0]),(dyne.getPow(j)+polyMXB[i][1]));
		}
		retPolynomial = (retPolynomial+tempPolynomial);
	}
	*this=retPolynomial;
	return *this;
}

//removing coefficient

//sort by exponent
void Polynomial::sort()
{
	double y,d;
	for (size_t i = 0;i < size-1;i++)
	{
		for(size_t j=0;j<size-1-i;j++)
		{
			if(*(*(polyMXB+j+1)+1) > *(*(polyMXB+j)+1))
			{
				y=*(*(polyMXB+j));
				d=*(*(polyMXB+j)+1);
				*(*(polyMXB+j))=*(*(polyMXB+j+1));
				*(*(polyMXB+j)+1)=*(*(polyMXB+j+1)+1);
				*(*(polyMXB+j+1))=y;
				*(*(polyMXB+j+1)+1)=d;
			}
		}
	}
}



Polynomial& Polynomial::operator+(const double& muadDouble) 
{
	Polynomial tempPoly,tempPoly2;
	tempPoly2=*this;
	tempPoly.insert(muadDouble,0);
	tempPoly = (tempPoly + tempPoly2);
	*this=tempPoly;
	return *this;
}
Polynomial& Polynomial::operator-(const double& muadDouble) 
{
	Polynomial tempPoly,tempPoly2;
	tempPoly2=*this;
	tempPoly.insert(muadDouble,0);
	tempPoly = (tempPoly - tempPoly2);
	*this=tempPoly;
	return *this;
}

Polynomial& Polynomial::operator*(const double& muadDouble) 
{
	Polynomial tempPoly,tempPoly2;
	tempPoly2=*this;
	tempPoly.insert(muadDouble,0);
	tempPoly = (tempPoly * tempPoly2);
	*this = tempPoly;
	return *this;
}
/*
const Polynomial& double::operator+(const Polynomial& poly) const
{
	Polynomial tempPoly=Polynomial();
	tempPoly.insert(this,0);
	tempPoly = tempPoly + poly;
	return tempPoly;
}

const Polynomial& double::operator-(const Polynomial& poly) const
{
	Polynomial tempPoly=Polynomial();
	tempPoly.insert(this,0);
	tempPoly = tempPoly - poly;
	return tempPoly;
}

const Polynomial& double::operator*(const Polynomial& poly) const
{
	Polynomial tempPoly=Polynomial();
	tempPoly.insert(this,0);
	tempPoly = tempPoly * poly;
	return tempPoly;
}
*/
//destructor
Polynomial::~Polynomial()
{
	delete [] polyMXB;
}

//overloaded =

Polynomial& Polynomial::operator=(Polynomial& dyne)
{
	if(size != dyne.getSize())
	{
		polyMXB = new double*[dyne.getSize()];
		size=dyne.getSize();
	}
	for(size_t i=0;i < size;i++)
	{
		*(polyMXB+i)=new double[2];
		*(*(polyMXB+i))=dyne.getMterm(i);
		*(*(polyMXB+i)+1)=dyne.getPow(i);
		

	}
	return *this;
}

//
//display coef
ostream& operator<<(ostream& outStream, Polynomial& dyne)
{
cout << "<"<<dyne.getSize()<<">";
	//xterm
	string xterm,dispString;
	
	for(size_t i=0;i < dyne.getSize();i++)
	{
		//cout << "here" << i;	
		if((dyne.getPow(i)>1)||(dyne.getPow(i)<0))
			 xterm=("x^"+to_string(dyne.getPow(i)));
		if(dyne.getPow(i)==1)
			xterm="x";
		if(dyne.getPow(i)==0){
			xterm="";
		}
		dispString+=(to_string(dyne.getMterm(i))+xterm);
		if (i < dyne.getSize()-1)
			dispString+="+";
	}
	//cout << dispString;
	outStream << dispString;
	return outStream;
}

double gDouble(string promptTxt)
{
	double returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
