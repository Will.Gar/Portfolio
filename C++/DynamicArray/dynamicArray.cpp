//Peter Houtkin dynamicArray.cpp 11/09/17
//Chapter 10 problem 6
/**DynamicArray.cpp - rewrite of  vector class for fun.
*/
#include <iostream>
#include <cstddef>
#include <string>
using namespace std;

class DynamicArray{
public:
	DynamicArray();
	DynamicArray(int size);
	void addEntry(string str);
	void deleteEntry(string str);
	string getEntry(int index);
	DynamicArray(DynamicArray& dyne);
	void clear();
	size_t size();
	~DynamicArray();
	DynamicArray& operator=( DynamicArray& dyne);
	string operator[](int index);
	//DynamicArray operator=(DynamicArray arr1);

private:
	string *dynamicStringArray;
	size_t len;
};




string gString(string promptTxt);
int gInt(string promptTxt);

int main()
{
	int length=0;
	size_t index=0;
	string letedStr;
	cout << "preemptively enter string to delete:";
	//cin >> letedStr;
	letedStr="wlp";
	cout << "test of the subjectively improved \"vektor\"!"<<endl;
	length = gInt("Enter the size of the string vektor:");
	DynamicArray dyne;
	for(int i = 0; i < length; i++)
	{
		dyne.addEntry(gString(("Populate string " + to_string(i) + ":")));
	}

	dyne.deleteEntry(letedStr);
	DynamicArray copy(dyne);
	index=gInt("enter index of element to display:");
	cout << "element " << index<< ":" << copy.getEntry(index)<< endl;
	cout << "Displaying all..." << endl;
	for(size_t i = 0; i < dyne.size();i++)
	{
		cout << "Element " << i << ":"<<dyne[i]<< endl;
	}

}
/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt=0;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}


//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr="";
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}


DynamicArray::DynamicArray() 
	: len(0)
{
	dynamicStringArray = nullptr;
}

DynamicArray::DynamicArray(int size)
{
	len=size;
	this->dynamicStringArray = new string[size];
}

DynamicArray::DynamicArray(DynamicArray& dyne)
{
	len=dyne.size();
	this->dynamicStringArray= new string[len];
	for(size_t i=0;i < len;i++)
	{
		this->dynamicStringArray[i]=dyne[i];
	}
}
DynamicArray::~DynamicArray()
{
	delete [] dynamicStringArray;
}

void DynamicArray::deleteEntry(string str)
{	
	string *tempDynamicStringArray = new string[len-1];

	bool match = false;
	if (len > 0)
	{
		int index;
		for (size_t i = 0; i < len; i++)
		{
			if (dynamicStringArray[i] == str)
			{
				match = true;
				index = i;
			}
		}

		if (match == true)
		{
			size_t j = 0;
			int k = 0;
			while(j < (len-1))
			{
				if (j = index)
				{
					k = 1;
				}
				tempDynamicStringArray[j] = this->dynamicStringArray[j + k];
				j++;
			}
			delete [] dynamicStringArray;
			this->dynamicStringArray=new string[len+1];
			for(size_t i=0;i < len;i++)
			{
				this->dynamicStringArray[i]=tempDynamicStringArray[i];
			}

			//dynamicStringArray = tempDynamicStringArray;
			//*tempDynamicStringArray=new string;
			delete [] tempDynamicStringArray;
			len--;
			cout<< "here"<<endl;
		}
	}
}


void DynamicArray::addEntry(string str)
{
	string *tempStringArray = new string[len + 1];
	//string tempEntry=str;
	for (size_t i = 0; i < len; i++)
	{
		tempStringArray[i] = this->dynamicStringArray[i];
		//cout << tempStringArray[0];
	}
	delete [] dynamicStringArray;
	//delete [] this->dynamicStringArray;
	this->dynamicStringArray=new string[len+1];
	for(size_t i=0;i < len;i++)
	{
		this->dynamicStringArray[i]=tempStringArray[i];
	}
	
	dynamicStringArray[len] = str;
	//cout << "here";
	//tempStringArray = new string[0];
	//cout << "here";
	delete []  tempStringArray;
	//cout << "here";
	len++;
}


void DynamicArray::clear() 
{
	this->dynamicStringArray = new string[0];
}

string DynamicArray::getEntry(int index)
{
	if((index >= 0) && (index < len))
	{
		return dynamicStringArray[index];
	}
	else
	{
		cout << "NICE TRY. BUT PLEASE REFERENCE VALID MEMORY LOCATION" << endl;
		return nullptr;
	}
}
DynamicArray& DynamicArray::operator=(DynamicArray& dyne)
{

	if (len != dyne.size())
	{
		delete [] dynamicStringArray;
		this->dynamicStringArray = new string[len];
	}
	//dynamicStringArray=new string[len];
	for(size_t i=0;i < len;i++)
	{
		this->dynamicStringArray[i]=dyne[i];
	}
	return *this;
}	

string DynamicArray::operator[](int index)
{
	if((index >= 0) && (index < len))
	{
		return this->dynamicStringArray[index];
	}
	else
	{
		cout << "NICE TRY. BUT PLEASE REFERENCE VALID MEMORY LOCATION" << endl;
		return nullptr;
	}
}
size_t DynamicArray::size()
{
	return len;
}
