/** gasoline
*/
#include <iostream>
using namespace std;

/** litersToGallons - conversion of liters to gallons
 * input- 	conversionRate - double value of the conversion rate of liters to gallons
 * 		liters - number of liters  to be converted
 * output-	number gallons in x amount of liters
 */
double litersToGallons(double conversionRate, int liters)
{
	return (liters * conversionRate);
}


/** mPG - calulales and returns  miles per gallon
 *input- 	gallons - number of gallons to be calculated
 *		miles - number of miles to be calculated
 *output-	miles travelled per gallon consumed
 */

double mPG(double gallons,int miles)
{
	return (miles/gallons);
}

int main()
{
	const double LITER_CONVERSION = .264719;
	int numberOfLiters; //number of liters consumed by car
	int miles; //distance travelled in miles
	double numberOfGallons; //number of gallons
	char tryAgainChar; // string for continuation request
	bool calcFlag = false; //flag to end calculation loop
	
	cout << "Gas Liter Distance calculator.";
	do 
	{ 
		//input getters
		cout << "Enter number of Liters:";
		cin >> numberOfLiters;
		cout << "Enter number of Miles:";
		cin >> miles;
		
		//calculation
		numberOfGallons = litersToGallons(LITER_CONVERSION,numberOfLiters);
		cout << "After " << miles << " miles, the car consumed "<< numberOfGallons << " gallons at a rate of " << mPG(numberOfGallons,miles) << " miles per gallon.";
		
		//continuing the loop	
		cout << "Calculate again? (y or n)";
		cin >> tryAgainChar;
		if(tryAgainChar != 'y')
		{
			calcFlag = true;
		}
	} while(!calcFlag);
	return 0;
}
