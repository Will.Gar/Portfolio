/**Words.cpp - check for the most consecutive double letters in a recursive fashion
*/

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;


//first loop -import file line by line adding checking for a single double consonant adding each true to a vector
//maybe class with number of start of initial double consonant
//successive loops check each word a step further for another set of double consonants, 
//if one should not exist code should check for:
//	another set of double consonant pairs,triplets etc is possible
//	check for another set
//if another is found an this set should be added to a new vector
//
//once new vector check is complete, new vector should be counted
//	if the new count = 0
//		old list should be returned
//	if new count > 0
//		old s\hould be disposed of
//
//design to double letter checks:
//1. one around single instances with import
//2. one around consecutive instances with second arguement for number of double letters
//
//word class
// word
// index of double letter combo instance
// size of double letter combo
class Word
{
	public:
		Word();
		Word(string w,int indexOfDbl,int scoreDbl);
		string getWord(); 
		void setindexOfDbl(int i);
		void setScore(int s);
		int getScore();
		int getIndex();
		bool doubleLetterCheck();
		bool doubleLetterCheck(int i,int size);
		
		friend ostream& operator<<(ostream& outputStream,Word w);
	private:
		string wordy;
		int index;
		int score;
};


string fNxString(ifstream& file);
void firstLoop(vector<Word> &dblLetter,ifstream& file);
void successiveLoop(vector<Word> &dblLetter,vector<Word> &newWords, int i);
void displayWordVect(vector<Word> &words);
string strip(string str);
void eraseAll(vector<Word> &words);
int main(){
	const string path = "words.txt";
	vector<Word> words, newWords;	
	int i=2;
	ifstream inf(path);
	firstLoop(words,inf);
	do {
		eraseAll(newWords);
		successiveLoop(words,newWords,i);
		if (newWords.size()>0)
		{

			eraseAll(words);
			for (int i=0;i < newWords.size();i++)
			{	
				Word tempword(newWords[i].getWord(),newWords[i].getScore(),newWords[i].getIndex());
				words.push_back(tempword);
			}
			
		}
		++i;
	}while(newWords.size()>0);
	displayWordVect(words);

}

string strip(string str)
{
	string retString;
	for(int i = 0;i < str.length();i++)
	{
		if (isalpha(str[i]))
		{
			retString+=str[i];
		}
		
	}
	return retString;
}

void eraseAll(vector<Word> &words)
{
	while (words.size() > 0)
	{
		words.erase(words.begin());
	}
}
void displayWordVect(vector<Word> &words)
{
	for(int i=0;i<words.size();i++)
	{
		cout<<words[i];
	}

}

//first loop -import file line by line adding checking for a single double consonant adding each true to a vector
//input-	dblLetterObject
//		ifStream &file
void firstLoop(vector<Word> &dblLetter,ifstream& file)
{
	string tempStr="";
	int tempInt;
	while(file){
		tempStr = strip(fNxString(file));
		Word tempWord(tempStr,0,0);
		
		if (tempWord.doubleLetterCheck()){
			dblLetter.push_back(tempWord);
			//cout<<tempWord;
		}
	}
}

//successiveLoop - check multiple consonant adding each true to a vector
//input-	dblLetter - vector of word Objects
//		newVect	- vector of word Objects
//		score - number double consonants to check for

void successiveLoop(vector<Word>& dblLetter,vector<Word>& newVect, int score)
{
	
	for(int i =0; i < dblLetter.size(); i++)
	{	
		if((dblLetter[i].doubleLetterCheck(score,dblLetter[i].getWord().size()))){
			//cout << dblLetter[i];
			Word tempWord(dblLetter[i].getWord(),dblLetter[i].getScore(),dblLetter[i].getIndex());
			newVect.push_back(tempWord);
		}
	}


}

//fNxString-read next fstream data as string
// input	file - stream of file
// output 	string value read from file

string fNxString(ifstream& file)
{
	string returnString;
	file >> returnString;
	return returnString;
}

Word::Word()
{
	wordy="";
	index=0;
	score=0;
}
		
Word::Word(string w,int indexOfDbl,int scoreDbl)
{
	wordy=w;
	index=indexOfDbl;
	score=scoreDbl;
}

string Word::getWord(){
	return wordy;
}

		
void Word::setindexOfDbl(int i)
{
	index=i;
}
void Word::setScore(int s)
{
	score=s;
}
	
bool Word::doubleLetterCheck()
{
	for(int i = 0; i <= wordy.size();i++)
	{
		if ((i!=(wordy.size()-1))&&(wordy[i]==wordy[(i+1)]))
		{
			//cout << wordy[i] << "==" << wordy[(i+1)];
			//cout << wordy.size();
			index=i;
			score=1;
			return true;
		}else if (i==(wordy.size()-1))
		{
			//cout << "False!";
			return false;
		}
	}	
	return false;
}

int Word::getScore()
{
	return score;
}
int Word::getIndex()
{
	return index;
}

bool Word::doubleLetterCheck(int maxScore,int size)
{
	//cout << "word" <<  wordy<< endl;
	int currentScore=0;
	int k=0;
	bool firstMatch = true;
	bool match =false;
	string tempword=strip(this->getWord());
	for (int i = this->getIndex(); i < ((size-(2*maxScore)-1));i++)
	{
		
		//cout << tempword[i] << endl;
		k=1;
		currentScore=0;
		do
		{
			if ((firstMatch)&&(tempword[i]==tempword[i+k]))
			{	
				currentScore=1;
				firstMatch=false;
				k+=1;
				match=true;
			}
			if ((not firstMatch) && ((i+k+1) < tempword.size())&& (tempword[i+k]==tempword[(i+k+1)]))
			{	
				//cout << tempword[i+k]<< "==" << tempword[(i+k+1)] << endl;
				currentScore+=1;
				if (currentScore==maxScore)
				{
					score=maxScore;
					index=i;
					return true;
				}
				k+=2;
			}
			else 
			{
				currentScore=0;
				firstMatch=true;
				k=1;
				match=false;
			}
				
		}while(match==true);
	}
	return false;
	
}

ostream& operator<<(ostream& outputStream,Word w)
{
	outputStream <<"Word:" <<w.getWord() << "\nNumber of double letter combos:" <<w.getScore()<<"\n";
}
