//counter.cpp - grocery counter program
//Will Gardner 10/23/17
//CSC223

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;

class HotDogStand
{
	public:
		int id;
		int dogsSold;
		string name;
		HotDogStand();
		HotDogStand(string storeName);
		void justSold();
		void output();
		int getTotalDogs();
	private:
		static int totalDogs;
		static int totalStores;
		int addStoreId();
};

int HotDogStand::totalDogs=0;
int HotDogStand::totalStores=0;


void displayAllStandStats(vector<HotDogStand> v);
int menu(const string menuArr[],int size);
int menu(vector<string>v);
void printArrayElem(const string arr1[],int index);
void printVectorElem(vector<string> v,int index);
void printAllArrayWithCount(const string arr1[],int size);
string gString(string promptTxt);
int gInt(string promptTxt);
void printAllVectorWithCount(vector<string>v);
int main()
{
	//vars
	const int size = 7;
	int choice;
	vector <HotDogStand> stands;
	vector <string> standNames;
	string tempName;
	HotDogStand tempStand;
	bool quit=false;
	
	//menu
	
	const string menuStr[7]={"add a store","remove a store","report sold hotdog","inspect store","See all hotdogs sold","Display all stats","Quit"};

	//menuloop
	do
	{
		cout << "MAIN MENU"<<endl;
		choice = menu(menuStr,size);
		standNames.clear();
		switch (choice)
		{
		//add a hotdogstore
			case 0:
				tempName = gString("What is the name of the new stand?");
				tempStand = HotDogStand(tempName);	
				stands.push_back(tempStand);
				break;
		//remove a hotdog store
			case 1:
				for(int i = 0;i < stands.size();i++)
				{
					standNames.push_back(stands[i].name);
				}
				cout << "Choose a store to remove:"<<endl;
				choice = menu(standNames);
				stands.erase(stands.begin()+choice);
				break;
		//just sold
			case 2:
				for(int i = 0;i < stands.size();i++)
				{
					standNames.push_back(stands[i].name);
				}
				cout << "Choose a store to just sold a dog:"<<endl;
				choice = menu(standNames);
				stands[choice].justSold();
				break;
		//inspect store
			case 3:
				for(int i = 0;i < stands.size();i++)
				{
					standNames.push_back(stands[i].name);
				}
				cout << "Choose a store to just sold a dog:"<<endl;
				choice = menu(standNames);
				stands[choice].output();
				break;
		//total number of dogs
			case 4:
				cout << "The total number of dogs is: " << stands[0].getTotalDogs() << endl;
				break;
		//display all
			case 5:
				displayAllStandStats(stands);
				cout << "The total number of dogs is: " << stands[0].getTotalDogs() << endl;
				break;
		//quit
			case 6:
				quit = true;
		}
	}while(!quit);
	return 0;
}



int HotDogStand::getTotalDogs()
{
	return totalDogs;
}

void HotDogStand::output()
{
	cout << name << "\t" << dogsSold << endl; 
}

//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}
//displayallstandstats - display the out put for all of hot dog stand class
//input		v-vector of hotDogStand
void displayAllStandStats(vector<HotDogStand> v)
{
	for(int i = 0; i < v.size(); i++)
	{
		cout << i << ".";
		v[i].output();
	}
}

/**gInt -- get int for the number of boxes sold
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}


//menu- display menu, requests input, returns input
//input		arr1- array
//		size-size of array
//output	user input
int menu(const string menuArr[],int size)
{
	int input=0;
	do
	{
		printAllArrayWithCount(menuArr,size);
		input = gInt("Enter Choice: ");
		if (input<0||input>=size)
			cout << "invalid option, please reenter:" << endl;
	}while(input<0||input>=size);
	return input;
}

//menu- display menu, requests input, returns input
//input		v- vector
//output	user input
int menu(vector<string> v)
{
	int input=0;
	do
	{
		printAllVectorWithCount(v);
		input = gInt("Enter Choice: ");
		if (input<0||input>=v.size())
			cout << "invalid option, please reenter:" << endl;
	}while(input<0||input>=v.size());
	return input;
}

//printAllVectorWithCount - loops through array and prints each element
//input		v-array to print 
void printAllVectorWithCount(vector<string> v)
{
	for(int i = 0;i < v.size();i++)
	{
		cout << i <<". ";
		printVectorElem(v,i);
	}
}



//printAllArrayWithCount - loops through array and prints each element
//input		arr1-array to print 
void printAllArrayWithCount(const string arr1[],int size)
{
	for(int i = 0;i < size;i++)
	{
		cout << i <<". ";
		printArrayElem(arr1,i);
	}
}


//printVectorElem - prints Vector element at index
//input-	v-vector
//		index - integer representing element to be printed
void printVectorElem(vector<string>v,int index)
{
	cout << v[index] << endl;
	
}

//printArrayElem - prints array element at index
//input-	arr1- array
//		index - integer representing element to be printed
void printArrayElem(const string arr1[],int index)
{
	cout << arr1[index] << endl;
}

//Constructor
HotDogStand::HotDogStand()
{
	id=addStoreId();
	dogsSold=0;
	name="";
}
//Constructor2
HotDogStand::HotDogStand(string storeName)
{
	id=addStoreId();
	dogsSold=0;
	name=storeName;
}
//
//Just sold class
void HotDogStand::justSold()
{
	dogsSold++;
	totalDogs++;
}
//add storeID
int HotDogStand::addStoreId()
{
	totalStores++;
	return totalStores;
}
//	




