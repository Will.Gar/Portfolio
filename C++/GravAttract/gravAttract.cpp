/** gravAttract.cpp -- calculates gravitational attractive force when given the mass of each object and distance
 * Will Gardner
 * 9/22/17
 */
#include <iostream>
#include <cmath>
using namespace std;

const double GRAV_CONST = .00000006673; //Gravitational constant

double gravForce(double distance,double mass1,double mass2)
{
	return ((GRAV_CONST*mass1*mass2)/pow(distance,2));
}


int main ()
{

	//Vars
	double mass1,mass2; //mass for each object
	double distance; //distance between each object
	double gravitationalForce;//gravitational force
	//Welcome
	cout << "The Amazing Gratitional Attractive Force Calculatron 6000" << endl;
	//input
	cout << "Enter mass of first object:";
	cin >> mass1;
	cout << "Enter mass of second object:";
	cin >> mass2;
	cout << "Enter distance between the objects:";
	cin >> distance;
	
	//Calculations
	gravitationalForce = gravForce(distance,mass1,mass2);
	cout << "The gravitational force is " << gravitationalForce << " dyne(s).";
	return 0;
	
}
