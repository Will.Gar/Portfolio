/*
 * Names: Wyatt J. Brown & Will Gardner
 * File name: time_brown.h
 * Date: 2017-11-16
 */

#pragma once

#include <ctime>

namespace timebrown
{
	using namespace std;

	class Time
	{
		public:
			Time();
			Time(int hour_arg, int minute_arg, int second_arg);
			Time(time_t time);
			int getHour() const;
			int getMinute() const;
			int getSecond() const;
			void setTime(long elapseTime);
		private:
			int hour;
			int minute;
			int second;
	};
}
