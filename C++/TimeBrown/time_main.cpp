/*
 * Names: Wyatt J. Brown & Will Gardner
 * File name: time_main.cpp
 * Date: 2017-11-16
 */

#include "time_brown.h"
#include <iostream>

using namespace std;
using namespace timebrown;

void display_time(const Time time);

int main()
{

	display_time(Time());
	display_time(Time(555550000));
}

void display_time(const Time time)
{
	cout << time.getHour() << ":" << time.getMinute() << ":" << time.getSecond() << endl;
}
