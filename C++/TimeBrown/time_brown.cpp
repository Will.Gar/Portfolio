/*
 * Names: Wyatt J. Brown & Will Gardner
 * File name: time_brown.cpp
 * Date: 2017-11-16
 */

#include "time_brown.h"

namespace timebrown
{
	Time::Time()
	{
		time_t current_time = time(nullptr);
		tm *tm = gmtime(&current_time);
		hour = tm->tm_hour;
		minute = tm->tm_min;
		second = tm->tm_sec;
	}

	Time::Time(int hour_arg, int minute_arg, int second_arg)
	{
		hour = hour_arg;
		minute = minute_arg;
		second = second_arg;
	}

	Time::Time(time_t time)
	{
		tm *tm = gmtime(&time);
		hour = tm->tm_hour;
		minute = tm->tm_min;
		second = tm->tm_sec;
	}

	int Time::getHour() const
	{
		return hour;
	}

	int Time::getMinute() const
	{
		return minute;
	}

	int Time::getSecond() const
	{
		return second;
	}

	void Time::setTime(long elapseTime)
	{
		hour = static_cast<int> ((elapseTime / (1000*60*60)) % 24);
		minute = static_cast<int> ((elapseTime / (1000*60)) % 60);
		second = static_cast<int> (elapseTime / 1000) % 60 ;
	}
}
