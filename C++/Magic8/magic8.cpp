//magic8-program reads in text from magic8.txt, prompts user for a question and randomly selects a response
#include <vector>
#include <string>
#include <iostream>
#include <cstdlib>
#include <random>
#include <algorithm>
#include <fstream>
#include <string.h>
//using std::ifstream;
//using std::ofstream;
//using std::cout;
//using std::cin;
//using std::random_device;
//using std::uniform_int_distribution;
//using std::remove;
//using std::strtol;
//using std::to_string;
//using std::string;
using namespace std;
//declaration

string gString(string promptTxt);
int getRandoIntegeresian(int maxN);
string charLoopwithNum(ifstream& inStream,int max);
string filterChars(string str,string chars);
string charLoop(ifstream& inStream);
int countLines(string path);
//main
int main()
{
	const string PATH_OF_FILE="magic8.txt";
	const int NUMBER_OF_CHAPTERS=20;
	//vector
	int lines,rando;
	
	lines = countLines(PATH_OF_FILE);
	vector<string> answers;
	ifstream input;
	ofstream output;
	string tempStr;
	cout << "INSULTING MAGIC 8-BALL Program" << endl;
	gString("Input Question:");
	//get random int
	//rando = getRandoIntegeresian((lines-1));
	rando = 0;
	//get answers file
	input.open("magic8.txt");

	for(size_t i=0; i < lines; i++)
	{
		if ((rando == 0)&&(i==0))
		{
			tempStr=charLoopwithNum(input,NUMBER_OF_CHAPTERS);
			answers.push_back(tempStr);
			
		}
		else
		{
			answers.push_back(charLoop(input));
		}			
	}
	input.close();
	
	cout << answers[rando];
	

	//output updated answers

	output.open(PATH_OF_FILE);
	for(size_t i=0;i <lines; i++)
	{
		output << answers[i] << endl;
	}
	output.close();
}


//ifstream charloop for inputString with a number
string charLoopwithNum(ifstream& inStream,int max)
{
	string retStr="";
	char next;
	string buffer="";
	inStream.get(next);

	int tempNum;
	while((!inStream.eof())&&(next != '\n'))
	{
		if(next=='#')
		{
			retStr=retStr+next;
			inStream.get(next);
			if (next == 'N')
			{
				retStr = retStr+"0";
				inStream.get(next);
			}
			else
			{
				buffer=buffer+next;
				inStream.get(next);
				while((((int)next-'0') <= 9)&&((int)next-'0'>=0))
				{
					buffer=buffer+next;
					inStream.get(next);
				}
				tempNum = stoi(buffer);
				if (tempNum == max)
					retStr =retStr+"0";
				else
				{
					++tempNum;
					retStr=retStr+to_string(tempNum);
				}

			}
		
		}
		else
		{
			retStr= retStr + next;
			inStream.get(next);
		}
	}
	return retStr;	
}

//case for number or pound n
//inputStringloop
string charLoop(ifstream& inStream)
{
	string retStr="";
	char next;
	inStream.get(next);
	while((!inStream.eof())&&(next != '\n'))
	{	
		retStr= retStr + next;
		inStream.get(next);
	}
	return retStr;	
}
//counter of return characters
int countLines(string path)
{
	ifstream inStream;
	inStream.open(path);
	char next;
	inStream.get(next);
	int count=0;
	while((!inStream.eof()))
	{
		if (next == '\n')
		{
			++count;
		}
		inStream.get(next);
	}
	inStream.close();
	return count;	
}
//get random int
int getRandoIntegeresian(int maxN)
{
	random_device rd;
	uniform_int_distribution<int> dist(0,maxN);
	return dist(rd);
}
//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}

