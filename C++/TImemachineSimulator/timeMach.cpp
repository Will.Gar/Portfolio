/** Time machine.cpp - manages early version time machine that is able to travel 24 hours into the future. 
 * Will Gardner 10/1/2017
 * CSC 223
 */
#include <iostream>
#include <iomanip>
using namespace std;

int minToMilitary(int minutes);
int militaryToMin(int military);
int gInt(string promptTxt);
string gString(string promptTxt);
int addMinutes(int currentTime, int timeToAdd);

int main() {
	bool flag1=false;
	bool flag2=false;
	int startTime;
	int tempTime;
	int currentTime = 0;
	int minutes;
	cout << "Time machine" << endl;
	cout << "You walk into a dusty arcade box you happened upon in an attic." << endl << "On the seat is a pocket calculator with a serial connection tied to the system. Taped to "<< endl << "the crt screen is a picture of Bill Murray." << endl;
	do{
		
		startTime=gInt("enter Current time:");
		flag2=true;
		if ((abs(startTime % 100)>60)||(abs(startTime/100)>23))
		{
			flag2 = false;
			cout << "invalid start time enter again";
		}
	}while (!flag2);
	currentTime=startTime;

	do {
		cout << "You pick up the number pad."<< endl;
		cout << "current Time:" << setw(4)<<setfill('0') << minToMilitary(militaryToMin(currentTime)%1440)<< "."<< endl;
		minutes = gInt("Punch in the number of minutes you'd like to travel:");
			
		if ((addMinutes(currentTime,minutes)>=startTime)&&(militaryToMin(addMinutes(currentTime,minutes))<militaryToMin(startTime)+1440))
			currentTime=addMinutes(currentTime,minutes);
		else
			cout << "cannot go that far back or forward in time.try again"<<endl;
		cout << "Time is now " <<setw(4)<<setfill('0')<< minToMilitary(militaryToMin(currentTime)%1440)  <<endl;
		if((gString("Travel again? (y or n)"))=="n")
		{
			flag1 = true;
		}
	}while (!flag1);
}

//minToMilitary- converts a specified number of minutes to military time
//input - 	minutes int representing a number of minutes in military  time
//output- 	time in military time
int minToMilitary(int minutes)
{
	return ((minutes/60)*100+(minutes % 60));	
}
	

//militaryToMin-converts a int representing military time an int of minutes
// input - 	military - int that represents military time
// output - 	minutes raw number of minutes 
int militaryToMin(int military)
{	 
	return ((military/100)*60)+(military % 100);
}


/**gInt -- get int for the number of minutes
 * input -- prompt text requested user for int value
 * output -- int value parsed from input
 */
int gInt(string promptTxt)
{
	int returnInt;
	cout << promptTxt;
	cin >> returnInt;
	return returnInt;
}
//gString-get string value from user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}
// addMinutes -- adds time to the current to go into the future
// input -	currentTime - Input representing current time
//		timeToAdd - input of time to add
// output -	time as military format value
int addMinutes(int currentTime,int timeToAdd)
{
	return (minToMilitary(militaryToMin(currentTime)+timeToAdd));
}
