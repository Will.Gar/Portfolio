//counter.cpp - grocery counter program
//Will Gardner 10/23/17
//CSC223

#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Counter
{
	public:
		void incr1();
		void incr10();
		void incr100();
		void incr1000();
		int getCounter();

		bool isOverFlow();
		Counter();
	private:
		int counterNum;
		bool overFlow;
		void overFlowCheck();
		
};

string gString(string promptTxt);
int toDigit(char c);

int main(){
	const string choices = "asdfoq";
	bool strCheck=false;
	bool numCheck=false;
	bool quit=false;
	string choiceString = "";
	Counter counter = Counter();
	cout << "Money counter" << endl;
	while (!quit)
	{
		strCheck=false;
		numCheck=false;	
		//num and string check
		while (!strCheck&&!numCheck)
		{	
			choiceString = gString("Enter asdf for denomination, followed by 1-9 of that denom, o for overflow,q for quit:");
			if (choiceString.length() == 2)
			{
				for(int i = 0; i < 4;i++)
				{
					if (choiceString[0] == choices[i])
					{
						strCheck=true;
						if ((toDigit(choiceString[1])>=0)&&(toDigit(choiceString[1])<=9))
						{
							numCheck=true;
						}
						
					}

				}
			
			}
			else if ((choiceString[0]== 'q')||(choiceString[0]=='o'))
			{
				strCheck=true;
				numCheck=true;
			}
			if(!strCheck)
			{
				cout<<"Enter a valid option"<<endl;
			}
			else if(!numCheck)
			{
				cout <<"Enter a valid numeric digit" << endl;
			}
		}
		switch (choiceString[0])
		{
			case 'a':
				for(int i=0;i < toDigit(choiceString[1]);i++)
					counter.incr1();
				break;		
			case 's':
				for(int i=0;i < toDigit(choiceString[1]);i++)
					counter.incr10();
				break;	
			case 'd':
				for(int i=0;i < toDigit(choiceString[1]);i++)
					counter.incr100();
				break;
			case 'f':
				for(int i=0;i < toDigit(choiceString[1]);i++)
					counter.incr1000();
				break;
			case 'o':
				if (counter.isOverFlow())
					cout << "Counter has overflowed!"<<endl;
				else
					cout << "Counter has not overflowed!" << endl;
				break;
			case 'q': 
				quit=true;
		}
				
	}

	return 0;
}
/*Converts a char to a int
 *input-	c- char value
 *output-	o- int value */
int toDigit(char c)
{
	return (int)(c-'0');

}
Counter::Counter()
{
	counterNum=0;
}



void Counter::incr1()
{
	counterNum+=1;
	overFlowCheck();
}


void Counter::incr10()
{
	counterNum+=10;
	overFlowCheck();
}


void Counter::incr100()
{
	counterNum+=100;
	overFlowCheck();
}


void Counter::incr1000()
{
	counterNum+=1000;
	overFlowCheck();
}

int Counter::getCounter()
{
	return counterNum;
}

void Counter::overFlowCheck()
{
		
	if((!overFlow)&&(counterNum > 9999))
	{
		counterNum=counterNum - 9999;
		overFlow=true;
	}
	
}
bool Counter::isOverFlow()
{
	return overFlow;
}
//gString-get string value from user input
//input-	promptTxt- prompt to request user input
//output	user input
string gString(string promptTxt)
{
	string  returnStr;
	cout << promptTxt;
	cin >> returnStr;
	return returnStr;
}
