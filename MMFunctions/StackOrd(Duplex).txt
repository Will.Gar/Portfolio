Function StackOrd
&& Description: Recreated StackOrderFunction. Calculates Stackorder
&& Created By: Will Gardner
&& Modified: 5/13/2014

Parameter TotalRecords,NumberAcross,NumberDown
&&-TotalRecords - Total Number of records Post Sort
&&-NumberAcross - Number of Labels Across Page
&&-NumberDown - Number of Labels Down Page

Private GroupSize,SideSize,Flag1,RowNumber,Current,Value,FB,Ret1,GOrder
&&-GroupSize- Size of a set of labels on a page(Front and Back)
&&-SideSize - Size of a single side
&&-FrontOrBack - Front or Back? F=TRUE B=FALSE
&&-RowNumber
&&-Value To Return
GroupSize = (NumberAcross * NumberDown)*2
SideSize = (NumberAcross * NumberDown)
Value = "String"
IF (Val(TotalRecords) > 2)
	&&Part A:StackOrder--------------------------------------------------------------------------------------
	&& Step I: Determine If Front or Back
	IF RecNo() MOD 2 = 0 THEN
		Flag1 = FALSE
		FB= "B"
	ELSE
		Flag1 = TRUE
		FB= "F"
	ENDIF


	&& Step 2: Detirmine Group Order
	
	IF (((RecNo()+ 1)Div 2) MOD (TotalRecords / GroupSize)) = 0 THEN
		Current = (TotalRecords / GroupSize)
	ELSE 
		Current = (((RecNo() + 1)Div 2) MOD (TotalRecords / GroupSize))
	ENDIF
	GOrder =   Current/*Base value*/ + IIF((Flag1 == TRUE),0,1) /*FrontBack*/ + (Groupsize*(Current-1))-(Current-1) /*SideGroups*/ + (2*((RecNo()-1) DIV (TotalRecords / (SideSize)))) /*StackGroups*/
	
	&& Base Divide groups 1 - ((Total Number of Records) / SideSize) Ex 1111,2222,3333
	&& FrontBack: groups front and back after base EX 1212,2323,3434
	&& Sidegroups: creates pattern based on which label set ex 1212,5656,910910
	&& Stackgroups: creates stack group uniqueness EX 1234,5678,9101112)
	
	&& Step 3: Detirmine Row
	IF (NumberDown = 1) THEN
	 RowNumber = 1
	ELSE
	 IF (GOrder MOD GroupSize) = 0 THEN
	  RowNumber = NumberDown - 1
	 ELSE
	  RowNumber = (((GOrder / 2) MOD SideSize)+NumberAcross-1)DIV(NumberAcross)-1
	 ENDIF
	ENDIF

	
	&& Step4:The Fun Part
	 IF Flag1 = FALSE THEN
	  IF (GOrder MOD GroupSize) == 0 THEN
	   Value = (GOrder DIV GroupSize)*GroupSize + (((((NumberAcross-1)/2)+1)-NumberAcross)*2)
	  ELSE
	   IF (GOrder /2 MOD NumberAcross) == 0 Then
		Current = NumberAcross
		IF(NumberAcross == 2) THEN
		 IF (GOrder / 2) MOD 2 = 1 THEN
		  Current = 1
		 ENDIF
		ENDIF
	   ELSE
		Current = (GOrder /2 MOD NumberAcross)
	   ENDIF
	   Value = (((GOrder -1) Div GroupSize)*GroupSize)/*Base*/ + SideSize/*SideSize Constant*/+(NumberAcross*(RowNumber)) /*Row Base*/+ (((NumberAcross)+1)/2) /*MidPoint*/+ ((((NumberAcross)+1)/2) - Current)
	&& (Current-(((NumberAcross)+1)/2))
	  ENDIF
	 ELSE
	  Value = (GOrder Div GroupSize)*GroupSize + (GOrder MOD GroupSize) - (((GOrder MOD GroupSize)-1)/2)
	 ENDIF 
&&---------------------------------------------------------------------------------------------
   && Step5
	Ret1 = Value
	Return Ret1
ENDIF 
&& PartII FrontBack-------------------------------------------
IF (TotalRecords = 0) THEN
 	IF RecNo() MOD 2 = 0 THEN
		Flag1 = FALSE
		FB= "B"
	ELSE
		Flag1 = TRUE
		FB= "F"
	ENDIF
	Return FB
ENDIF
&& PartIII: Labels---------------------------------------------
IF (TotalRecords = 1) THEN 
	IF NumberAcross = "F" THEN
		Ret1 = "Main"
	ELSE
		Ret1 = "Back"
	ENDIF
	Return Ret1
ENDIF
