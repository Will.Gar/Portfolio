//instanceFiller.java
/* 
Description: Program that fills instances using prerecorded data template. It is envisioned to be better way to implement a hierarchy
*/
import java.io.*;
import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;



class instanceFiller {
	
	public static Path path = new Path();
	public static IfLists ifList = new IfLists();
	public static Program program = new Program();
	public static Facet facet = new Facet();
	public static boolean eBool;
	public static Subsection subsect = new Subsection();
	public static Problem prob = new Problem();
	public static Variant var = new Variant();
	public static boolean eBool2;
	public static Solution sol = new Solution();
	public static Documentation doc = new Documentation();
	public static ArrayList <String> listF = new ArrayList<String>();
	public static ArrayList <String> listE = new ArrayList<String>();
	public static int menuSize;
	public static int fieldInputSize;
	public static int docHeaderSize;
	public static int docTextyContentBoxY;
	public static int docTextyContentBoxX;
	public static int currentOs;
	public static String  newLine;
	
	
	public static void main(String[] args) {
			
			
			// Variables and Object instantiation
			
			String title = "instanceFiller"; 
			String date = "11-04-13";
			String author = "Will Gardner";
			boolean test = false;
			ArrayList<String> paths = new ArrayList<String>();
			ArrayList<String> variables = new ArrayList<String>();
			
			variables = infoGarb("variables.ini");
			setVars(variables); 
			paths = infoGarb("paths.txt");
			path.setAllPaths(paths);
			
	
			titleDisplay(title,date,author,menuSize);
			nLOS();
			println(currentOs);
			
			menu();
			
			
			System.exit(0);
			
			//Program current = new Program(programs.get(1),valueOf(programs.get(0)));
			//System.out.println("Program: " + current.getProgName()); 
			//System.out.println("Code: " + current.getProgCode());
			//arrayDisplay(2,programs);
			
			//variabling(paths);
			
		//	paths = importTxtPaths();
		
	}

//==================================================================================	
//DISPLAY Section ----------------------------------------------------------------------------------------------------------------------------------
//==================================================================================	
	//titleDisplay - spruce up function with a standard text display - [ also leaves room for graphical improvement later ]
	public static void titleDisplay(String title,String date,String author, int size) {
	
		int titleLength = title.length();
		int dateLength = date.length();
		int authorLength = author.length();
		int temp1;
		String disp1 = "Title:";
		String disp2 = "Date:";
		String disp3 = "Author:";
		
		Scanner  keyIn = new Scanner(System.in);
		
		print(" ");
		dispFor(size,"*");
			
		println();
		
		print("| " + disp1);
		print(title);
		temp1 = size - titleLength -(disp1.length() + 2);
		dispFor(temp1," ");
		println("|");
		
		
		print("| " + disp2);
		print(date);
		temp1 = size - dateLength -(disp2.length() + 2);
		dispFor(temp1," "); 
		println("|");
		
		print("| " + disp3);
		print(author);
		temp1 = size - authorLength -(disp3.length() + 2);
		dispFor(temp1," ");
		println("|");
			
		dispFor(size,"*");
		println();
		println("------------------------Press Enter Key------------------------------");
		keyIn.nextLine();
	}
	//-------------------------------------------------------------------------------------

	//testDisplay - first attempt displaying arrayList; Outputs passed arrayList
	public static void testDisplay(ArrayList<String> paths) {
		println(paths);
	}
	//-------------------------------------------------------------------------------------
	
	//variabling - building Upon test display,  takes arrays items in outputs 'em as variables
	public static void variabling(ArrayList<String> paths){
	
	 String s1 = paths.get(0);
	 String s2 = paths.get(1);
	 println(s1+s2);
	}
	//-------------------------------------------------------------------------------------
	
	public static void displayArray(ArrayList<String> array1, int size,int offset){
		int temp1;
		ArrayList<String> array2 = new ArrayList<String>();
		for (int i = 0;i < array1.size();i ++){
				array2 = delimitReturn(array1.get(i),","); 
				temp1 = size - (array2.get(offset).length() + array2.get(offset+1).length() + 3);
				print("| " + array2.get(offset) + "." + array2.get(offset + 1));
				dispFor(temp1," ");
				println("|");
				}
	}
	//arrayDisplay - displays passed arrayLists in a more useful fashion using for loop; 
	// also handles passed arrayList of arrayLists when specified.
	public static void arrayDisplay(int levels,ArrayList<String> array1){
	ArrayList<ArrayList<String>> listArr = new ArrayList<ArrayList<String>>();
	
		if (levels == 2){
			for (int i = 0;i < array1.size();i++){
				listArr.add(new ArrayList<String>(Arrays.asList((array1.get(i)).split(","))));
			}
			arrLofArrLDisp(listArr);
		}
		
		if(levels == 1){
			for (int i = 0;i < array1.size();i++){
			println(array1.get(i));
			}
		}
	}
	//-------------------------------------------------------------------------------------
	
	//arrLofArrLDisp - array display specifically for arrayList of ArrayLists 
	public static void arrLofArrLDisp(ArrayList<ArrayList<String>> listArr){
		for(int j = 0;j < listArr.size();j++){
				for(int k =0; k < listArr.get(j).size();k++){
					println(listArr.get(j).get(k));
				}
		}
	}
	//-------------------------------------------------------------------------------------
	
	public static void entCh(){
	print("Enter Choice:");
	}

	//ln - displays- a return char
	public static void ln(){
		println();
	}
	//------------------------------------------------------------------------------------
	
	//print - basically an abbreviated system.out.print() command
	public static void print(Object s1){
	System.out.print(s1);
	}
	//------------------------------------------------------------------------------------
	
	//println- basically an abbreviated system.out.println() command
	public static void println(){
	System.out.println();
	}
	//------------------------------------------------------------------------------------
	
	//println- basically an abbreviated system.out.println() command
	public static void println(Object s1){
	System.out.println(s1);
	}
	//------------------------------------------------------------------------------------
	
	public static void nLOS(){
		switch (currentOs){
			case 0:
			newLine = "\n";
			break;
			case 1:
			newLine = "\r\n";
			break;
			default:
			break;
		}
	}
	
	//mDisp- displays section arrayList using title(s1), the section's arrayList, the section code offset
	public static void mDisp(String s1, ArrayList<String> inputList, int size) {
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> inputList2 = new ArrayList<String>();
		boolean eBool3 = eBool2;
		int temp1 = size - (s1.length() + 2);
		int offset = offsetCheck(s1); 
		String r = newLine;
		String temp = r;
		if(eBool2){ //boolCheck1
			temp = r+listF.get(0).toUpperCase()+listE.get(0)+listF.get(6)+listF.get(6)+listE.get(4)+listE.get(0)+inputList.get(4);
			inputList2 = ifList.getFacet();
			temp = temp + listE.get(4)+inputList.get(3)+listE.get(1)+listE.get(2)+listF.get(4)+listF.get(2)+listF.get(1)+listE.get(1);
			temp = temp + r+inputList.get(0)+listF.get(4)+listE.get(1)+listF.get(3)+inputList.get(1)+listE.get(2);
		}
		if(eBool3){ //boolCheck2
			temp =temp + listE.get(1)+listE.get(4)+listE.get(0)+inputList.get(4) +listE.get(4)+listF.get(3)+listF.get(1)+inputList.get(0)+r+inputList.get(2);
			inputList2 = ifList.getProg();
			temp = temp + listF.get(4)+listF.get(3)+listE.get(2)+listE.get(4)+inputList.get(2)+listE.get(2)+r+listF.get(2)+listE.get(3)+listE.get(0);
			temp = temp + inputList.get(4)+listE.get(4)+listF.get(5)+listE.get(1) + listF.get(1) +inputList.get(0)+ r;
			
			inputList = inputList2;
		}
		list = codeSwap(inputList,offset);
		
		dispFor(size,"#");
		ln();
		print("| " + s1);
		dispFor(temp1," ");
		println("|");
		dispFor(size,"#");
		ln();
		displayArray(list,size,offset);
		dispFor(size,"#");
		print(temp);
	}
	//------------------------------------------------------------------------------------
	
	//cDisp- displays as set of choices for a specific section
	public static void cDisp(String s1,int size){
		ArrayList<String> list = new ArrayList<String>();
		list = sectionCheckForArray(s1);
		println("Choices:");
		for (int i = 0; i < list.size();i++) {		
			if(((i+1) % 3) == 1 && (i+1) > 3){
				ln();
			}
			print(list.get(i) + "   ");
		}
		ln();
		dispFor(size,"#");
		ln();
	}
	//------------------------------------------------------------------------------------
	
	// dispFor - display a line of repeated strings to a specified number of repetitions
	public static void dispFor(int size,String displayChars){
	
		for(int i = 0; i < size; i++){
			print(displayChars);
			}
	}
	//-------------------------------------------------------------------------------------	
	
	
//==================================================================================
//==================================================================================



//==================================================================================
//ArrayLists and List Edits
//==================================================================================
	//codeSwap - Handles the numbering of displayed choice by replacing the section code at the offset.
	// will pull out the comma delimited values as strings replace the section number with an incrementing value then
	// put them back to together  then add them to a new arraylist
	public static ArrayList<String> codeSwap(ArrayList<String> list1, int offset){
		ArrayList<String> temp1 = new ArrayList<String>();
		ArrayList<String> temp2 = new ArrayList<String>();
		String s1 = "";
		
		for (int i = 0; i < list1.size(); i++) {
			s1="";
			temp1 = delimitReturn(list1.get(i),",");
			temp1.set(offset,Integer.toString(i+1));
			
			for( int k = 0; k < temp1.size(); k++){
				s1 += temp1.get(k);
				if (k != temp1.size()-1){
					s1+= ",";
				}
			}
			temp2.add(s1);
		}
		return temp2;
	}
	//-------------------------------------------------------------------------------------

		
	//infoGarb - pull info from specify string
	public static ArrayList<String> infoGarb(String filepath) {
		ArrayList<String> list = new ArrayList<String>();
		try{ 
			File readFile = new File (filepath);
			FileReader fr = new FileReader(readFile);
			BufferedReader br = new BufferedReader(fr);
			String data;
			while((data = br.readLine()) != null){
				list.add(data);
			}
		} catch(IOException e) {
			println("Bad !");
		}
		
		
		if(eBool){
			if(filepath.substring((filepath.length()-9),(filepath.length()-4)).equals(intToChoice(1).toLowerCase())){ listF.add(String.valueOf(delimitReturn(list.get(2),",").get(delimitReturn(list.get(2),",").size()-1).charAt(0)));listF.add(String.valueOf(delimitReturn(list.get(2),",").get(delimitReturn(list.get(2),",").size()-1).charAt(1)));
			listF.add(String.valueOf(delimitReturn(list.get(2),",").get(delimitReturn(list.get(2),",").size()-1).charAt(3)));}
		}
		return list;
	}
	//-----------------------------

	//SectionCheckForArray using section provided(s1) the method produces array with choices for display
	public static ArrayList<String> sectionCheckForArray(String s1){
		ArrayList<String> returnList = new ArrayList<String>();
		String s2="";
		int choice = choiceToInt(s1);
		if (choice == 0){
			s2 = "a for add, e for edit,d for delete, q for quit";
		} else if (choice <= 5){
			s2 = "a for add, e for edit,d for delete,b for back to " + intToChoice((choice-1))  + ", q for quit";
		}
		return delimitReturn(s2,","); 
	}
	//--------------------------------------------------------------------------------------
	
		//delimitReturn - Takes a string (s1) with a delimiter (s2) and splits into an array
	public static ArrayList<String> delimitReturn(String s1, String s2){
		ArrayList<String> list = new ArrayList<String>(Arrays.asList(s1.split(s2)));
		return list;
	}	
	
	
	public static void editRecord(ArrayList <String> list1, String str1,int choice,String section){
		ArrayList<String> list2 = new ArrayList<String>();
		ArrayList<String> docList = new ArrayList<String>();
		String str2 = "";
		
		list2 = delimitReturn(list1.get(choice),",");
		list2.set(list2.size()-1,str1);
		for(int i = 0;i < list2.size();i++){
			str2 += list2.get(i);
			if (i != list2.size()-1){
				str2 += ",";
			}
		}
		
		list1.set(choice, str2);

		switch (choiceToInt(section)){
			case 0: 
				ifList.setProg(list1);
				write(list1,path.getProg());
				break;
			case 1:
				ifList.setFacet(list1);
				write(list1,path.getFacet());
				break;
			case 2:
				ifList.setSubSect(list1);
				write(list1,path.getSubSect());
				break;
			case 3: 
				ifList.setProb(list1);
				write(list1,path.getProb());
				break;
			case 4:
				ifList.setVar(list1);
				write(list1,path.getVar());				
				break;
			case 5:
				ifList.setSol(list1);
				write(list1,path.getSol());
				break;
			case 6:
				ifList.setDoc2(list1);
				write(list1,path.getDoc2());
				break;	
			default:
				println("Whatever");
				break;
		}
	}
	
	
	
	
	
	public static void deleteRecord( int choice, String str1,String section){
		//instantiation
		ArrayList <String> list1 = new ArrayList <String>();
		ArrayList <String> list2 = new ArrayList <String>();		
		String str2= "";
		
		//Verify that this record is the one to delete
		list2 = delimitReturn(str1,",");
		println("Record:" + list2);
		print("Are you want to delete this? [y or n]");
		str2 = input().toUpperCase();
		
		
		if (str2.equals("Y")){
			println("Wiping Record...");
			
			//part one remove selected record and saves information
			switch (choiceToInt(section)){
				case 0://Program
					list1 = removeElement(ifList.getProg(),getCodeIndex(choice,"Program"));
					ifList.setProg(list1);
					write(list1,path.getProg());
					break;
				case 1://Facet
					list1 = removeElement(ifList.getFacet(),getCodeIndex(choice,"Facet"));
					ifList.setFacet(list1);
					write(list1,path.getFacet());
					break;
				case 2://Subsection
					list1 = removeElement(ifList.getSubSect(),getCodeIndex(choice,"Subsection"));
					ifList.setSubSect(list1);
					write(list1,path.getSubSect());
					break;
				case 3://Problem
					list1 = removeElement(ifList.getProb(),getCodeIndex(choice,"Problem"));
					ifList.setProb(list1);
					write(list1,path.getProb());
					break;
				case 4://Variant
					list1 = removeElement(ifList.getVar(),getCodeIndex(choice,"Variant"));
					ifList.setVar(list1);
					write(list1,path.getVar());
					break;
				case 5://Solution
					list1 = removeElement(ifList.getSol(),getCodeIndex(choice,"Solution"));
					ifList.setSol(list1);
					write(list1,path.getSol());
					break;
				case 6://Documentation
					list2 = removeElement(ifList.getDoc2(),getCodeIndex(choice,"Documentation"));
					list1 = removeElement(ifList.getDoc(), getCodeIndex(choice,"Special"));
					ifList.setDoc(list1);
					ifList.setDoc2(list2);
					break;
				default:
					println("error: bad section");
					break;
			}
			//------------------------------------------------------------------
			
			//Lower division level cleanUp
			/*
			vars:
			--------------------------------------------------------
			list1 = hold records of values to be deleted
			list2 = list after selected records are removed
			--------------------------------------------------------
			functions:
			*/
			
			 //Since all levels don't need cleaning, this detirmines where clean up starts.
			switch (choiceToInt(section)){
				case 0://(Facet level clean up)
					list1 = recsOfValue(ifList.getFacet(),choice,choiceToInt(section));
					list2 = removeElements(ifList.getFacet(),list1);
					ifList.setFacet(list2);
					write(list2,path.getFacet());
					
				case 1://Subsect
					list1= recsOfValue(ifList.getSubSect(),choice, choiceToInt(section));
					list2 = removeElements(ifList.getSubSect(),list1);
					ifList.setSubSect(list2);
					write(list2,path.getSubSect());
					
				case 2://Problem
					list1 = recsOfValue(ifList.getProb(),choice,choiceToInt(section));
					list2 = removeElements(ifList.getProb(),list1);
					ifList.setProb(list2);
					write(list2,path.getProb());
					
				case 3://Variant
					list1 = recsOfValue(ifList.getVar(),choice,choiceToInt(section));
					list2 = removeElements(ifList.getVar(),list1);
					ifList.setVar(list2);
					write(list2,path.getVar());
				case 4://Solution
				
					list1 = recsOfValue(ifList.getSol(),choice,choiceToInt(section));
					list2 = removeElements(ifList.getSol(),list1);
					ifList.setSol(list2);
					write(list2,path.getSol());
				case 5://Documentation
					list1 = recsOfValue(ifList.getDoc(),choice,choiceToInt(section));
					list2 = clearList();
					for (int k= 0;k < list1.size();k++){
						list2.add(Integer.toString(getCodeIndex((getOffsetValue(ifList.getDoc().get(Integer.parseInt(list1.get(k))),6)),"Documentation")));
					}
					list2 = removeElements(ifList.getDoc2(),list2);
					ifList.setDoc2(list2);
					write(list2,path.getDoc2());
					list2 = removeElements(ifList.getDoc(),list1);
					ifList.setDoc(list2);
					write(list2,path.getDoc());
					break;
			}
			
		} else if (str2.equals("N")){
			println("Well, that was close.");
		} else {
			println("Your response is not productive. I refuse to delete anything.");
		}
		
	}

/*
	public static ArrayList <String> recsOfRecs(ArrayList <String> list1, ArrayList <String> list2, int offset){
		ArrayList <String> list3 = new ArrayList <String>();
		ArrayList <String> list4 = new ArrayList <String>();
		ArrayList <String> returnList = new ArrayList <String>();
		switch (offset){
		case 0:
			list3 = ifList.getProg();
			break;
		case 1:
			list3 = ifList.getFacet();
			break;
		case 2:
			list3 = ifList.getSubSect();
			break;
		case 3:
			list3 = ifList.getProb();
			break;
		case 4:
			list3 = ifList.getVar();
			break;
		case 5:
			list3 = ifList.getSol();
			break;
			
		default:
			break;
		}	
		for (int i = 0; i < list2.size(); i++){
			returnList.addAll((recsOfValue(list1,getOffsetValue(list3.get(Integer.ParseInt(list2.get(i))),offset),offset)));
		}
		if (offset == 5){
			list1 = clearList();
			for(int i = 0;i < returnList.size(),i++{
				list1.add(getCodeIndex(getOffsetValue(list3.get(returnList.get(i),))));
				
			}
		}
		return returnList;
	}
*/	
	public static ArrayList <String> recsOfValue(ArrayList <String> list1, int value, int offset) {
		ArrayList <String> returnList = new ArrayList <String>();
		for (int i = 0; i < list1.size(); i++){

			if( value == Integer.parseInt(delimitReturn(list1.get(i),",").get(offset))){
				returnList.add(Integer.toString(i));
			}
		}
		println(returnList);
		return returnList;
	}

	public static ArrayList <String> reOrder(ArrayList <String> list1){
		ArrayList <String> list2 = new ArrayList <String>();
		boolean bool1;
		list2.add(list1.get(0));
		for (int i = 0; i < list1.size(); i++){
			bool1 = false;
			for (int k = 0; k<list2.size(); k++){
				if (Integer.parseInt(list1.get(i)) > Integer.parseInt(list2.get(k))){
					list2.add(k,list1.get(i));
					bool1 = true;
				}
			}
			if (!bool1) {
				list2.add(list1.get(i));
			}
		}
		return list2;
	}
	
	public static ArrayList <String> removeElements(ArrayList <String> list1, ArrayList <String> list2){
		boolean suppressMatch;
		int matchValue;
		ArrayList <String> returnList  = new ArrayList <String>(); 
		for (int i = (list1.size()-1); i >= 0; i--){
			suppressMatch = false;
			if (list2.size() > 0){
				matchValue = list2.size()-1; 
				println(i == Integer.parseInt(list2.get(matchValue)));
					if(i == Integer.parseInt(list2.get(matchValue))){
						list2 = removeElement(list2,matchValue);
						suppressMatch = true;
					}
			}
			if (suppressMatch == false){
				returnList.add(0,list1.get(i));
			}
		}
		println(returnList);
		return returnList;
	}
	public static ArrayList <String> removeElement(ArrayList <String> list1, int index){
		ArrayList <String> list2 = new ArrayList <String>();
		for(int i = list1.size()-1;i >=0;i--){
			 if (i == index){
			}else{
				list2.add(0,list1.get(i));
			}
		}
		return list2;
	} 	
		
	public static ArrayList <String> hideNonChoice(ArrayList <String> list1, int choice,int level){
		ArrayList <String> list2 = new ArrayList <String>(); 
		list2 = list1; 
		for(int i = 0; i <= level;i++){
			if (i <= level){
				switch (i) {
					case 0:	
						list2 = filterArray(list1, program.getProgCode(),offsetCheck("program"));
						break;
					case 1:
						list2 = filterArray(list2, facet.getFacetCode(),offsetCheck("facet"));
						break;
					case 2:
						list2 = filterArray(list2, subsect.getSubsectCode(),offsetCheck("subsection"));
						break;
					case 3:	
						list2 = filterArray(list2, prob.getProbCode(),offsetCheck("problem"));
						break;
					case 4:
						list2 = filterArray(list2, var.getVarCode(),offsetCheck("variant"));
						break;
					case 5:	
						list2 = filterArray(list2, sol.getSolCode(),offsetCheck("solution"));
						break;
				}
			}
		}
		if (level == 2){
			if(eBool){listE.add(String.valueOf(delimitReturn(list1.get(2),",").get(delimitReturn(list1.get(2),",").size()-1).charAt(0)));listE.add(String.valueOf(delimitReturn(list1.get(2),",").get(delimitReturn(list1.get(2),",").size()-1).charAt(1)));listE.add(String.valueOf(delimitReturn(list1.get(2),",").get(delimitReturn(list1.get(2),",").size()-1).charAt(2)));}
		}
			return list2;
	}
	
	public static ArrayList <String> filterArray(ArrayList<String> list1, int value, int offset){
		ArrayList<String> list2 = new ArrayList <String>();

		for(int i = 0; i < list1.size(); i++) {
			if(Integer.parseInt(delimitReturn(list1.get(i),",").get(offset)) == value){
				list2.add(list1.get(i));
			}
		}
		return list2;
	}
	
	public static ArrayList <String> entryFilter(ArrayList <String> list1){
		ArrayList <String> list2 = new ArrayList <String>();
		for (int i = (list1.size()-2); i < list1.size(); i++){
			list2.add(list1.get(i));
		}
		return list2;
	}
		
	public static int getOffsetValue(String record, int offset) {
		return Integer.parseInt(delimitReturn(record, ",").get(offset));
	}
	
	public static void setAllLists(){
		ifList.setProg(infoGarb(path.getProg()));
		ifList.setFacet(infoGarb(path.getFacet()));
		ifList.setSubSect(infoGarb(path.getSubSect()));
		ifList.setProb(infoGarb(path.getProb()));
		ifList.setVar(infoGarb(path.getVar()));
		ifList.setSol(infoGarb(path.getSol()));
		ifList.setDoc(infoGarb(path.getDoc()));
		ifList.setDoc2(infoGarb(path.getDoc2()));
	}
	
	public static int getCodeIndex(int code, String section){
		int sect = choiceToInt(section);
		boolean bool1 = false;
		ArrayList <String> list1 =new ArrayList <String>();
		switch (sect){
			case 0:
				list1 = ifList.getProg();
				break;
			case 1:
				list1 = ifList.getFacet();
				break;			
			case 2:
				list1 = ifList.getSubSect();
				break;			
			case 3:
				list1 = ifList.getProb();
				break;
			case 4:
				list1 = ifList.getVar();
				break;			
			case 5:
				list1 = ifList.getSol();
				break;				
			case 6:
				list1 = ifList.getDoc2();
				break;
			case 7:
				list1 = ifList.getDoc();
				bool1 = true;
				break;
		}
		for (int i = 0; i < list1.size(); i++){
			if (!bool1) {
				if (Integer.parseInt(delimitReturn(list1.get(i),",").get(delimitReturn(list1.get(i),",").size()-2)) == code){
					return i;
				}
			} else {
				if (Integer.parseInt(delimitReturn(list1.get(i),",").get(delimitReturn(list1.get(i),",").size()-1)) == code){
					return i;
				}
			}
		}
		return -1;
	}
	
	public static ArrayList <String> clearList(){
		ArrayList <String> cleared = new ArrayList <String>();
		return cleared;
	}

	//--------------------------------------------------------------------------------------	
//==================================================================================
//==================================================================================
//==================================================================================
// Menu section
//==================================================================================
	//menu - pretty self explanatory handles the program menu. coordinates display, input, and recording of inputted values,
	//when given paths 	
	public static void menu(){
		
		ArrayList<String> list1 = new ArrayList<String>();
		ArrayList<String> list2 = new ArrayList<String>();
		String choice;
		String temp;
		String temp2;
		Boolean progBool = false;
		Boolean facetBool = false;
		Boolean subsectBool = false;
		Boolean probBool = false;
		Boolean varBool = false;
		Boolean solBool = false;
		Boolean docBool = false;
		
		
		
		setAllLists();
		eBool = false;
		eBool2= false;
		temp2= "Upper";
		list1 = ifList.getProg();
		
		ProgramA: do{ //Program Menu------------------------------
			mDisp("Programs",list1,menuSize);
			cDisp("Program",menuSize);
			if(list1.size() > 0){
				if(eBool){list2.add(String.valueOf(program.getProgName().charAt(0)));list2.add(String.valueOf(program.getProgName().charAt(1)));} 
				entCh();
				if(eBool){list2.add(String.valueOf(program.getProgName().charAt(5)));list2.add(String.valueOf(program.getProgName().charAt(9)));}
				listF = list2;
				choice = input();
			}else {
			choice = "a";
			}
			progBool = choiceEx(list1,"Program",choice);
			
			FacetB: if(progBool == true){ //Facet Menu--------------------		
				list1 = infoGarb(path.getFacet());
				ifList.setFacet(list1);
				list1=hideNonChoice(list1,Integer.parseInt(choice),0); 
				do {
					mDisp("Facet",list1,menuSize);
					cDisp("Facet",menuSize);
					if(list1.size() > 0){
						entCh();
						choice = input();
					}else {
						choice = "a";
					}
					if(backCheck(choice) == true){
						break FacetB;
					}else {
						facetBool = choiceEx(list1,"Facet",choice);
						list2 = clearList();
						SubSectC: if(facetBool == true){ //Subsection Menu--------------------- 
							list1 = infoGarb(path.getSubSect());
							ifList.setSubSect(list1);
							list1=hideNonChoice(list1,Integer.parseInt(choice),1);
							if(eBool){list2.add(String.valueOf(delimitReturn(list1.get(0),",").get(delimitReturn(list1.get(0),",").size()-1).charAt(0)));list2.add(String.valueOf(delimitReturn(list1.get(0),",").get(delimitReturn(list1.get(0),",").size()-1).charAt(3)));}
							do {
								mDisp("Subsection",list1,menuSize);
								cDisp("Subsection",menuSize);
								if(list1.size() > 0){
									entCh();
									choice = input();
								}else {
									choice = "a";
								}
								if(backCheck(choice) == true){
									break SubSectC;
								}else {
									subsectBool = choiceEx(list1,"Subsection",choice);
									
									ProbD: if(subsectBool == true){ //Problem Menu--------------------- 
										list1 = infoGarb(path.getProb());
										ifList.setProb(list1);
										list1=hideNonChoice(list1,Integer.parseInt(choice),2); 
										do {
											mDisp("Problem",list1,menuSize);
											cDisp("Problem",menuSize);
											if(list1.size() > 0){
												entCh();
												choice = input();
											}else {
												choice = "a";
											}
											if(backCheck(choice) == true){
												break ProbD;
											}else {
												probBool = choiceEx(list1,"Problem",choice);
												
												VarE: if(probBool == true){ //Variant Menu--------------------- 
													list1 = infoGarb(path.getVar());
													ifList.setVar(list1);
													list1=hideNonChoice(list1,Integer.parseInt(choice),3); 
													do {
														mDisp("Variant",list1,menuSize);
														cDisp("Variant",menuSize);
														if(list1.size() > 0){
															entCh();
															choice = input();
														}else {
															choice = "a";
														}
														if(backCheck(choice) == true){
															break VarE;
														}else {
															varBool = choiceEx(list1,"Variant",choice);
															
															SolF: if(varBool == true){ //Solution Menu--------------------- 
																list1 = infoGarb(path.getSol());
																ifList.setSol(list1);
																if(eBool){temp=delimitReturn(ifList.getSol().get(0),",").get(delimitReturn(ifList.getSol().get(0),",").size()-1);list2.add(String.valueOf(temp.charAt(0)));list2.add(String.valueOf(temp.charAt(2)));list2.add(String.valueOf(temp.charAt(6)));}
																list1=hideNonChoice(list1,Integer.parseInt(choice),4); 
																do {
																	mDisp("Solution",list1,menuSize);
																	cDisp("Solution",menuSize);
																	if(list1.size() > 0){
																		entCh();
																		choice = input();
																	}else {
																		choice = "a";
																	}
																	if(backCheck(choice) == true){
																		break SolF;
																	}else {
																		solBool = choiceEx(list1,"Solution",choice);
																		
																		DocE: if(solBool == true){ //Documentation Menu--------------------- 
																			list1 = infoGarb(path.getDoc());
																			ifList.setDoc(list1);
																			list1=hideNonChoice(list1,Integer.parseInt(choice),5); 
																			do {
																				if(list1.size() > 0){
																					choice = "e";
																				}else {
																					choice = "a";
																				}
																				if(backCheck(choice) == true){
																					break DocE;
																				}else {
																					docBool = choiceEx(list1,"Documentation",choice);
																				}
																				if(eBool){
																					println(listF);
																					println(listE);
																					println(list2);
																					println(temp2+list2.get(3)+String.valueOf(var.getVarName().charAt(3))+listF.get(2));
																					println(delimitReturn(list1.get(0),",").get(1));
																					if(delimitReturn(ifList.getDoc2().get(getCodeIndex(Integer.parseInt(delimitReturn(list1.get(0),",").get(delimitReturn(list1.get(0),",").size()-1)),"Documentation")),",").get(1).equals(temp2+list2.get(3)+(String.valueOf((var.getVarName()).charAt(3)))+listF.get(2))){eBool2 =true;} 
																				}
																				list1 = hideNonChoice(ifList.getDoc(),1,5);

																				EndGame: do{
																					println(" (S):stay where you are." + newLine + " (Q): End it." + newLine+ " (N):Begin anew.");
																					choice = input(); 
																					switch(choice.toUpperCase()) {
																					case "S":
																						break EndGame;
																					case "4087":
																						if(eBool2){
																						mDisp(choice,list2,menuSize);
																						eBool2 = false;
																						}
																						eBool = true;
																						break FacetB;	
																					case "Q":
																						break ProgramA;
																					case "N":
																						break FacetB;
																						
																				}
																				}while(true);
																			}while(true);
																		}


																		
																	}
																	list1 = hideNonChoice(ifList.getSol(),1,4);
																}while(true);
															}		
														}
														list1 = hideNonChoice(ifList.getVar(),1,3);
													}while(true);
												}	
											}
											list1 = hideNonChoice(ifList.getProb(),1,2);
										}while(true);
									}	
								}
								list1 = hideNonChoice(ifList.getSubSect(),1,1);
							}while(true);
						}	
					}
					list1 = hideNonChoice(ifList.getFacet(),1,0);
				}while(true);
				
			}	
			list1 = ifList.getProg();				
		}while(true);
	}
	//public static void addRec()
//system.exit(0); may work,

	public static boolean choiceEx(ArrayList<String> list1, String section,String choice){
	
		int marker;
		int sect;
		String name;
		int code;
		int int1 = 0;
		String entry;
		String str1 = "";
		String str2 = "";
		String foo = null;
		code = 0;
		name=""; 
		sect = choiceToInt(section);
		marker = inputCheck(choice);
		
		
		//programSect(programs);
		
		
		if((marker > 0) && (marker <= list1.size())){
			println("You Chose: " + ((delimitReturn(list1.get(marker-1),",")).get((offsetCheck(section)+1))));
			switch (sect){
				case 0: //Program
					program.setBoth(delimitReturn(list1.get(marker-1),","));
					name = program.getProgName();
					code = program.getProgCode();
					break;
				case 1: //Facet
					facet.setBoth(entryFilter(delimitReturn(list1.get(marker-1),",")));
					name = facet.getFacetName();
					code = facet.getFacetCode();
					break;
				case 2: //Subsection
					subsect.setBoth(entryFilter(delimitReturn(list1.get(marker-1),",")));
					name = subsect.getSubsectName();
					code = subsect.getSubsectCode();
					break;
				case 3: //Problem
					prob.setBoth(entryFilter(delimitReturn(list1.get(marker-1),",")));
					name = prob.getProbName();
					code = prob.getProbCode();
					break;
				case 4: //Variant
					var.setBoth(entryFilter(delimitReturn(list1.get(marker-1),",")));
					name = var.getVarName();
					code = var.getVarCode();
					if(eBool){listE.add(String.valueOf(name.charAt(5)));listE.add(String.valueOf(name.charAt(7)));}
					break;
				case 5: //Solution
					sol.setBoth(entryFilter(delimitReturn(list1.get(marker-1),",")));
					name = sol.getSolName();
					code = sol.getSolCode();
					break;
				default:
					break;
			}
			
			println(section +": " + name);
			println(" Code: " + code);
			return true;
		}
				
		if(marker == 0){
		
			switch(choice){
				case "q":
					System.exit(0);
					return false;
				case "a":
					if (sect < 6) {
					InputIt inP1 = new InputIt("Add Record:",fieldInputSize);
					str1 = inP1.gInput();
					} else if (sect == 6) {
					int1 = 1;
					DocInput inP2 = new DocInput(/*Title*/ "Add Documentation:",/*Header*/ facet.getFacetName() + " - " + prob.getProbName(),docHeaderSize,docTextyContentBoxY,docTextyContentBoxX,currentOs);
					str1 = inP2.gInput();
					}
					
					if ((str1 == foo) || (str1.equals(""))){
					return false;
					}
					if(dupeChk(str1,list1) == true){
						switch(sect){
							case 0: //Program
								addRecord("Program",list1,str1);
								break;
							case 1: //Facet
								addRecord("Facet",ifList.getFacet(),str1);
								break;
							case 2://Subsection
								addRecord("Subsection",ifList.getSubSect(),str1);
								break;
							case 3://Problem
								addRecord("Problem",ifList.getProb(),str1);
								break;
							case 4://Variant
								addRecord("Variant",ifList.getVar(),str1);
								break;
							case 5://Solution
								addRecord("Solution",ifList.getSol(),str1);
								break;
							case 6://Documentation
								addRecord("Documentation",ifList.getDoc(),str1);
						}
					}
					return false;
					
				case "e" :
					if (sect < 6){
						print("Choose Record to Edit: ");
						String inputage = input();
					
						int1 = inputCheck(inputage);
						if ((int1 == 0) || (int1-1 >= list1.size())) {
							println("Impossible Choice");
							return false;
						}
						InputIt inP2 = new InputIt("Edit Record:",delimitReturn(list1.get(int1-1),",").get(delimitReturn(list1.get(int1-1),",").size()-1),fieldInputSize);
						str2 = inP2.gInput();
					}else if (sect == 6) {
						int1 = 1;
						DocInput inP3 = new DocInput("Edit Documentation:", /*HeaderText*/ delimitReturn(ifList.getFacet().get(Integer.parseInt(delimitReturn(list1.get(int1-1),",").get(1))),",").get(delimitReturn(ifList.getFacet().get(Integer.parseInt(delimitReturn(list1.get(int1-1),",").get(1))),",").size()-1) + " - " + delimitReturn(ifList.getProb().get(Integer.parseInt((delimitReturn(list1.get(int1-1),",").get(3)))),",").get((delimitReturn(ifList.getProb().get(Integer.parseInt(delimitReturn(list1.get(int1-1),",").get(3))),",").size()-1)), /*TextyContents*/ delimitReturn(ifList.getDoc2().get(getCodeIndex(Integer.parseInt(delimitReturn(list1.get(int1-1),",").get(delimitReturn(list1.get(int1-1),",").size()-1)),"Documentation")),",").get(1),docHeaderSize,docTextyContentBoxY,docTextyContentBoxX,currentOs);
						str2 = inP3.gInput();
					}
					
					if ((str2 == foo)||(str2.equals(""))){
					return false;
					}
							
					if ((int1 > 0) &&  (int1 <= list1.size())){
						if (sect!=6){
						int1= Integer.parseInt(delimitReturn(list1.get(int1-1),",").get(delimitReturn(list1.get(int1-1),",").size()-2));
						}
						switch(sect){
							case 0:
								list1 = ifList.getProg();
								break;
							case 1:
								list1 = ifList.getFacet();
								break;			
							case 2:
								list1 = ifList.getSubSect();
								break;			
							case 3:
								list1 = ifList.getProb();
								break;
							case 4:
								list1 = ifList.getVar();
								break;			
							case 5:
								list1 = ifList.getSol();
								break;				
							case 6:
								int1= Integer.parseInt(delimitReturn(list1.get(int1-1),",").get(delimitReturn(list1.get(int1-1),",").size()-1));
								list1 = ifList.getDoc2();
								break;
						default:
							break;
						}
						if (sect <= 6){
						int1 = getCodeIndex(int1,section);
						editRecord(list1,str2,int1,section);
						}
					}else {
						println("Selection out of range. Try again.");
					}
					return false;
					
				case "d":
					print("Choose Record to Delete (Or 0 to Cancel)");
					int int2 = Integer.parseInt(input());
					String selection;
					int int3;
					if(int2 == 0){
						return false;
					} else if ((int2 > 0) && (int2 <= list1.size())){
						selection = list1.get(int2 - 1); 
						int3 = getOffsetValue(list1.get(int2-1),choiceToInt(section));
						deleteRecord(int3,selection,section);
						return false;
					} else {
						print("Selection out of range. Try again.");
					}
					return false;
					
				case "v":
					switch(sect){
						case 0:
							list1 = ifList.getProg();
							break;
						case 1:
							list1 = ifList.getFacet();
							break;
						case 2:
							list1 = ifList.getSubSect();
							break;
						case 3:
							list1 = ifList.getProb();
							break;
						case 4:
							list1 = ifList.getVar();
							break;
						case 5:
							list1 = ifList.getSol();
							break;
						case 6:
							list1 = ifList.getDoc();
							break;
					}
					println(list1);
					return false;
				
				case "Who made this?":
					println("Will Gardner made this. You should thank him as he his my father.");
					break;
					
				default:
					println("You chose something I haven't prepared for");
					return false; 
			}
			
		}
		return false;
	}
	//----------------------------
//==================================================================================
//==================================================================================	
	
	
//==================================================================================	
// Input Section
//==================================================================================	
	//input - returns an entered string value
	public static String input(){
		String inp1;
		Scanner  keyIn = new Scanner(System.in);
		inp1 = keyIn.nextLine();
		return inp1;
	}
	//------------------------------------------------------------------------------------

	
	
	public static void inln(){
		Scanner keyln = new Scanner(System.in);
		keyln.nextLine(); 
	}
//==================================================================================	
//==================================================================================


//==================================================================================
//Boolean checks
//==================================================================================
	
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	public static boolean dupeChk(String str1, ArrayList<String> list1){
		String str2;
		ArrayList <String> list2 = new ArrayList<String>();
		for(int i = 0;i < list1.size();i++){
			list2 = delimitReturn(list1.get(i), ",");
			str2 = list2.get(list2.size()-1);
			if (str1.equals(str2)) {
				println("DUPLICATE RECORD! BAD!");
				return false;
			}
		}
		return true;
	}
	
	public static boolean isNumeric(String s1){
		try{
			double d = Double.parseDouble(s1);
		}catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}	
	
	public static boolean backCheck(String str1){
		if (str1.toUpperCase().equals("B")){
			return true;
		}
		return false;
	}
	
//==================================================================================
//==================================================================================
	

//==================================================================================
// Strings and Ints
//==================================================================================	
	//ChoiceToInt - take choice and replaces it with an int value
	public static int choiceToInt(String s1){
		switch (s1.toUpperCase()){
			case  "PROGRAM":
				return 0;
			case "FACET":
				return 1;
			case "SUBSECTION":
				return 2;
			case "PROBLEM":
				return 3;
			case "VARIANT":
				return 4;
			case "SOLUTION":
				return 5;
			case "DOCUMENTATION":
				return 6;
			case "SPECIAL":
				return 7;
			default: 
				return 8;
		}
	}
	
		
	public static String intToChoice(int i){
		switch (i){
			case  0:
				return "Program";
			case 1:
				return "Facet";
			case 2:
				return "Subsection";
			case 3:
				return "Problem";
			case 4:
				return "Variant";
			case 5:
				return "Solution";
			case 6: 
				return "Documentation";					
			default: 
				return "Badnumber";
		}	
	}
		
	//valueOf - takes an input string(s1) an returns it as int (if it can) 
	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}
	//--------------------------------------------------------------------------------------
	
	//offsetCheck - given a section (s1), the method will return an offset int
	public static int offsetCheck(String s1){
	

		switch (s1.toUpperCase()){
		
			case "PROGRAM":	return 0;
									
			case "FACET": 	return 1;
			case "SUBSECTION": return 2;
			
			case "PROBLEM": return 3;
			
			case "VARIANT": return 4;
			
			case "SOLUTION": return 5;
			
			case "DOCUMENTATION": return 6;
			
			default:	return 0;
								
		}	
	}
	//--------------------------------------------------------------------------------------

	public static int inputCheck(String s1){
		
		if(isNumeric(s1)){
		return Integer.parseInt(s1);
		}else {
		return 0; 
		}
	
	}
	
	public static void setVars(ArrayList <String> variables){
		
		for(int i = 0;i < variables.size(); i++){
			switch (i) {
			
				case 0:
					menuSize = Integer.parseInt(delimitReturn(variables.get(i),":").get(1));
					break;
				case 1:
					fieldInputSize = Integer.parseInt(delimitReturn(variables.get(i),":").get(1));
					break;
				case 2:
					docHeaderSize = Integer.parseInt(delimitReturn(variables.get(i),":").get(1));
					break;
				case 3:
					docTextyContentBoxY = Integer.parseInt(delimitReturn(variables.get(i),":").get(1));
					break;
				case 4:
					docTextyContentBoxX = Integer.parseInt(delimitReturn(variables.get(i),":").get(1));
					break;
				case 5:
					currentOs = Integer.parseInt(delimitReturn(variables.get(i),":").get(1));
					break;
				default:
					break;
			}
		}
	
	}
//==================================================================================
//==================================================================================

	
//==================================================================================
//Text Read/Write
//==================================================================================	
	
	public static void addRecord(String section, ArrayList<String> sectList, String record){
		ArrayList <String> docList = new ArrayList <String>();
		int int1;
		int sectInt = choiceToInt(section);
		String entry;
		String temp="";
		boolean spaceCheck = false;
		
		for(int i = 0; i < sectList.size(); i++){
			println(((delimitReturn((sectList.get(i)),",")).get(sectInt)));
			println(sectInt);
			int1 = Integer.parseInt((delimitReturn((sectList.get(i)),",")).get(sectInt));
			if ((spaceCheck == false) && (int1 != i)){
				switch (sectInt) {
					case 0: //Program 
						sectList.add(i,(i + "," + record));
						break;
					case 1: //Facet
						sectList.add(i,(program.getProgCode() + "," + i + "," + record));
						println("thisfar");
						break;
					case 2: //Subsection
						sectList.add(i,(program.getProgCode() + "," + facet.getFacetCode() + "," + i + "," + record));
						break;
					case 3: //Problem
						sectList.add(i,(program.getProgCode() + "," + facet.getFacetCode() + "," + subsect.getSubsectCode() + "," + i + "," + record));
						break;
					case 4: //Variant
						sectList.add(i,(program.getProgCode() + "," + facet.getFacetCode() + "," + subsect.getSubsectCode() + "," + prob.getProbCode() + "," + i + "," + record));
						break;
					case 5: //Solution
						sectList.add(i,(program.getProgCode() + "," + facet.getFacetCode() + "," + subsect.getSubsectCode() + "," + prob.getProbCode() + "," + var.getVarCode() + "," + i + "," + record));
						break;
						case 6: //Documentation
						sectList.add(i,(program.getProgCode() + "," + facet.getFacetCode() + "," + subsect.getSubsectCode() + "," + prob.getProbCode() + "," + var.getVarCode() + "," + sol.getSolCode() + ","+ i));
						docList = ifList.getDoc2();
						docList.add(i,(i + "," + record));
						break;
					default:
						println("this wont work");
				}
				println("truth");
				spaceCheck = true;
				println("truth");
			}
		}
		if(spaceCheck == false){
			entry= ""+ sectList.size();
			switch (sectInt){
				case 6: //documentation
					temp = sol.getSolCode() + ",";
					docList = ifList.getDoc2();
					docList.add(entry+","+ record);
				case 5: //Solution
					temp = var.getVarCode() + "," +temp;
				case 4: //Variant
					 temp = prob.getProbCode() + "," + temp;
				case 3: // Problem
					temp = subsect.getSubsectCode() + "," + temp;
				case 2://Subsection
					temp = facet.getFacetCode() + "," + temp; 
				case 1://Facet
					temp = program.getProgCode() + "," + temp;
				case 0://Program
					if (sectInt < 6){
						entry += "," + record;
					}
					entry = temp + entry;
					break;
				default:
					println("Error");
					break;						
			}
			sectList.add(entry);
		}
		switch (sectInt){
			case 0:
				ifList.setProg(sectList);
				write(sectList,path.getProg());
				break;
			case 1:
				ifList.setFacet(sectList);
				write(sectList,path.getFacet());
				break;
			case 2:
				ifList.setSubSect(sectList);
				write(sectList,path.getSubSect());
				break;
			case 3: 
				ifList.setProb(sectList);
				write(sectList,path.getProb());
				break;
			case 4:
				ifList.setVar(sectList);
				write(sectList,path.getVar());				
				break;
			case 5:
				ifList.setSol(sectList);
				write(sectList,path.getSol());
				break;
			case 6:
				ifList.setDoc(sectList);
				ifList.setDoc2(docList);
				write(sectList,path.getDoc());
				write(docList,path.getDoc2());
				break;
		}
				
	}
	

	public static void write(ArrayList<String> list1, String path) {
		BufferedWriter writer = null;
        try {
            //create a temporary file
            File file = new File(path);

            // This will output the full path where the file will be written to...
            System.out.println(file.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(file));
			for(int i = 0; i < list1.size(); i++){
				writer.write(list1.get(i) + newLine);
			}
			
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }
}
	

class TextInFrame extends JFrame{
	public String frameLabel;
	public String boxText;
	public JTextField jT = new JTextField(); 
	public FlowLayout fL = new FlowLayout();
	
	public TextInFrame(String titleIn, String textyContents,int size){
		super(titleIn);
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
		jT.setText(textyContents);
		jT.setColumns(size);
		add(jT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLayout(fL);
		pack();
		setLocationRelativeTo(null);
	}
	
	public void SDisp(String titleIn, String textyContents,int size){
		setTitle("JTextField in a JFrame");
		setUndecorated(true);
		getRootPane().setWindowDecorationStyle(JRootPane.FRAME);
		add(new JTextField("POOP, Im a box!",25));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLayout(new FlowLayout());
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	
	}
}


class InputIt {
	public String st1;
	
	public InputIt(String direct,int x) {
		 st1 = JOptionPane.showInputDialog(null,direct + ": ");
	}
	
	public InputIt(String direct,String textyContents,int x) {
		st1 = JOptionPane.showInputDialog(null,direct + ": ",textyContents);
	}
		
	public String gInput(){
	return st1;
	}
}


class DocInput {
	public String docText = "";
	public String displayText = "";
	public JTextField header =new JTextField();
	public JTextArea documentation = new JTextArea();
	public JScrollPane scroll = new JScrollPane();
	public String searchChar;
	public DocInput(String title,String headerText,int headerSize,int boxX, int boxY,int currentOs) {
	
		header = new JTextField(headerText,headerSize);
		header.setEditable(false);
		documentation = new JTextArea("Enter Documentation:",boxX,boxY);
				scroll = new JScrollPane(documentation);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		searchChar = "";
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Header"),
					header,
					new JLabel("Documentation"),
					scroll,
			};
			JOptionPane.showMessageDialog(null, inputs, title, JOptionPane.PLAIN_MESSAGE);
			if (currentOs == 0){
				searchChar = System.getProperty("line.separator");
			}
			if (currentOs == 1){
				searchChar = "\r\n";
			}
			if (currentOs ==2){
				searchChar = String.valueOf('\r');
			}
			if (currentOs ==3){
				searchChar = System.lineSeparator();
			}
			if (currentOs == 4){
				searchChar = "\n";
			}
			
			
			if (currentOs == 0){
			docText = documentation.getText().replace(searchChar, "$!");
			}else if (currentOs == 1){
			docText = textAreaConvert(documentation.getText(),"$!");
			}
			docText = docText.replace(",", "@4");
			displayText = docText.replace("$!", searchChar);
			displayText = displayText.replace( "@4",",");
			System.out.println("You entered " + header.getText() + " , " +displayText);		
	}
		
	public DocInput(String title,String headerText,String textyContents,int headerSize,int boxX, int boxY, int currentOs) {
	
		header = new JTextField(headerText,headerSize);
		header.setEditable(false);
			if (currentOs == 0){
				searchChar = System.getProperty("line.separator");
			}
			if (currentOs == 1){
				searchChar = "\r\n";
			}
			if (currentOs ==2){
				searchChar = String.valueOf('\r');
			}
			if (currentOs ==3){
				searchChar = System.lineSeparator();
			}
			if (currentOs == 4){
				searchChar = "\n";
			}
		
		documentation = new JTextArea(textyContents.replace("$!", searchChar).replace( "@4",","),boxX,boxY);
		scroll = new JScrollPane(documentation);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		
			final JComponent[] inputs = new JComponent[] {
					new JLabel("Header"),
					header,
					new JLabel("Documentation"),
					scroll,
			};
			JOptionPane.showMessageDialog(null, inputs, title, JOptionPane.PLAIN_MESSAGE);
			
			if (currentOs == 0){
			docText = documentation.getText().replace(searchChar, "$!");
			}else if (currentOs == 1){
			docText = textAreaConvert(documentation.getText(),"$!");
			}

			docText = docText.replace(",", "@4");
			displayText = docText.replace("$!", searchChar);
			displayText = displayText.replace( "@4",",");
			
			System.out.println("You entered " + header.getText() + ", " +docText);		
	}
	
	public static String textAreaConvert (String str1,String replaceNl){
		BufferedWriter writer = null;
		ArrayList<String> list = new ArrayList<String>();
		String str2 ="";
        try {
            //create a temporary file
            File file = new File("temp.txt");

            // This will output the full path where the file will be written to...
            System.out.println(file.getCanonicalPath());

            writer = new BufferedWriter(new FileWriter(file));
			writer.write(str1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
		
		try{
			File file2 = new File("temp.txt");		
			FileReader fr = new FileReader(file2);
			BufferedReader br = new BufferedReader(fr);
			String data;
			while((data = br.readLine()) != null){
				list.add(data);
			}
		} catch(IOException e) {
			System.out.println("Bad !");
		}
		for (int i = 0; i < list.size();i++){
			str2 = str2 + list.get(i);
			if(i < list.size()-1){
				str2 = str2+ replaceNl;
			}
		}
		return str2;
	}	
	public String gInput(){
		return docText;
	}
}


class Path {
	public ArrayList <String> pathList = new ArrayList<String>();
	public String prog;
	public String facet;
	public String subsect;
	public String problem;
	public String variant;
	public String solution;
	public String documentation;
	public String documentation2;
	
	public Path() {
		prog = "";
		facet = "";
		subsect = "";
		problem = "";
		variant = "";
		solution = "";
		documentation = "";
		documentation2 = "";
	}
	
	public Path(ArrayList <String> newPath){
		pathList = newPath;
		prog = pathList.get(0);
		facet = pathList.get(1);
		subsect = pathList.get(2);
		problem = pathList.get(3);
		variant = pathList.get(5);
		solution = pathList.get(4);
		documentation = pathList.get(6);
		documentation2 = pathList.get(7);
	}
	
	public void setAllPaths(ArrayList <String> newPath){
		pathList = newPath;
		prog = pathList.get(0);
		facet = pathList.get(1);
		subsect = pathList.get(2);
		problem = pathList.get(3);
		variant = pathList.get(5);
		solution = pathList.get(4);
		documentation = pathList.get(6);
		documentation2 = pathList.get(7);
	}
	
	
	
	public String getProg(){
		return prog;
	}
	public String getFacet(){
		return facet;
	}
	public String getSubSect(){
		return subsect;
	}
	public String getProb(){
		return problem;
	}
	public String getVar(){
		return variant;
	}
	public String getSol(){
		return solution;
	}
	public String getDoc(){
		return documentation;
	}
	public String getDoc2(){	
		return documentation2;
	}		
}
	//importCsvPaths- grabs manually recorded path files located in text file in program directory
	// the file paths are separated from the csv structure and then returned at t
/* 	public List <String> importTxtPaths() {

		csvRw path = new csvRw("path.csv");
		for (int i = 0; i < path.size(); i++) {
		
		path.printItemLine();
		}
	}	
}  */

class IfLists {
		public ArrayList<String> programList;
		public ArrayList<String> facetList;
		public ArrayList<String> subsectList;
		public ArrayList<String> problemList;
		public ArrayList<String> variantList;
		public ArrayList<String> solutionList;
		public ArrayList<String> documentationList;
		public ArrayList<String> documentationList2;
		
		public IfLists() {
		programList = new ArrayList<String>();
		facetList = new ArrayList<String>();
		subsectList = new ArrayList<String>();
		problemList = new ArrayList<String>();
		variantList = new ArrayList<String>();
		solutionList = new ArrayList<String>();
		documentationList = new ArrayList<String>();
		documentationList2 = new ArrayList<String>();
		}
		
		
	public void setProg(ArrayList<String> list1){
		 programList = list1;
	}
	public void setFacet(ArrayList<String> list1){
		 facetList = list1;
	}
	public void setSubSect(ArrayList<String> list1){
		 subsectList = list1;
	}
	public void setProb(ArrayList<String> list1){
		 problemList = list1;
	}
	public void setVar(ArrayList<String> list1){
		 variantList = list1;
	}
	public void setSol(ArrayList<String> list1){
		 solutionList = list1;
	}
	public void setDoc(ArrayList<String> list1){
		 documentationList = list1;
	}
	public void setDoc2(ArrayList<String> list1){
		 documentationList2 = list1;
	}
	
	public ArrayList<String> getProg(){
		return programList;
	}
	public ArrayList<String> getFacet(){
		return facetList;
	}
	public ArrayList<String> getSubSect(){
		return subsectList;
	}
	public ArrayList<String> getProb(){
		return problemList;
	}
	public ArrayList<String> getVar(){
		return variantList;
	}
	public ArrayList<String> getSol(){
		return solutionList;
	}
	public ArrayList<String> getDoc(){
		return documentationList;
	}
	public ArrayList<String> getDoc2(){
		 return documentationList2;
	}
		
}
		
class Program {

	public String programName;
	public int progCode;
	
	public Program(){
		programName ="";
		progCode = 0;
	}
	public Program(String setName,int code) {
		programName = setName;
		progCode = code;
	}
	
	public Program(ArrayList<String> list1){
		programName = list1.get(1);
		progCode = valueOf(list1.get(0));				
	}
	
	public String getProgName() {
		return programName;
	}
	
	public int getProgCode(){
		return progCode;
	}
	
	public void setBoth(ArrayList<String> list1){
		programName = list1.get(1);
		progCode = valueOf(list1.get(0));	
	}
	
	public void setProgName(String name) {
		programName = name;
	}
	
	
	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}

	
	public void setProgCode(int code) {
		progCode = code;
	}
}

class Facet {

	public String facetName;
	public int facetCode;
	
	public Facet(){
		facetName ="";
		facetCode = 0;
	}
	public Facet(String setName,int code) {
		facetName = setName;
		facetCode = code;
	}
	
	public Facet(ArrayList<String> list1){
		facetName = list1.get(2);
		facetCode = valueOf(list1.get(1));				
	}
	
	public String getFacetName() {
		return facetName;
	}
	
	public int getFacetCode(){
		return facetCode;
	}
	
	public void setFacetName(String name) {
		facetName = name;
	}
	public void setBoth(ArrayList<String> list1){
		facetName = list1.get(1);
		facetCode = valueOf(list1.get(0));	
	}
	
	
	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}

	
	public void setFacetCode(int code) {
		facetCode = code;
	}
}

class Subsection {

	public String subsectionName;
	public int subsectionCode;
	
	public Subsection(){
		subsectionName ="";
		subsectionCode = 0;
	}
	public Subsection(String setName,int code) {
		subsectionName = setName;
		subsectionCode = code;
	}
	
	public Subsection(ArrayList<String> list1){
		subsectionName = list1.get(2);
		subsectionCode = valueOf(list1.get(1));				
	}
	
	public String getSubsectName() {
		return subsectionName;
	}
	
	public int getSubsectCode(){
		return subsectionCode;
	}

	public void setBoth(ArrayList<String> list1){
		subsectionName = list1.get(1);
		subsectionCode = valueOf(list1.get(0));	
	}

	
	public void setSubsectionName(String name) {
		subsectionName = name;
	}
	
	
	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}

	
	public void setSubsectCode(int code) {
		subsectionCode = code;
	}
}

class Problem {

	public String probName;
	public int probCode;
	
	public Problem(){
		probName ="";
		probCode = 0;
	}
	public Problem(String setName,int code) {
		probName = setName;
		probCode = code;
	}

		public void setBoth(ArrayList<String> list1){
		probName = list1.get(1);
		probCode = valueOf(list1.get(0));	
	}

	
	public Problem(ArrayList<String> list1){
		probName = list1.get(2);
		probCode = valueOf(list1.get(1));				
	}
	
	public String getProbName() {
		return probName;
	}
	
	public int getProbCode(){
		return probCode;
	}
	
	public void setProbName(String name) {
		probName = name;
	}
	
	
	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}

	
	public void setProbCode(int code) {
		probCode = code;
	}
}

class Variant {

	public String varName;
	public int varCode;
	
	public Variant(){
		varName ="";
		varCode = 0;
	}
	public Variant(String setName,int code) {
		varName = setName;
		varCode = code;
	}
	
	public Variant(ArrayList<String> list1){
		varName = list1.get(2);
		varCode = valueOf(list1.get(1));				
	}
	
	public String getVarName() {
		return varName;
	}

	public void setBoth(ArrayList<String> list1){
		varName = list1.get(1);
		varCode = valueOf(list1.get(0));	
	}

	
	public int getVarCode(){
		return varCode;
	}
	
	public void setVarName(String name) {
		varName = name;
	}
	
	
	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}

	
	public void setVarCode(int code) {
		varCode = code;
	}
}

class Solution {

	public String solName;
	public int solCode;
	
	public Solution(){
		solName ="";
		solCode = 0;
	}
	public Solution(String setName,int code) {
		solName = setName;
		solCode = code;
	}

	public void setBoth(ArrayList<String> list1){
		solName = list1.get(1);
		solCode = valueOf(list1.get(0));	
	}

	
	public Solution(ArrayList<String> list1){
		solName = list1.get(2);
		solCode = valueOf(list1.get(1));				
	}
	
	public String getSolName() {
		return solName;
	}
	
	public int getSolCode(){
		return solCode;
	}
	
	public void setSolName(String name) {
		solName = name;
	}
	
	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}
	
	public void setSolCode(int code) {
		solCode = code;
	}
}

class Documentation {

	public String documentationName;
	public int documentationCode;
	
	public Documentation(){
		documentationName ="";
		documentationCode = 0;
	}
	public Documentation(String setName,int code) {
		documentationName = setName;
		documentationCode = code;
	}
	
	public Documentation(ArrayList<String> list1){
		documentationName = list1.get(2);
		documentationCode = valueOf(list1.get(1));				
	}
	
	public String getDoc() {
		return documentationName;
	}
	
	public int getDocCode(){
		return documentationCode;
	}

	public void setBoth(ArrayList<String> list1){
		documentationName = list1.get(1);
		documentationCode = valueOf(list1.get(0));	
	}

	
	public void setDoc(String name) {
		documentationName = name;
	}
	

	public static int valueOf(String s1){
	int foo = Integer.parseInt(s1);
	return foo;
	}

	
	public void setDocCode(int code) {
		documentationCode = code;
	}
}
// If your reading this, sorry for the mess. I clean it up in here eventually. 