// Java Csv test prog
// Csv Read Class

/*  csvRW.java

	A Class to read and write to csv to maintain data with more permanence.
	
	Aug 23
*/ 

//Import
import java.io.*;
import java.util.*;
//

public class csvRW {
		
	private String csvRow;
	private List<String> csvList = new ArrayList<String>();
	private String path;
	public void csvRW (String pathIn)	{
		path = pathIn;
		BufferedReader csvFile = new BufferedReader (new FileReader(pathIn));
		csvRow = csvFile.readLine();
		while (csvRow != null){
			csvList.add(csvRow);
			csvRow = csvFile.readLine();
		}
		csvFile.close();
		
	}
	  //csvRW: using path input provided, method will produce an array list of csv lines
	public List <String> getItemLine(int index){
		List<String> itemList = new ArrayList<String>(Arrays.asList((csvList.get(index)).split(",")));
		return(itemList);
	}
	  //getItemline: returns an array list of a specific item line on the CSV
		
	public void printItemLine(int index){
		List<String> itemList = new ArrayList<String>(Arrays.asList((csvList.get(index)).split(",")));
	
		for (String item: itemList) {
			System.out.println(item + "\t");
		}
	}
	  //outputs specified line of csv array list
	
	public void printTable(){
		int i = 0;
		for(String item : csvList){
			printItemLine(i);
			i++;
		}	
	}	
	  //outputs table of all lines
	/*public void exportToCsv(String pathIn){
		path = pathIn;
		BufferedWriter newCsvFile = new BufferedWriter(new FileWriter(path));
		int i = 0;
		for (String item : csvList){
			newCsvFile.write(getItemLine(i).get(i));
			i++;
		}
		newCsvFile.close();
	}*/
	  //exports info back to Csv
	
/*public void updateCycle(){
		exportToCsv(path);
		csvRW(path);
	}*/	
	  //runs exportToCsv and csvRW in one method
	 
	public List <String> getList(){
	
	return csvList;	
	
	} 
	  //returns arrayList to the foreground
	
	public void listDump(ArrayList <String> passedArray) {
		csvList = passedArray;
	}
	
		
}
		