/******************************************************************************************
* Program name: ReadToken2.java                                                                                                                                               
* Name:Will Gardner                                                                                                                                                    
* Due Date: 11/2/16                                                                                                                                                    
* Program Description: Separates string into chars. starting off point for lab                                                                                                 
*******************************************************************************************/  

import java.io.*;
import java.util.StringTokenizer;

class ReadToken2
{
	public static void main (String[] args) throws IOException
	{
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String line;
		StringTokenizer reader;
		String delim ="\t\n\\s,";
		
		System.out.println("enter few number per line separated by space or a comma");
		line = stdin.readLine();
		reader = new StringTokenizer(line,delim);
		
		System.out.println("The number are");
		while(reader.countTokens() != 0)
			System.out.println(Integer.parseInt(reader.nextToken()));
	}//method main
}//class ReadToken2

