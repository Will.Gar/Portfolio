/*
 * Discount.java
 * Name: Will Gardner
 * Class:10/21/16
 * Description: Based on number of coffee bags, a discount is rendered
*/

import java.util.Scanner;
import java.text.DecimalFormat;


class Discount
{
	
	public static void main (String[] args)
	{
		
		//vars------------------------------------
		final int DISC_PERCENT_1 = 30;		//Discount Percentage for Discount 1
		final int DISC_PERCENT_2 = 25;		//Discount Percentage for Discount 2
		final int DISC_PERCENT_3 = 20;		//Discount Percentage for Discount 3
		final int DISC_PERCENT_4 = 15;		//Discount Percentage for Discount 4
		final int DISC_PERCENT_5 = 10;		//Discount Percentage for Discount 5
		final int DISC_PERCENT_6 = 5;		//Discount Percentage for Discount 6
		final int DISCOUNT_1 = 300;		//number of bags needed for discount 1		
		final int DISCOUNT_2 = 200;		//number of bags needed for discount 2
		final int DISCOUNT_3 = 150;		//number of bags needed for discount 3	
		final int DISCOUNT_4 = 100;		//number of bags needed for discount 4	
		final int DISCOUNT_5 = 50;		//number of bags needed for discount 5	
		final int DISCOUNT_6 = 25;		//number of bags needed for discount 6	
		final int BAG_PRICE = 550;		//price for each bag
		final double DOUBLE_DOLLAR=100;		//Switch to dollars
		int orderNum;				//number of bags
		int discount;				//discount for inputted bag amount

		DecimalFormat df = new DecimalFormat("'$'0.00");	//currency format
		DecimalFormat df2 = new DecimalFormat("#'%'");		//percent format
		Scanner scan = new Scanner(System.in);			//input grabber
		

		//input-----------------------------------
		System.out.println("Welcome to the program that gives discounts");
		System.out.print("Enter the number of bags for your order: ");
		orderNum= scan.nextInt();

		//calculations and output-----------------
		//Bag price
		System.out.println("Number of bags ordered: " + orderNum + " - " + df.format((orderNum * BAG_PRICE)/DOUBLE_DOLLAR)+"\n");

		//Discount
		discount = 0;
		if(orderNum >= DISCOUNT_6)
		{
			discount = DISC_PERCENT_6;
			if(orderNum >= DISCOUNT_5)
			{
				discount = DISC_PERCENT_5;
				if(orderNum >= DISCOUNT_4)
				{
					discount = DISC_PERCENT_4;
					if(orderNum >= DISCOUNT_3)
					{
						discount = DISC_PERCENT_3;
						if(orderNum >= DISCOUNT_2)
						{
							discount = DISC_PERCENT_2;
							if(orderNum >= DISCOUNT_1)
							{	
								discount = DISC_PERCENT_1;
							}
						}
					}
				}
			}
		}	
                     
		System.out.println("\tDiscount:");
		System.out.println("\t\t"+ df2.format(discount) + " - " + df.format((discount/DOUBLE_DOLLAR)*(orderNum * BAG_PRICE)/DOUBLE_DOLLAR));
		
		//Total
		System.out.println("Your total charge is: " + df.format((((100-discount)/DOUBLE_DOLLAR)*(orderNum * BAG_PRICE)/DOUBLE_DOLLAR)));
		scan.nextLine();
		System.out.println("thanks for playin'");
		
			
	}
}
