/****************************************************************                                                                                                       
* Program Name: Purchase.java                                                                                                                                               
* Name:Will Gardner                                                                                                                                                    
* Due Date: 9/29/16                                                                                                                                                    
* Program Description: Calculates prices divies into fine denominations
****************************************************************/                                                                                                      
import java.lang.Math;                                                                                                                                                  
import java.util.Scanner;                                                                                                                                               
import java.text.DecimalFormat;   

class Purchase
{
	public static void main(String[] args)
	{
		
		//Vars
		final int DOLLARIZE= 100; 		//Dollar amount in cents
		final int ROYALE_WITH_CHEESE = 25; 	//Quarter amount in cents
		final int DIME = 10; 			//Dime amount in cents
		final int NICKEL = 5; 			//Nickel amount cents
		final double DOUBLE_DOLLAR = 100;	//Floating point Dollar amount in cents
		int cents; 				//Input price and manupulated value
		int cents2; 				//place holder input value
		int tenderAmt;			//Second input value
		int tenderAmt2;			//place holder second input value
	
		DecimalFormat dF = new DecimalFormat("'$'#.00");//Money Format
		Scanner scanner = new Scanner(System.in);//Scanner Class
		
		//Input
		System.out.print("Welcome to the Simplified POS System");
		scanner.nextLine();
		System.out.print("Enter Price in cents:");
		cents = scanner.nextInt();
		System.out.print("Enter Amount Tendered in cents:");
		tenderAmt = scanner.nextInt();
		
		//Output+Calc
		System.out.println("\tPRICE:\t\t\t" + dF.format((cents/DOUBLE_DOLLAR)));
		System.out.println("\tAmount Tendered:\t" + dF.format(tenderAmt/DOUBLE_DOLLAR));
		System.out.println("\tChange Due:\t\t" + dF.format(((tenderAmt-cents)/DOUBLE_DOLLAR)));
		
		//Dollar Div
		System.out.println("\t\tDollars:\t"+((tenderAmt-cents)/DOLLARIZE));	
		cents = (tenderAmt-cents) % DOLLARIZE;
		
		//Quarter Div
		System.out.println("\t\tQuarters:\t"+cents/ROYALE_WITH_CHEESE);
		cents = cents % ROYALE_WITH_CHEESE;
		
		//Dime Div
		System.out.println("\t\tDimes:\t\t"+cents/DIME);
		cents = cents % DIME;

		//Nickel Div
		System.out.println("\t\tNickels:\t"+cents/NICKEL);
		cents=cents % NICKEL;
		//Remainder
		System.out.println("\t\tPennies:\t"+cents);
		
		scanner.nextLine();
		System.out.print("Thanks for Playin' Press ENTER to exit");
		scanner.nextLine();


	}

}


