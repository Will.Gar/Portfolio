// Coffee.java
// Name: Will Gardner
// Class:10/13/16
// Description: Divies up coffee in 20,10,5 2lb bags in boxes with the last box holding remainder
// single 2lb is a different price
//
//
//
//
//  not an if calculation for singular bag 
//  (int)(1.0/numOfBags)*((5.50*(int)(1.0/numOfBags)) + .60*( ((1-(1.0/numberofbags))+   )))
//

import java.text.DecimalFormat;
import java.util.Scanner;
import java.lang.Math;

class Coffee
{
	public static void main (String[] args)
	{
		//Vars---------
		
		final int SINGLE_BAG_PRICE = 550; 	//Single Bag Price
		final int LARGE_BOX_AMOUNT= 20;
		final int MEDIUM_BOX_AMOUNT = 10;
		final int SMALL_BOX_AMOUNT = 5;
		final int LARGE_BOX_PRICE = 180; 		//large box price
		final int MEDIUM_BOX_PRICE = 100;		//medium box price
		final int SMALL_BOX_PRICE = 60;		//small box price
		final double DOUBLE_DOLLAR=100.0;
		int orderNum;				//number of bag or ordered (input)
		int tempInt;				//remainder
		int total;				//total price
		//input int
		DecimalFormat df = new DecimalFormat("'$'#.00");	//Currency format
		Scanner scantron = new Scanner(System.in);		//Input grabber
		

		//Input--------
		System.out.println("Welcome to the coffee Order-Matic 9000.");
		System.out.print("Please enter the number of 2lb coffee bags you plan to order: ");
		orderNum = scantron.nextInt();
		
		
		//Calculation and output----------	
		//Bag total and price of bags
		System.out.println("----------------------------------------------");
		System.out.println("Number of Bags ordered: " + orderNum + " - " +  df.format(((orderNum*SINGLE_BAG_PRICE)/DOUBLE_DOLLAR))+"\n");
		System.out.println("----------------------------------------------");
		total = (orderNum*SINGLE_BAG_PRICE);
		//Box calculation
		System.out.println("Boxes used: ");
		
		//Large Box
		System.out.println("\t\t\t" + (orderNum/LARGE_BOX_AMOUNT) + " Large Boxes - " + df.format((((orderNum/LARGE_BOX_AMOUNT)*LARGE_BOX_PRICE)/DOUBLE_DOLLAR)));  
		tempInt = orderNum % LARGE_BOX_AMOUNT;
		total += (orderNum/LARGE_BOX_AMOUNT)*LARGE_BOX_PRICE;
		
		//Medium Box 
		System.out.println("\t\t\t"+(tempInt/MEDIUM_BOX_AMOUNT) + " Medium Boxes - " + df.format((((tempInt/MEDIUM_BOX_AMOUNT)*MEDIUM_BOX_PRICE)/DOUBLE_DOLLAR)));
		total += (tempInt/MEDIUM_BOX_AMOUNT)*MEDIUM_BOX_PRICE;
		tempInt = tempInt % MEDIUM_BOX_AMOUNT;

		//Small Box
		System.out.println("\t\t\t" + (tempInt/SMALL_BOX_AMOUNT + (int)(1-Math.pow(0,(tempInt%SMALL_BOX_AMOUNT)))) + " Small Boxes - " +  df.format(((((tempInt/SMALL_BOX_AMOUNT) + (1-Math.pow(0,(tempInt%SMALL_BOX_AMOUNT))))*SMALL_BOX_PRICE)/DOUBLE_DOLLAR)));
		
		total+= ((tempInt/SMALL_BOX_AMOUNT)+(1-Math.pow(0,(tempInt%SMALL_BOX_AMOUNT))))*SMALL_BOX_PRICE;
		//Total
		System.out.println("Your total cost is: " + df.format(total/DOUBLE_DOLLAR));
		scantron.nextLine();
		System.out.println("Thanks for playin'");
		
	

		//tempint/SMALL_BOX_AMOUNT [initial BOX amount]
		//tempint%SMALL_BOX_AMOUNT [remai
		//   
		//   
		//   1-Math.POW(0,remainder)
		//
		//   

						
		//	[large Box]- ((orderNum/LARGE_BOX_AMOUNT))
		//	[large Amount] - (((orderNum/LARGE_BOX_AMOUNT))*LARGE_BOX_PRICE)
		//	[medium Box] -(((orderNum%LARGE_BOX_AMOUNT))/MEDIUM_BOX_AMOUNT)
		//	[medium Amount] - ((((orderNum%LARGE_BOX_AMOUNT))/MEDIUM_BOX_AMOUNT)*MEDIUM_BOX_PRICE)
	
		//

		//
		//	
		//
	}
}
