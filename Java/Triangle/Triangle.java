/**
a class to describe a Triangle
*/
public class Triangle 
{
	/**
	instance variables
	**/ 
	private double side1;
	private double side2;
	private double side3;
	private static int count=0;
	
	/**
	default constructor
	*/
	public Triangle() 
	{      
		side1 = 0;
		side2 = 0;
		side3 = 0;
		count++;
	}     
	
	/**
	constructor
	@param side1 side1 of triangle
	@param side2 side2 of triangle
	@param side3 side3 of triangle
	**/
	public Triangle(double side1, double side2, double side3) 
	{
		this.side1 = side1;
		this.side2 = side2;
		this.side3 = side3;
		count++;
	} 
	
	/**
	isTriangle- verifies that obj is triangle
	@return true if it is a valid triangle     
	else returns false     
	a valid triangle is when all sides are positive, and every two sides are     
	greater then the third
	**/
	public boolean isTriangle() 
	{ 
		boolean isTri = false;
		isTri = ((side1+side2>side3)&&(side1+side3>side2)&&(side2+side3>side1)&&(side1*side2*side3>0));
		if (!isTri)
		{
			count--;
		}
		return isTri;
	}
	
	
	
	/**
	@return the side 1 of the triangle
	**/
	public double getSide1() 
	{     
		return side1;
	} 
	
	/**
	@return the side 2 of the triangle
	**/
	public double getSide2() 
	{      
		return side2;
	} 
	
	/**
	@return the side 3 of the triangle
	**/
	public double getSide3() 
	{     
		return side3;
	} 
	
	/**
	@param side1 of the triangle
	**/
	public void setSide1(double side1) 
	{     
		this.side1 = side1;
	}
	/**
	@param side2 of the triangle
	**/
	public void setSide2(double side2) 
	{     
		this.side2 = side2;
	} 
	/**
	@param side3 of the triangle
	**/
	public void setSide3(double side3) 
	{     
		this.side3 = side3;
	}
	
	/**
	@return the perimeter of the triangle
	**/
	public double perimeter() 
	{ 
		return side1+side2+side3;
	} 
	
	/** @deprecated as of 2014 - herons formula as is gets a little dicey as is with floating point datatypes 
		https://people.eecs.berkeley.edu/~wkahan/Triangle.pdf 
	 @return the area of the triangle    
	 the area is the square root of (s(s-a)(s-b)(s-c)), where s is half of the perimeter 
	**/
	public double area () 
	{    
		return Math.sqrt(((perimeter()*.5)*((perimeter()*.5)-(side1))*((perimeter()*.5)-(side2))*((perimeter()*.5)-(side3))));
	} 
	
	/**
	revised algorithm as heron's formula is a little unstable with floating point datatypes
	https://people.eecs.berkeley.edu/~wkahan/Triangle.pdf
	@see area
	 the area is the a quarter of the square root of ((a+(b+c)) (c-(a-b))(c+(a-b))(a+(b-c))
	 where a greater that or equal to b greater than or equal to c
	 @return area of triangle
	*/
	public double areaBerk ()
	{	
		double[] order = new double[3];
		order[0] = longestSide();
		if (order[0]==side1)
		{
			order[1] = Math.max(side2,side3);
		} else if (order[0]==side2)
		{
		order[1] = Math.max(side1,side3);
		} else if (order[0]==side3)
		{
			order[1] = Math.max(side2,side3);
		}	
		order[2] = shortestSide();
		return (.25*Math.sqrt(((order[0]+(order[1]+order[2]))*(order[2]-(order[0]-order[1]))*(order[2]+(order[0]-order[1]))*(order[0]+(order[1]-order[2])))));
		//double shoobie = (.25 * Math.sqrt(((order[0]+(order[1]+order[2]))*(order[2]-(order[0]-order[1]))*(order[2]+(order[0]-order[1]))*(order[0]+(order[1]-order[2])))));
		//return shoobie;
	}
	
	/**
	@return the shortest side of the triangle
	*/
	public double shortestSide() 
	{     
		return Math.min(side1,Math.min(side2,side3));
	}
	
	/**
	@return the longest side of the triangle
	**/
	public double longestSide() 
	{ 
		return Math.max(side1,Math.max(side2,side3));
	} 
	/**
	 multiply each side by the number r
	@param r - what to multiply the sides by
	**/
	public void scale (int r) 
	{      
		side1 *= r;
		side2 *= r;
		side3 *= r;
	} 
	/**
			@return "right trianle" (when (s1*s1 + s2*s2 = s3*s3) ||(s1*s1 + s3*s3 = s2*s2) ||  (s2*s2 + s3*s3 = s1*s1) 
	        @return "isosceles" ( two sides are equal to each other) 
	        @return "equilateral" (all sides are equal to each other) 
	        @return "isosceles and right triangle"  
	        @return "scalene" (if all sides are different) 
	**/
	public String typeOfTriangle() 
	{
		boolean right = ((side1*side1)+(side2*side2)==(side3*side3))||((side1*side1)+(side3*side3)==(side2*side2))||((side2*side2)+(side3*side3)==(side1*side1));
		double[] order = new double[3];
		if ((side1==side2)||(side1==side3)||(side2==side3))
		{
			if ((side1==side2)&&(side1==side3)&&(side2==side3))
				return ("equilateral");
			if (right)
				return ("isosceles and right");
			return ("isosceles");
		}
		if (right)
			return ("right");
		return ("scalene");
	} 
	/**
	 a method to determine if this triangle is equal to the one passed as a parameter
	@return true if triangles equal eachother
	@return false if they dont
	@param obj - triangle object
	**/
	public boolean equals ( Object obj)  
	{           
		boolean isEqual= false;
		Triangle r;
		if( obj instanceof Triangle)
		{
			r = (Triangle)obj;
			if( side1 == r.side1 && side2 == r.side2 && side3 == r.side3)
			{
				isEqual = true;
			}
		}
		return isEqual;
	}
	/**
	@return this Triangle as a String, displaying the three sides
	**/
	public String toString()
	{                      
		String s="side 1: " + side1 + "\n";
		s+="side 2: " + side2 + "\n";
		s+="side 3: " + side3 + "\n";
		return s;
	}        
	/**
	the method returns how many triangles where created so far
	@return count of triangles
	**/
	public static int countTriangles()          
	{
		return count;
	} 
}// end class 