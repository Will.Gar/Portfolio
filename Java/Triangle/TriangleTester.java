/**
 a class to test to use and Rectangle
*/

import java.io.*;
import java.util.*;
public class TriangleTester
{
	public static void main(String[] args) throws IOException
	{
		final String NEWLINE =  (String)System.getProperty("line.separator");
		Triangle t1,t2;
		String[] input;
		double[] sides;
		
		input = tempGetSides2();
		
		for (int i = 0;i<input.length;i++)
		{
			sides = tokenConvert(input[i]);
		
			System.out.println("Welcome to the Triangle maker!");
			for (int k=0;k<sides.length;k++)
			{
				System.out.println("Side" + k + ": " + sides[k]);
			}
		}
		
	}
	
	public static String[] tempGetSides()
	{
		String[] str1;
		str1 =new String[] {"3 4 8","1 1 3","2 3 5"};
		return str1;
	}
	public static String[] tempGetSides2() throws IOException
	{
		int count = 0;
		Scanner stdin = new Scanner(new File("triangleInput.txt"));	//Input getter
		String[] str1 = new String[countLines("triangleInput.txt")];
		while (stdin.hasNextLine())
		{
			str1[0]= stdin.nextLine();
			count++;
			
		}
		//str1 =new String[] {"3 4 8","1 1 3","2 3 5"};
		return str1;
	}
	public static int countLines(String filename) throws IOException
	{
		final String NEWLINE =  (String)System.getProperty("line.separator");
		LineNumberReader lnr = new LineNumberReader(new FileReader(new File(filename)));
		lnr.skip(Long.MAX_VALUE);
		int i = (int)(lnr.getLineNumber()+1);
		lnr.close();
		return i;
		
	}
	public static double[] tokenConvert(String input)
	{
		final String NEWLINE =  (String)System.getProperty("line.separator");
		double[] tokenDoubles = new double[3];
		int count=0;
		StringTokenizer reader;
		String delim =" \t\r,"+NEWLINE;
		reader= new StringTokenizer(input,delim,false);
		System.out.println(input);
		while (reader.hasMoreTokens())
		{
			tokenDoubles[count] = Double.parseDouble(reader.nextToken());
			count++;
		}
		count=0;
		return tokenDoubles;
	}
	
	
	
}