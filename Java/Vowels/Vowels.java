/******************************************************************************************
 * * Program name: Vowels.java                                                                                                                                               
 * * Name:Will Gardner                                                                                                                                                    
 * * Due Date: 11/2/16                                                                                                                                                    
 * * Program Description: Separates string into chars. Counts the consonants and vowels                                                                                                 
 * *******************************************************************************************/


import java.io.*;
import java.util.StringTokenizer;


public class Vowels 
{
	public static void main(String[] args) throws IOException
	{

		//constant
		final boolean RETURNDELIMS = true; 					//Set string tokenizer to return delimeted data		
		final int MIN_UPPERCASE=65;							//minimum ascii value of an uppercase letter
		final int MAX_UPPERCASE=90;							//maximum ascii value of an uppercase letter
		final int MIN_LOWER=97;								//minimum ascii value of a lowercase letter
		final int MAX_LOWER=122;							//maximum ascii value of a lowercase letter
		final int UA = 65;									//A ascii value
	    final int UE = 69;									//E ascii value
		final int UI = 73;									//I ascii Value
		final int UO = 79;									//O ascii value
		final int UU = 85;									//U ascii value
		final int LA = 97;									//a ascii value
		final int LE = 101;									//e ascii value
		final int LI = 105;									//i ascii value
		final int LO = 111;									//o ascii value
		final int LU = 117;									//u ascii value
		final String DELIM = "aeiouAEIOU";					//what symbols are counted as a delimiter
		final String newline =  (String)System.getProperty("line.separator");
		
		
		
		//vars
		boolean cont = true;								//used to check whether the human element wishes to continue
		boolean checkForAnswer = false;						//used to chech check that the human element actually stated that they want to continue
		int numberOfVowels=0;								//total number of vowels in the set strings
		int numberOfNonVowels=0;							//number of nonvowels in the set of strings
		int lineCount=0;									//counts the number of lines
		int a=0;											//tallies a's in a string
		int e=0;											//tallies e's in a string
		int i=0;											//tallies i's in a string
		int o=0;											//tallies o's in a string
		int u=0;											//tallies u's in a string
		int totalA=0;										//total number of a's in the set of strings										
		int totalE=0;										//total number of e's in the set of strings
		int totalI=0;										//total number of i's in the set of strings
		int totalO=0;										//total number of o's in the set of strings
		int totalU=0;										//total number of u's in the set of strings
		String token;										//token string
		String line;										//line input
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));	//Input getter
		StringTokenizer reader;								//Splits strings into tokens. In this case,the string will be broken down to each char
		

		//Title and input loop
		System.out.println("You are currently enjoying the experience of ..." + newline + "\tTHE VOWEL COUNTER");	
		//while loop1 - line iteration
		while(cont)
		{
			lineCount++;
			System.out.print("Enter Line " + lineCount +":");
			line = stdin.readLine();
			reader = new StringTokenizer(line,DELIM,RETURNDELIMS);	

			//while loop 2 - token iteration
			while(reader.hasMoreTokens())
			{	
				token = reader.nextToken();
				//Token length check - Vowels only occur when token.length == 1
				if(token.length() == 1)
				{
					//Single Letter Switch
					switch (token.codePointAt(0))
					{
						case UA:
							a++;
							numberOfVowels++;
							break;
						case UE:
							e++;
							numberOfVowels++;
							break;

						case UI:
							i++;
							numberOfVowels++;
							break;

						case UO:
							o++;
							numberOfVowels++;
							break;

						case UU:
							u++;
							numberOfVowels++;
							break;

						case LA:
							a++;
							numberOfVowels++;
							break;
						case LE:
							e++;
							numberOfVowels++;
							break;

						case LI:
							i++;
							numberOfVowels++;
							break;

						case LO:
							o++;
							numberOfVowels++;
							break;

						case LU:
							u++;
							numberOfVowels++;
							break;
						default:
							numberOfNonVowels++;
					}//Switch
				}else
					numberOfNonVowels+=token.length();		
			}//while loop 2
			
			//Line Output + totalling
			System.out.println("A_count:" + a);
			totalA+=a;
			System.out.println("E_count:" + e);
			totalE+=e;
			System.out.println("I_count:" + i);
			totalI+=i;
			System.out.println("O_count:" + o);
			totalO+=o;
			System.out.println("U_count:" + u);
			totalU+=u;
		

			//while loop 3 = Continuation
			while(!checkForAnswer)
			{
				System.out.println("Do you want to continue?{[Y|y]|[N|n]||[Y|y]es||[N|n]o}");
				switch(stdin.readLine())
				{
					case "no":
					case "No":
					case "n":
					case "N":
						cont = false;
						checkForAnswer = true;
						break;
					case "yes":
					case "Yes":
					case "y":
					case "Y":
						checkForAnswer = true;
						break;
					default:
						System.out.print("You didn't answer in an understandable way. TRY AGAIN.");
				}
			}//while loop 3
			
			//reset counters
			checkForAnswer = false;
			a=0;
			e=0;
			i=0;
			o=0;
			u=0;
		}//While Loop 1
		
		
		//Finalized Output
		System.out.println("Total Number of Lines: " + lineCount);
		System.out.println("Total A: " + totalA);
		System.out.println("Total E: " + totalE);
		System.out.println("Total I: " + totalI);
		System.out.println("Total O: " + totalO);
		System.out.println("Total U: " + totalU);
		System.out.println("Total number of vowels: " + numberOfVowels); 
		System.out.println("Total number of nonvowels: " + numberOfNonVowels);
		System.out.println(newline + "Thanks for playin'");
	}//method main
}//class Vowels
