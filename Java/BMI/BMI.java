/****************************************************************
 * Program Name: BMI.java
 * Name:Will Gardner 
 * Due Date: 9/29/16
 * Program Description: Calculates BMI
 ****************************************************************/
import java.lang.Math;
import java.util.Scanner;
import java.text.DecimalFormat;


class BMI
{
	public static void main (String[] args)
	{
		
		final double CENTIMETER_CONVERSION=100.0; // used to convert centimeters to meters
		double weight; //weight[in kilos] 
		double height; //height[centimeters]
		double bMI; // the body mass index
		//double weight;
		//double height;
		Scanner scanner= new Scanner(System.in);
		DecimalFormat dF = new DecimalFormat("#0.00");
		

		//Input
		System.out.println("Welcome to the B.M.I. (Body Mass Index) Calculatron 2000");
		scanner.nextLine();
		System.out.print("enter Weight in kilos: ");
		//weight = scanner.nextDouble();
		weight = scanner.nextDouble();
		System.out.print("enter Height in centimeters: ");
		//height = scanner.nextDouble();
		height = scanner.nextDouble();

		//Calculation
		//CalcBMI bMI = new CalcBMI(weight,height);
		bMI = weight/(Math.pow((height/CENTIMETER_CONVERSION),2));
	
		//Output
		//System.out.println("Your BMI is ..." +bMI.getBMI());
		System.out.println("Your BMI is ..." + dF.format(bMI));
		scanner.nextLine();
		System.out.print("Thanks for Playin'"); 
		

	}
}	
		

