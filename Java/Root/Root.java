/****************************************************************                                                                                                       
* Program Name: Root.java                                                                                                                                               
* Name:Will Gardner                                                                                                                                                    
* Due Date: 9/29/16                                                                                                                                                    
* Program Description: Calculates roots of polynomial using quadratic equation
****************************************************************/                                                                                                      
import java.lang.Math;                                                                                                                                                  
import java.util.Scanner;                                                                                                                                               
  
class Root
{



	public static void main (String[] args)
	{
		//Vars
		double dblA;		//-Input Coefficient A
		double dblB; 		//-Input Coefficient B
		double dblC; 		//-Input Constant C
		double rootOne; 	//-Resulting Root1
	   	double rootTwo; 	//-Resulting Root2
		Scanner scanner = new Scanner(System.in);
		

		//Input
		System.out.print("Welcome to the root finder");
		scanner.nextLine();
		System.out.println("Please enter A,B,& C of equation:");
		System.out.print("Enter A:");
		dblA = scanner.nextDouble();
		System.out.print("Enter B:");
		dblB = scanner.nextDouble();
		System.out.print("Enter C:");
		dblC = scanner.nextDouble();
		
		//Calc
		rootOne = (-1*dblB - (Math.sqrt((Math.pow(dblB,2)-(4.0*dblA*dblC)))))/(2.0*dblA);
		rootTwo = (-1*dblB + (Math.sqrt((Math.pow(dblB,2)-(4.0*dblA*dblC)))))/(2.0*dblA);

		//Output
		System.out.println("Ok. your roots are... \nRoot 1: " + rootOne + "\nRoot 2: "+ rootTwo);
		scanner.nextLine();
		System.out.println("Thanks for Playin'. Press ENTER to exit");
		scanner.nextLine();
		
	}	

}



