/******************************************************************************************
 * * Program name: Vowels.java                                                                                                                                               
 * * Name:Will Gardner                                                                                                                                                    
 * * Due Date: 11/2/16                                                                                                                                                    
 * * Program Description: Separates string into chars. Counts the consonants and vowels                                                                                                 
 * *******************************************************************************************/


import java.io.*;
import java.util.StringTokenizer;

class Vowels 
{
	public static void main(String[] args) throws IOException
	{

		//constant
		final boolean RETURNDELIMS = true; 						//Useful for String tokenizer		
		final int MIN_UPPERCASE=65;							//minimum ascii value of an uppercase letter
		final int MAX_UPPERCASE=90;							//maximum ascii value of an uppercase letter
		final int MIN_LOWER=97;								//minimum ascii value of a lowercase letter
		final int MAX_LOWER=122;							//maximum ascii value of a lowercase letter
		final int UA = 65;									//A ascii value
	       	final int UE = 69;									//E ascii value
		final int UI = 73;									//I ascii Value
		final int UO = 79;									//O ascii value
		final int UU = 85;									//U ascii value
		final int LA = 97;									//a ascii value
		final int LE = 101;									//e ascii value
		final int LI = 105;									//i ascii value
		final int LO = 111;									//o ascii value
		final int LU = 117;									//u ascii value
		final String DELIM = "aeiouAEIOU";						//what symbols are counted as a delimiter
		
		//vars
		boolean cont = true;								//used to check whether the human element wishes to continue
		boolean checkForAnswer = false;							//used to chech check that the human element actually stated that they want to continue
		int numOfVowels=0;								//number of vowels in a string
		int numOfNonVowels=0;
		int lineCount=1;
		int a=0;											
		int e=0;
		int i=0;
		int o=0;
		int u=0;
		int totalA=0;
		int totalE=0;
		int totalI=0;
		int totalO=0;
		int totalU=0;
		String token;
		String line;
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));	//Input getter
		StringTokenizer reader;								//Splits strings into tokens. In this case,the string will be broken down to each char
		

		//Title and input loop
		System.out.print("You are currently enjoying the experience of ...\n\tTHE VOWEL COUNTER");	
		//while loop1 - line iteration
		while(cont)
		{
			line = stdin.readLine();
			reader = new StringTokenizer(line,DELIM,RETURNDELIMS);	

			//while loop 2 - token iteration
			while(reader.hasMoreTokens())
			{	
				token = reader.nextToken();
				//Token length check - Vowels only occur when token.length == 1
				if(token.length() > 1)
				{
					
					//Single Letter Switch
					switch (token.codePointAt(0))
					{
						case UA:
							a++;
							numberOfVowels++;
							break;
						case UE:
							e++;
							numberOfVowels++;
							break;

						case UI:
							i++;
							numberOfVowels++;
							break;

						case UO:
							o++;
							numberOfVowels++;
							break;

						case UU:
							u++;
							numberOfVowels++;
							break;

						case LA:
							a++;
							numberOfVowels++;
							break;
						case LE:
							e++;
							numberOfVowels++;
							break;

						case LI:
							i++;
							numberOfVowels++;
							break;

						case LO:
							o++;
							numberOfVowels++;
							break;

						case LU:
							u++;
							numberOfVowels++;
							break;
						default:
							numberOfNonVowels++;
					}//Switch
				}else
					numberOfNonVowel+=token.length;		
			}//while loop 2
			
			//Line Output + totalling
			numberOfLines++;
			System.out.print("A_count:" + a);
			totalA+=a;
			System.out.print("E_count:" + e);
			totalE+=e;
			System.out.print("I_count:" + i);
			totalI+=i;
			System.out.print("O_count:" + o);
			totalo+=o;
			System.out.print("U_count:" + u);
			totalU+=u;
		

			//while loop 3 = Continuation
			while(!checkForAnswer)
			{
				System.out.print("Do you want to continue?{"[Y|y]\|[N|n]||[Y|y]es||[N|n]o)");
				switch(stdin.readLine())
				{
					case "no":
					case "No":
					case "n":
					case "N":
						cont = false;
						checkForAnswer = true;
						break;
					case "yes":
					case "Yes":
					case "y":
					case "Y":
						checkForAnswer = true;
						break;
					default:
						System.out.print("You didn't answer in an understandable way. TRY AGAIN.");
				}
			}//while loop 3
			
			//reset counters
			checkForAnswer = false;
			a=0;
			e=0;
			i=0;
			o=0;
			u=0;
		}//While Loop 1
		
		
		//Finalized Output
		System.out.println("Total A: " + a);
		System.out.println("Total E: " + e);
		System.out.println("Total I: " + i);
		System.out.println("Total O: " + o);
		System.out.println("Total U: " + u);
		System.out.println("Total number of vowels: " + numberOfVowels); 
		System.out.println("Total number of nonvowels: " + numberOfNonVowels);
		System.out.println("\nThanks for playin'");
	}//method main
}//class Vowels
