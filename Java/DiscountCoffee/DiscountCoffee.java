// Coffee.java
// Name: Will Gardner
// Class:10/13/16
// Description: Divies up coffee in 20,10,5 2lb bags in boxes with the last box holding remainder. Discount now added.

import java.text.DecimalFormat;
import java.util.Scanner;
import java.lang.Math;

class DiscountCoffee 
{
	public static void main (String[] args)
	{
		//Vars---------
		
		final int SINGLE_BAG_PRICE = 550; 	//Single Bag Price
		final int LARGE_BOX_AMOUNT= 20;
		final int MEDIUM_BOX_AMOUNT = 10;
		final int SMALL_BOX_AMOUNT = 5;
		final int LARGE_BOX_PRICE = 180; 		//large box price
		final int MEDIUM_BOX_PRICE = 100;		//medium box price
		final int SMALL_BOX_PRICE = 60;		//small box price
		final int DISC_PERCENT_1 = 30;		//Discount Percentage for Discount 1
		final int DISC_PERCENT_2 = 25;		//Discount Percentage for Discount 2
		final int DISC_PERCENT_3 = 20;		//Discount Percentage for Discount 3
		final int DISC_PERCENT_4 = 15;		//Discount Percentage for Discount 4
		final int DISC_PERCENT_5 = 10;		//Discount Percentage for Discount 5
		final int DISC_PERCENT_6 = 5;		//Discount Percentage for Discount 6
		final int DISCOUNT_1 = 300;		//number of bags needed for discount 1		
		final int DISCOUNT_2 = 200;		//number of bags needed for discount 2
		final int DISCOUNT_3 = 150;		//number of bags needed for discount 3	
		final int DISCOUNT_4 = 100;		//number of bags needed for discount 4	
		final int DISCOUNT_5 = 50;		//number of bags needed for discount 5	
		final int DISCOUNT_6 = 25;		//number of bags needed for discount 6
		final double DOUBLE_DOLLAR=100.0;
		int orderNum;				//number of bag or ordered (input)
		int tempInt;				//remainder
		int discount;				//discount rendered from input
		int total;				//total price
		

		//input int
		DecimalFormat df = new DecimalFormat("'$'#.00");	//Currency format
		DecimalFormat df2 = new DecimalFormat("#'%'");		//Percent format
		Scanner scantron = new Scanner(System.in);		//Input grabber
		

		//Input--------
		System.out.println("Welcome to the coffee Order-Matic 9000.");
		System.out.print("Please enter the number of 2lb coffee bags you plan to order: ");
		orderNum = scantron.nextInt();
		
		
		//Calculation and output----------	
		//Bag total and price of bags
		System.out.println("----------------------------------------------");
		System.out.println("Number of Bags ordered: " + orderNum + " - " +  df.format(((orderNum*SINGLE_BAG_PRICE)/DOUBLE_DOLLAR))+"\n");
		System.out.println("----------------------------------------------");
		total = (orderNum*SINGLE_BAG_PRICE);
		
		
		//Discount Calculation
		discount = 0;
		if(orderNum >= DISCOUNT_6)
		{
			discount = DISC_PERCENT_6;
			if(orderNum >= DISCOUNT_5)
			{
				discount = DISC_PERCENT_5;
				if(orderNum >= DISCOUNT_4)
				{
					discount = DISC_PERCENT_4;
					if(orderNum >= DISCOUNT_3)
					{
						discount = DISC_PERCENT_3;
						if(orderNum >= DISCOUNT_2)
						{
							discount = DISC_PERCENT_2;
							if(orderNum >= DISCOUNT_1)
							{	
								discount = DISC_PERCENT_1;
							}
						}
					}
				}
			}
		}	
		
		System.out.println("\t\tDiscount:");
		System.out.println("\t\t\t"+ df2.format(discount) + " - " + df.format(((discount)*(orderNum * SINGLE_BAG_PRICE))/Math.pow(DOUBLE_DOLLAR,2)));
		total -= ((discount)*(orderNum * SINGLE_BAG_PRICE))/Math.pow(DOUBLE_DOLLAR,1); 
		
		
		//Box calculation
		System.out.println("Boxes used: ");
		
		//Large Box
		System.out.println("\t\t\t" + (orderNum/LARGE_BOX_AMOUNT) + " Large Boxes - " + df.format((((orderNum/LARGE_BOX_AMOUNT)*LARGE_BOX_PRICE)/DOUBLE_DOLLAR)));  
		tempInt = orderNum % LARGE_BOX_AMOUNT;
		total += (orderNum/LARGE_BOX_AMOUNT)*LARGE_BOX_PRICE;
		
		//Medium Box 
		System.out.println("\t\t\t"+(tempInt/MEDIUM_BOX_AMOUNT) + " Medium Boxes - " + df.format((((tempInt/MEDIUM_BOX_AMOUNT)*MEDIUM_BOX_PRICE)/DOUBLE_DOLLAR)));
		total += (tempInt/MEDIUM_BOX_AMOUNT)*MEDIUM_BOX_PRICE;
		tempInt = tempInt % MEDIUM_BOX_AMOUNT;

		//Small Box
		System.out.println("\t\t\t" + (tempInt/SMALL_BOX_AMOUNT + (int)(1-Math.pow(0,(tempInt%SMALL_BOX_AMOUNT)))) + " Small Boxes - " +  df.format(((((tempInt/SMALL_BOX_AMOUNT) + (1-Math.pow(0,(tempInt%SMALL_BOX_AMOUNT))))*SMALL_BOX_PRICE)/DOUBLE_DOLLAR)));
		
		total+= ((tempInt/SMALL_BOX_AMOUNT)+(1-Math.pow(0,(tempInt%SMALL_BOX_AMOUNT))))*SMALL_BOX_PRICE;
		//Total
		System.out.println("Your total cost is: " + df.format(total/DOUBLE_DOLLAR));
		scantron.nextLine();
		System.out.println("Thanks for playin'");
		
	

	}
}

