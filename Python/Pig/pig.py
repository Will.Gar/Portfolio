#pig.py - pig game 
#Will Gardner
#CPT 101
import math as m
import random as r

#computer_turn - handles each computer roll, output results of each roll, and returns score for that turn
#input-     playerScore-score of user
#           CPUScore - score of computer player
#output-    computer score at the end of the round
def computer_turn(playerScore,CPUScore):
    roundPoints=0
    cycles=0
    roll = 2
    print("CPU TURN:")
    while(roll > 1):
        roll = cpu_decision(playerScore,CPUScore,cycles,roundPoints)
        roundPoints = round_path("CPU",roll,roundPoints)
        cycles+=1
    print("CPU SCORE: " +str(roundPoints + CPUScore))
    return (roundPoints + CPUScore)
    
#cycle_stress - returns a point value based on the number of times rolled
def cycleStress(cycles):
    thresh=50
    cycleScore =(1+2**cycles-1)
    return (thresh if (cycleScore>thresh) else cycleScore)


#underDogPush - return a point value based on the difference between player and cpuscore
def underDogPush(playerScore,CPUScore):
    max=-70
    score= -(playerScore-CPUScore)
    if (score>0):
        return 0
    elif (score<max):
        return max
    else:
        return score

#the closer the computer is to 100 the more likely it is to keep rolling
def winningDistance(CPUScore):
    max = -70 
    return int((float(max)*((2.)*float(2**-(100-CPUScore)))))

#cpu_decision - checks if < threshold, if not will roll else random but weighted decision based on user score, difference between scores, and distance from 100.
def cpu_decision(playerScore,CPUScore,cycles,roundPoints):
    decision = r.randint(0,100)
    maxi = 95
    mini = 5
    thresh =50
    modifier = cycleStress(cycles) + underDogPush(playerScore,CPUScore) + winningDistance(CPUScore)
    if ((modifier+thresh)<mini):
        thresh=mini
    elif ((modifier+thresh)>maxi):
        thresh=maxi
    else:
        thresh += modifier
    if roundPoints<20:
        return die_roll()
    else:
        if (decision >= thresh):
            return die_roll()
        else:
            return -1

#user_turn - handles interactions with user, returns score for that turn
#input-     player score- score of player
#output-    score of player at end of round
def user_turn(playerScore):
    roundPoints=0
    roll = 2
    print("PLAYER TURN:")
    while(roll > 1):
        print("Potential Score: " + str((playerScore + roundPoints)))
        roll = user_choice()
        roundPoints=round_path("Player",roll,roundPoints)
    return (playerScore+roundPoints)

#round path - facilitates interpretation of dice roll
#input -    name - name of current player
#           roll - int value representing dice roll
#           roundPoints - points of the round
def round_path(name,roll,roundPoints):
    if (roll == 1):
        roundPoints = 0
        print(name + " lost everything for this round.")
    elif (roll == -1):
        print(name + " takes the holding stance.")
    elif (roll > 1):
        roundPoints += roll
    return roundPoints


#user_choice - asks user if they wish to roll or hold, returning the result
#output-    -1 signifying a hold position or a dice roll
def user_choice():
    choice = -1
    
    while ((choice < 0) or (choice > 1)):
        try:  
            choice = int(input("Choose 0 to roll or 1 to hold:"))
            if  (choice == 0):
                return die_roll()
            elif (choice == 1):
                return -1
        except ValueError:
            print ("Enter either a 1 or a 0.")
    

#die_roll	- returns random value between 1 and 6
def die_roll():
    return r.randint(1,6)


#victory_checker - send in both current totals and checks if we have a winner, returns True if a winner is found
#input -    playerScore - player's score
#           CPUScore - Computer's score
#output - boolean value representing if someone has won the round or not
def victory_checker(playerScore,CPUScore):
    return ((playerScore >= 100) or (CPUScore >= 100))
 
#main - handles overall scores, main loop to alternate turns, winner announcement
def main():
    print("pig: the game [due to educational setting, ascii blood has been removed]")
    playerScore = 0
    CPUScore = 0
    while (not victory_checker(playerScore,CPUScore)):
        playerScore=user_turn(playerScore) 
        if (not victory_checker(playerScore,CPUScore)):
            CPUScore = computer_turn(playerScore,CPUScore)
    if (playerScore >= 100):
        print("Player Wins")
    elif (CPUScore >= 100):
        print("The robot overlords demand grovelling and ram chips as the price of experiencing their glorious domination.")
    print("FINAL SCORE: ")
    print("PLAYER:",playerScore)
    print("CPU:",CPUScore)
    


if __name__ == '__main__':
    main()
