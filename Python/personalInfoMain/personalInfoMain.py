#personalInfoMain-a personal program that utilizes the personalInfo class
import PersonalInfo as PI

def main():
    me= PI.PersonalInfo("Will","123 AshcroftMadeUp St.","219","555-5555")
    friend1=PI.PersonalInfo("Loneliness","Everwhere","-1","123-4567")
    friend2=PI.PersonalInfo("Whiskey","On the rocks","25","345-3559")
    print(me)
    print(friend1)
    print(friend2)
if __name__=="__main__":
    main()

