#populationStation.py - takes in a list of total populations from a file at a specified base year and returns statistics
#based on changes between the years


#getPopulationList- get a list of ints from a file
#input - path-string representing the path of a file
#output - list of ints
def getPopulationList(path):
    f = open(path,"r")
    listy=[]
    listy = f.readlines()
    for i in range(len(listy)):
        listy[i]=int(listy[i])
    return listy
#getPopulationChangeList - returns a list of differences between a sequence of ints
#input - listy- list of ints
#output - listerine - list of differences
def getPopulationChangeList(listy):
    listerine=[]
    for i in range(len(listy)-1):
        listerine.append((listy[i+1]-listy[i]))
    return listerine

#avg -- gets the average of values within a list
#input- listy- list of ints
#output- double value representing average
def avg(listy):
    pool=0.
    for num in listy:
        pool+=num
    return (pool/len(listy))

def main():
    print("US POPULATION ANALYZER.")
    base=1951
    popList=getPopulationList("USPopulation.txt")
    changeList=getPopulationChangeList(popList)
    #print(popList)
    #average
    print("The average change per year is:" + str(avg(changeList)))
    #min change
    print("The smallest change happened in " + str((changeList.index(min(changeList))+base)) + " with a population change of " + str(min(changeList)))
    #max change
    print("The largest change happened in "+ str((changeList.index(max(changeList))+base)) + " with a population change of " + str(max(changeList)))

if __name__=="__main__":
    main()
