# hotdog.py - based on the number of hotdogs and buns in a package, amount of packs are calculated
# Will Gardner 
# Cpt101 9/17/16

#number of items in pack
hotDogsInPack=10
bunsInPack=8

#get input
print("Hot dog calc.")
numberOfDogs= int(input("How many hotdogs do you want?"))

hotDogPack =  numberOfDogs//hotDogsInPack + (numberOfDogs % hotDogsInPack > 0)
bunPack =  numberOfDogs//bunsInPack + (numberOfDogs % bunsInPack > 0)

print("Number of Hotdog packs:",hotDogPack)
print("Number of Bun packs:",bunPack)
print("Remaining hotdogs in pack",(((hotDogPack*hotDogsInPack)-(numberOfDogs//hotDogsInPack)*hotDogsInPack)-(numberOfDogs % hotDogsInPack)))
print("Remaining buns in pack",(((bunPack*bunsInPack)-(numberOfDogs//bunsInPack)*bunsInPack)-(numberOfDogs % bunsInPack)))
