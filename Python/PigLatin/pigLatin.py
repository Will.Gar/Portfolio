#pigLatin.py - Converts a user inputted sentence to pig latin



def pLConvert(inpStr):
    words = inpStr.split()
    newString = ""
    for word in words:
        newString+= word[1:] + word[0] + "ay "
    return newString

def main():
    userInp=str(input("enter a string to convert to pig latin:"))
    print("Converted text:\n" + pLConvert(userInp))

if __name__=="__main__":
    main()
