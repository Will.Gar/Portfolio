#Car.py - car class for simulating carr


class Car:
    def __init__(self,make,year_model):
       self.__year_model=year_model
       self.__make=make
       self.__speed = 0
    
    def accelerate(self):
        self.__speed+=5
    def brake(self):
        self.__speed-=5
    def get_speed(self):
        return self.speed
    def __str__(self):
        return ("The " + str(self.__year_model)+ " "+self.__make+" is going " + str(self.__speed) + "MPH")

