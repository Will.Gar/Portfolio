#carMain.py - program that calls the car Class
import Car as c

def main():
    pinto = c.Car("Ford Pinto",1971)
    for i in range(5):
        pinto.accelerate()
        print(pinto)
    for i in range(5):
        pinto.brake()
        print(pinto)
if __name__=="__main__":
    main()
    
