#word_count.py - Word Count analyzer pseudo code; takes input of a file, outputs the number of words,character,sentences, and each word and its frequency within the file
#Will Gardner
#CPT101
#11/13/17
import copy as c

#getInput - get a string file using string path parameter
#input - path - string representing the path of the file
#output - string of entire file
def getInput(path):
	file=open(path)
	fileString=""
	for line in file:
		fileString += line
	return fileString
	
#charFilter - filter out a list of characters from filestring
#input fileString - string of entire file
#output - modified string
def charFilter(fileString):
	tempString = fileString;
	replaceStr="?.,/\\\n\'\"[]<>()!@#$%^&*`~{}-|"
	for i in replaceStr:
		tempString = tempString.replace(i," ")
	return tempString
	
#sentenceDelimiterFilter - change ? and ! to . in file
#input - fileString - string of entire file
#output - modified string
def sentenceDelimiterFilter(fileString):
	return fileString.replace("!",".").replace("?",".")

def getWordList(fileString):
	tempList = charFilter(fileString).split()
	for i in range(len(tempList)):
		tempList[i]=tempList[i].lower()
	return tempList
#getWordCount - gets word count
#input - string of entire file
#output - length of file
def getWordCount(fileString):
	return len(getWordList(fileString))
	
#getSentenceCount - gets a count of sentences
# input	-	fileString - string of entire file
# output -	length of array
def getSentenceCount(fileString):
	return len(sentenceDelimiterFilter(fileString).split("."))

#analyzeWords- makes a list of lists contain unique word in the file and their occurences
#input - wordlist - list of words
#output - list of list of words and frequency
def analyzeWords(wordList):
	analyzedList = []
	tempList = []
	for word in wordList:
		tempList = []
		tempList.append(word)
		initialCount = len(wordList)
		removed=True
		while(removed):
			try:
				wordList.remove(word)
			except ValueError:
				removed=False
		tempList.append((initialCount-len(word)))
		analyzedList.append(c.deepcopy(tempList))
	return analyzedList


#main
def main():
	print("Word Count v1.0")
	print ("This program will analyze all the text within a saved file")
	path = input("Please Enter the path of file to analyze:")
	if(path == ""):
		path = "nationaltreasure.txt"
	fileString = getInput(path)
	words = []
	print("File " +path+ " was read in and contains:")
	print("number of total words = " + str(getWordCount(fileString)))
	print("Number of total characters = " +str(len(fileString)))
	words = analyzeWords(getWordList(fileString))
	print("Number of unique words = " + str(len(words)))
	print("Word Occurences:")
	for elem in words:
		print("\t" + str(elem[0]) + " = " + str(elem[1]))
	print("thanks for using my program program. Goodbye")


if __name__ == "__main__":
	main()