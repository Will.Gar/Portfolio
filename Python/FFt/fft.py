
# sources: https://jakevdp.github.io/blog/2013/08//28/understanding-the-fft/
# 

import numpy as np
def DFT(x):
    """ Discrete Fourier! Slow and passionate transformation """
    

    # numpy array
    x = np.asarray(x, dtype=float)
    # len of x
    N= x.shape[0]
    #iterating range from 0 to N
    n=np.arange(N)
    #reshape as 1-D array of N
    k = n.reshape((N,1))
    #e^(-i*2pi*k*n/N)
    M = np.exp(-2j * np.pi * k *n / N)
    #print (M)
    #print (x)

    return np.dot(M,x)

def FFT(x):
    x =np.asarray(x,dtype=float)
    N= x.shape[0]
    if np.log2(N) % 1 > 0:
        raise ValueError("size of x must be a power of 2")
    #N_min is stopping condition for eq
    N_min = min(N,32)
    
    n=np.arange(N_min)
    k=n[:,None]
    M = np.exp(-2j * np.pi * n * k / N_min)
    X = np.dot(M, x.reshape((N_min,-1)))
    #print (np.dot(M,x.reshape((N_min,-1))))
    #print(M)
    #print(x)
    #print (X.shape[0])
    while X.shape[0] < N:
        #print(X.shape[0])
        X_even = X[:,:int(X.shape[1]/int(2))]
        #print (X_even)
        X_odd = X[:, int(X.shape[1]/int(2)):]
        factor = np.exp(-1j *np.pi * np.arange(X.shape[0],dtype="complex128")/X.shape[0])[:,None]
        #print(X_even)
        #print(factor)
        print ("presto")
        print(X_even+(factor * X_odd))

        X= np.vstack([X_even +factor * X_odd, X_even - factor * X_odd])
    return X.ravel()


def FFTMOD(x):
    x =np.asarray(x,dtype=complex)
    N= x.shape[0]
    if np.log2(N) % 1 > 0:
        raise ValueError("size of x must be a power of 2")
    #N_min is stopping condition for eq
    N_min = min(N,32)
    
    n=np.arange(N_min)
    k=n[:,None]
    M = np.exp(-2j * np.pi * n * k / N_min)
    X = np.dot(M, x.reshape((N_min,-1)))
    
    while X.shape[0] < N:
        #print ((X.shape[1] /2))
        X_even = X[:,:X.shape[1]//2]
        X_odd = X[:, X.shape[1] // 2:]
        factor = np.exp(-1j *np.pi * np.arange(X.shape[0])/X.shape[0])[:,None]
        X= np.vstack([X_even +factor * X_odd, X_even - factor * X_odd])
    return X.ravel()


def FFT2(x):
    #columns
    x = np.asarray(x,dtype=float)
    y=[]
    for i in range(x.shape[0]):
        y.append(FFT(x[i]))
    
    temp = []
    #Rows
    y=np.asarray(y,dtype=complex)
    
    print("+==================================FIRSTPASS============")
    print(y)
    for j in range(y.shape[1]):
        temp =[]
        for k in range(y.shape[0]):
            temp.append(y[k][j])
        temp2 = FFTMOD(temp)
        for i in range(len(temp2)):
            #if (i==0):
                #print (temp2.shape[0])
                #print (y.shape[0])
                #print (len(temp))
            
            y[i][j]= temp2[i]
            #print (y[i][j])
    return y


def floatFormatter(x):
    return "%.2f" % x


if __name__ == "__main__":
    #rando fill array
    np.set_printoptions(formatter={'float_kind':floatFormatter})
    np.set_printoptions(suppress=True)
    x = np.random.random(128)
    x = np.arange(128)
    #check closeness
    #print (np.allclose(DFT(x),np.fft.fft(x)))
    print (np.allclose(FFT(x),np.fft.fft(x)))
    #y = np.random.random((128,128))
    #print (np.allclose(FFT2(y),np.fft.fft2(y)))
    print (FFT2(np.arange(256).reshape(16,-1)))
    #j=(np.fft.fft2(y))
    #print (len(j))
    #print (j[127])

