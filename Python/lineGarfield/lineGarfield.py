#lineGarfield.py - takes in years worth of gas data and graphs it

import matplotlib.pyplot as plt

#getPopulationList- get a list of ints from a file
#input - path-string representing the path of a file
#output - list of ints
def getPopulationList(path):
    f = open(path,"r")
    listy=[]
    listy = f.readlines()
    for i in range(len(listy)):
        listy[i]=float(listy[i])
    return listy


def main():
    y_coords = getPopulationList("1994_Weekly_Gas_Averages.txt")
    x_coords = range(len(y_coords))
    
    plt.plot(x_coords,y_coords, marker='o')
    plt.title("1994 average gas price by week")
    plt.xlabel('week')
    plt.ylabel('price')
    plt.grid(True)
    plt.show();

if __name__=="__main__":
    main()

