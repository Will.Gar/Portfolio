# binaryConvert.py - pretty self numbers to bin using radix
#	Will Gardner 9/13/2017
#

num1=int(input("input number to convert to binary:"))
binStr=""
while (num1 > 0):
	binStr=str((num1%2))+binStr
	num1 /= 2

print binStr
