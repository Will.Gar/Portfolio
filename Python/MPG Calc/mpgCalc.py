import tkinter as tk

class Mpg_calc:
    def __init__(self):
        #mainwindow
        self.main_window= tk.Tk()
        
        self.gallon_frame= tk.Frame(self.main_window)
        self.mile_frame= tk.Frame(self.main_window)
        self.mpg_frame= tk.Frame(self.main_window)
        self.bottom_frame= tk.Frame(self.main_window)
        
        #gallon frame
        self.gallon_label = tk.Label(self.gallon_frame,text="Enter number of gallons used:")
        self.gallon_entry = tk.Entry(self.gallon_frame,width=20)
        self.gallon_label.pack(side='left')
        self.gallon_entry.pack(side='left')
        #mile frame
        self.mile_label = tk.Label(self.mile_frame,text="Enter number of miles travelled:")
        self.mile_entry = tk.Entry(self.mile_frame,width=20)
        self.mile_label.pack(side='left')
        self.mile_entry.pack(side='left')
       
        #mpg frame
        self.mpg_label = tk.Label(self.mpg_frame,text="MPG:")
        self.mpg = tk.StringVar()
        self.mpg.set(0)
        self.mpgVar_label= tk.Label(self.mpg_frame,textvariable=self.mpg)
        self.mpg_label.pack(side='left')
        self.mpgVar_label.pack(side='left')
        #ok/quit buttons
        self.ok_button = tk.Button(self.bottom_frame,text='CALC',command=self.calculate_MPG)
        self.quit_button = tk.Button(self.bottom_frame,text='Quit',command=self.main_window.destroy)
        self.ok_button.pack(side='left')
        self.quit_button.pack(side='left')
        
        self.mile_frame.pack()
        self.gallon_frame.pack()
        self.mpg_frame.pack()
        self.bottom_frame.pack()
        
        tk.mainloop()
    def calculate_MPG(self):
        self.mpg.set((float(self.mile_entry.get())/float(self.gallon_entry.get())))
def main():
    mpg=Mpg_calc()

if __name__=="__main__":
    main()

