#triangle.py--program that draws ascii triangles
#Will Gardner 9/17/17
#CPT 101


print("Triangle Maker")
#input getter
size = int(input("Enter the size of the triangle:"))
triangleStr="" # string to draw line of triangle
for i in range(0,size):
        for k in range(0,i+1):
            triangleStr += "#"
        print("\t",triangleStr)
        triangleStr= ""
