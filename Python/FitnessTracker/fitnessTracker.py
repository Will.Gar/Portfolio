#fitnessTracker-a program that takes each line and averages and display the pool at line representatative of a new month


#maxday return the maximum number of days base on the day number
#input     i - number representing the day
#output    value representing the number of days in a month
def maxDay(i):
    if(i>333):
        return 31
    elif(i>303):
        return 30
    elif(i>272):
        return 31
    elif(i>242):
        return 30
    elif(i>211):
        return 31
    elif(i>180):
        return 31
    elif(i>150):
        return 30
    elif(i>119):
        return 31
    elif(i>89):
        return 30
    elif(i>58):
        return 31
    elif(i>30):
        return 28
    elif(i>=0):
        return 31
#mean - return the average using a pool of items and the number of items within that pool
#input-      pool - a pool of items
#           numberOfItems - the number of items within that pool
def mean(pool,numberOfItems):
    return (pool/numberOfItems)

#month - returns month based on a number
#input-     num - numeric representation of months in a year starting at 0
#output-    three letter representation of a month   
def month(num):
    if(num == 0):
        return "Jan"
    elif(num == 1):
        return "Feb"
    elif(num == 2):
        return "Mar"
    elif(num == 3):
        return "Apr"
    elif(num == 4):
        return "May"
    elif(num == 5):
        return "Jun"
    elif(num == 6):
        return "Jul"
    elif(num == 7):
        return "Aug"
    elif(num == 8):
        return "Sep"
    elif(num == 9):
        return "Oct"
    elif(num == 10):
        return "Nov"
    elif(num == 11):
        return "Dec"

def main():
    i=0
    m=0
    path="steps.txt"
    f=open(path,"r")
    while i < 365:
        j=0;
        pool=0;
        maxD=maxDay(i)
        while j < maxD:
            pool+=float(f.readline())
            if (j == (maxDay(i)-1)):
                print("Average of " + month(m)+":"+str(mean(pool,maxDay(i))))
                m+=1; 
            i+=1
            j+=1
            
if __name__=="__main__":
   main()

