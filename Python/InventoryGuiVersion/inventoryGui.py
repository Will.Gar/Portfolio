import tkinter as tk
import tkinter.messagebox
from pathlib import Path
import Inventory as inven
#File input prompt
class fileInput:
    def __init__(self):
        #window
        self.path=""
        self.main_window= tk.Tk();
        #main frame
        self.main_frame=tk.Frame(self.main_window)
        #main frame contents
        self.file_label=tk.Label(self.main_frame,text="Enter Inventory path")
        self.file_entry=tk.Entry(self.main_frame,width=40)
        
        #button frame
        self.button_frame=tk.Frame(self.main_window)
        self.OkButton=tk.Button(self.button_frame,text='OK',command=self.checkPath)
        self.quit_button = tk.Button(self.button_frame,text='Quit',command=self.main_window.destroy)
        self.file_label.pack()
        self.file_entry.pack()
        self.OkButton.pack(side='left')
        self.quit_button.pack(side='left')

        self.main_frame.pack()
        self.button_frame.pack()

        tk.mainloop()

    def checkPath(self):
        pathname=self.file_entry.get()
        invFile = Path(pathname)
        if(invFile.is_file()):
            self.path=pathname
            self.main_window.destroy()
        else:
            tkinter.messagebox.showinfo("ERROR:STUPID PERSON","Enter Valid Path")


class textDescInput:
    def __init__(self,master,itemList,labelText):
        self.input=""
        #window
        self.main_window=master;
        #main frame
        self.itemList_frame=tk.Frame(self.main_window)
        self.main_frame=tk.Frame(self.main_window)
        #main frame contents
        self.string_label=tk.Label(self.main_frame,text=labelText)
        self.string_entry=tk.Entry(self.main_frame,width=40)
        
        #itemList frame
        self.iL_Label=tk.Label(self.itemList_frame,text=itemList)
        self.iL_Label.pack()
        #button frame
        self.button_frame=tk.Frame(self.main_window)
        self.OkButton=tk.Button(self.button_frame,text='OK',command=self.checkString)
        self.quit_button = tk.Button(self.button_frame,text='Quit',command=self.main_window.destroy)
        self.string_label.pack()
        self.string_entry.pack()
        self.OkButton.pack(side='left')
        self.quit_button.pack(side='left')
        self.itemList_frame.pack()
        self.main_frame.pack()
        self.button_frame.pack()

        tk.mainloop()
    def checkString(self): 
        string1=self.string_entry.get()
        if(string1 != ""):
            self.input=string1
            self.main_window.quit()
        else:
            tkinter.messagebox.showinfo("ERROR:STUPID PERSON","Enter Valid String")

class textInput:
    def __init__(self,master,labelText):
        self.input=""
        #window
        self.main_window=master
        #main frame
        self.main_frame=tk.Frame(self.main_window)
        #main frame contents
        self.string_label=tk.Label(self.main_frame,text=labelText)
        self.string_entry=tk.Entry(self.main_frame,width=40)
        
        #button frame
        self.button_frame=tk.Frame(self.main_window)
        self.OkButton=tk.Button(self.button_frame,text='OK',command=self.checkString)
        self.quit_button = tk.Button(self.button_frame,text='Quit',command=self.main_window.destroy)
        self.string_label.pack()
        self.string_entry.pack()
        self.OkButton.pack(side='left')
        self.quit_button.pack(side='left')

        self.main_frame.pack()
        self.button_frame.pack()

        tk.mainloop()
    def checkString(self): 
        string1=self.string_entry.get()
        if(string1 != ""):
            self.input=string1
            self.main_window.quit()
        else:
            tkinter.messagebox.showinfo("ERROR:STUPID PERSON","Enter Valid String")
        
class register:
    def __init__(self,path=""):
        #file to inventory object
        self.path=path
        self.inv=inven.Inventory()
        filey=open(self.path,'r')
        num=0
        for line in filey.readlines():
            if (num % 3 == 2):
                self.inv.add_Item(tempItem,tempQuan,float(line))
            #tempItem line1
            if (num % 3 == 1):
                tempQuan=int(line)
            if (num % 3 == 0):
                tempItem = line.replace('\n',"")
            num+=1
        filey.close()
        
        
        #window
        self.main_window= tk.Tk();
        self.main_window.title("main menu:")
        
        #framesnbuttons
        self.frameList=[]
        self.buttonList=[]
        menu =["sell one item","increase or reduce inventory","report total worth of assets","show list of all items in inventory","show list of all items currently out of stock","add new item to inventory","remove item","close and update file"]
        n=0
        for texty in menu:
            self.frameList.append(tk.Frame(self.main_window))
            self.buttonList.append(tk.Button(self.frameList[n],text=texty,command=lambda x=n:self.menuEx(x)))
            self.buttonList[n].pack()
            self.frameList[n].pack()        
            n+=1

        tk.mainloop()
            
  
    def menuEx(self,num):      
        print(num)
        if (num==0):
            #sell one item
                #display item list
                #prompt for item desc
            nwin=tk.Toplevel(self.main_window)
            tI=textDescInput(nwin,self.inv.showItems(),"Enter Item Description:")
            tempDesc=tI.input
            if (not self.inv.sellItem(tempDesc)):
                tkinter.messagebox.showinfo("Notice","ITEM SOLD OUT")
            tI.main_window.destroy()
        if (num==1):
            #increase or reduce inventory
            self.inv.showDesc()
                #prompt for item desc
            
            nwin=tk.Toplevel(self.main_window)
            tI=textDescInput(nwin,self.inv.showItems(),"Enter Item Description:")
            tempDesc=tI.input
                #prompt for quantity update
            
            nwin=tk.Toplevel(self.main_window)
            tQ=textInput(nwin,"Enter quantity:")
            tempQuan=int(tQ.input) 
            self.inv.changeQuantity(tempDesc,tempQuan)
            tQ.main_window.destroy()
            tI.main_window.destroy()
            #report total worth of assets
        if (num==2):
            tkinter.messagebox.showinfo("TOTAL ASSET VALUE: ", moneysForm(self.inv.assetValue()))
            #show list of all items in inventory
        if (num==3):
            tkinter.messagebox.showinfo("ITEM LIST:" , self.inv.showItems())
            #show list of all items currently out of stock
        if (num==4):    
            tkinter.messagebox.showinfo("OUT OF STOCK ITEMS:",self.inv.outOfStock())
        
        #add new item to inventory
        if (num == 5):
            #prompt for item desc
            nwin=tk.Toplevel(self.main_window)
            tD=textInput(nwin,"Enter Item Description:")
            tempDesc=tD.input
            #prompt for initial quantity
            nwin=tk.Toplevel(self.main_window)
            tQ=textInput(nwin,"Enter Total Quantity:")
            tempQuan=int(tQ.input)
            #prompt for price
            nwin=tk.Toplevel(self.main_window)
            tP=textInput(nwin,"Enter Item Price:")
            tempPrice=float(tP.input)
            #add to inventory
            self.inv.add_Item(tempDesc,tempQuan,tempPrice)
            tQ.main_window.destroy()
            tD.main_window.destroy()
            tP.main_window.destroy()
            #remove item 
        if (num == 6):
                #display item list
            nwin=tk.Toplevel(self.main_window)
            tI=textDescInput(nwin,self.inv.showItems(),"Enter Description of Item to remove:")
            tempDesc=tI.input
                #remove item
            self.inv.remove_Item(tempDesc)
            tI.main_window.destroy()
            #close and update file
        if (num==7):
            updateFile(self.inv,self.path)
                #call updateFile
            self.main_window.destroy()
                #set quit=true
    def descList():
        print ("bleh")

def main():
    fI=fileInput()
    path=fI.path
    reg=register(path)


def updateFile(inv,filePath):
        #open file Write
        filey=open(filePath,'w')
        #loop through Inventory size
        for i in range(inv.getSize()):
            filey.write(inv.itemAt(i).getItemDesc()+'\n')
            filey.write(str(inv.itemAt(i).getItemQuan())+'\n')
            filey.write(str(inv.itemAt(i).getItemPrice())+'\n')
            #write Invetory[i].itemDesc
            #write Invetory[i].itemQuan
            #write Invetory[i].itemPrice

def moneysForm(flt):
    return '${:,.2f}'.format(flt)

    ##IF TIME DESIGN AS GUI!
if __name__=="__main__":
    main()


