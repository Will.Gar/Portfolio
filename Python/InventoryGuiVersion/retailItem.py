##Will Gardner
##class : retailItem
class retailItem:
    #private vars
        # item description
        # item quantity
        # item price
    #contstructor
    def __init__(self,desc=0,quant=0,price=0.): 
        self.__item_desc=desc
        self.__item_quantity=quant
        self.__item_price=price
    #accessors
        #getItemDesc
    def getItemDesc(self):
        return self.__item_desc
        #getItemQuan
    def getItemQuan(self):
        return self.__item_quantity
        #getPrice
    def getItemPrice(self):
        return self.__item_price
    #mutators
        #setItemDesc
    def setItemDesc(self,desc):
        self.__item_desc=desc
        #setItemQuan
    def setItemQuan(self,quan):
        self.__item_quantity=quan
        #setPrice
    def setItemPrice(self,price):
        self.__item_price=price
    #str method
    def __str__(self):
        return (self.__item_desc + ": " + str(self.__item_quantity)+"x"+moneysForm(self.__item_price))
    #totalprice method - itemQuan x price
    def totalPrice(self):
        return (self.__item_quantity * self.__item_price)


def moneysForm(flt):
    return '${:,.2f}'.format(flt)

    ##IF TIME DESIGN AS GUI!
