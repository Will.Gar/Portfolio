##Will Gardner
import retailItem as ri
##class inventory
class Inventory:
    #private vars
        #list of retailItem
    def __init__(self,retail_Item_Lst=[]):
       self.__retail_Item_Lst=retail_Item_Lst
    #sellItem
        #passed a string description, array searched for matching description, true decrements stock by one, false and 0 stock reports error.
    def sellItem(self,desc):
        index = self.indexOf(desc)
        if (index >=0):
            if (self.__retail_Item_Lst[index].getItemQuan() > 0):
                self.__retail_Item_Lst[index].setItemQuan((self.__retail_Item_Lst[index].getItemQuan()-1))
           
            return True
        else:
            return False
    #change quantity
    #same as sell but quantity is also passed, negative sums changed to 0
    def changeQuantity(self,desc,quan):
        index = self.indexOf(desc)
        if (index >=0):
            if (self.__retail_Item_Lst[index].getItemQuan() + quan  > 0):
                self.__retail_Item_Lst[index].setItemQuan((self.__retail_Item_Lst[index].getItemQuan()+quan))
            else:
                self.__retail_Item_Lst[index].setItemQuan(0)
    #show items
        #prints all items using string method
    def showItems(self):
        text=""
        for element in self.__retail_Item_Lst:
            text += str(element)+"\n"
        
        return text
    #show description
    def showDesc(self):
        text=""
        for element in self.__retail_Item_Lst:
            text+=element.getItemDesc()+"\n"
        #prints all desc
    #out of stock 
        #prints all items with 0 stock
    def outOfStock(self):
        text =""
        for element in self.__retail_Item_Lst:
            if (element.getItemQuan() == 0):
                text= str(element) + "\n"
        return text
    #asset value
        #method will return a total cost of all inventory items
    def assetValue(self):
        total = 0
        for element in self.__retail_Item_Lst:
            total+=element.totalPrice()
        return total
    #add_item
        #makes a new retail item
    def add_Item(self,desc,quan,price):
        tempItem=ri.retailItem(desc,quan,price)
        self.__retail_Item_Lst.append(tempItem)
        self.sortArray()
    #remove_item
    def remove_Item(self,desc):
        index=self.indexOf(desc)
        if(index >= 0):
            self.__retail_Item_Lst.pop(index)
    #getSize
        #print size of list
    def getSize(self):
        return len(self.__retail_Item_Lst)
    #itemAt
        #return retailItem at index
    def itemAt(self,dex):
        return self.__retail_Item_Lst[dex]
    def sortArray(self):
        self.__retail_Item_Lst=sorted(self.__retail_Item_Lst,key=lambda x: x.getItemDesc())
    #search array
    def indexOf(self,desc):
        for index, element in enumerate(self.__retail_Item_Lst):
            if(element.getItemDesc() == desc):
                return index
        else: 
            return -1

