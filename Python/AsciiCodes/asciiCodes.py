# Takes in string and returns an array of ascii value representing each char

def asciiCodes(s1):
    arr1=[]
    for i in range(len(s1)):
        arr1 += [ord(s1[i])]
    return arr1
if __name__=="__main__":
    enter = input("Enter Some stuff:")
    arr=asciiCodes(enter)
    print(arr)
