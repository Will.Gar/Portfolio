#capitalQuiz.py - selects Five random quiz values from a list of state capitals
import random as r
def getQuiz(path):
    capFile = open(path,'r')
    fileList = capFile.readlines()
    fileList = [word.strip() for word in fileList]
    capFile.close()
    quizData={}
    for i in range(0,len(fileList),2):
        quizData[fileList[i]]=fileList[i+1]
    return quizData

def getQuestionData(quizData):
     
    return list(quizData.items())[r.randint(0,len(quizData.items())-1)]

def question(quizData):
    state,capital = getQuestionData(quizData)
    guess = input("What is the capital of " + state + "?:")
    if (guess==capital):
        return 1
    else:
        return 0


def main():
    numberOfQuestions=5
    score = 0
    path = "state_capitals.txt"
    print("THE ULTIMATE STATE CAPITAL QUIZ GAME!!!!!")
    quizData = getQuiz(path)
    for i in range(5):
        score+=question(quizData)
    print("Final Score:" + str(score) + " out of " +  str(numberOfQuestions))

if __name__=="__main__":
    main()
