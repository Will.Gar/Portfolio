from testscroller import *
import Tkinter as tk

class GUIbles:
	def __init__(self,master,listoflist,desc):
		self.tkool = master
		self.tkool.title("Copy...")
		self.Results = -1
		#Frame1:Copy List Box
		self.copySet = TestGUI(self.tkool,listoflist)
		self.copySet.pop()
		self.copySet.descList = desc
		self.frame1 = self.copySet.getFrame()
		#Frame2:Buttons
		self.frame2 = tk.Frame(self.tkool)
		self.Button1 = tk.Button(self.frame2,text="OK",command= lambda: self.oaks())
		self.Button2 = tk.Button(self.frame2,text ="apply",command= lambda:self.applyChanges())
		self.Button3 =  tk.Button(self.frame2,text ="Cancel",command= lambda:self.Cancel())
		#Packs
		self.frame1.pack()
		self.frame2.pack()
		self.Button1.pack(side=tk.LEFT)
		self.Button2.pack(side=tk.RIGHT)
		self.Button3.pack(side=tk.RIGHT)
	def oaks(self):
		self.tkool.quit()
		
	def applyChanges(self):
		self.Results = int(self.copySet.sel[0])					
	def Cancel(self):
		self.Results = -1
		self.tkool.quit()
def copyFromGUI(listsofL,descriptions):
	master = tk.Tk()
	app = GUIbles(master,listsofL,descriptions)
	master.mainloop()
	results = app.Results
	master.destroy()
	return results

if __name__ == '__main__':
	master = tk.Tk()
	list1 = [1,2,3,4,5,6,7,8]
	list2 = ['one','two','three','four','five','six','seven','eight']
	list3 = ['value of one(1)','value of two(2)','value of three(3)','value of four(4)','value of five(5)','value of six(6)','value of seven(7)','value of eight(8)']
	lOL = [list1,list2]
	app = GUIbles(master,lOL,list3)
	master.mainloop()
	print app.Results
