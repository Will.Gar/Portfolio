class ININoSects:
	def __init__(self,fileName):
		# ReadINI File to Array=======================
		self.fN = fileName
		self.linus = []
		self.temp = []
		with open(self.fN,'r') as f:
			for line in f:
				if(line[0] <> '#'):
					self.temp.append(line.rstrip())
		# =============================================
		# Split lines at '='===========================
		for index in range(len(self.temp)):
			self.temp2 = []
			self.linus.append([self.temp[index].split("=",1)[0],((self.temp[index].split("=",1)[1].rstrip('\n')))])
		#==============================================
	def printArray(self):
		for i in range(len(self.linus)):
			print self.linus[i]
	def delArray(self,delimiter):
		s1 = ''
		self.delArray = []
		for i in range(len(self.linus)):
			tempArray = []
			tempArray.append(self.linus[i][0])
			for j in range(len(self.linus[i][1].split(delimiter))):
				tempArray.append(self.linus[i][1].split(delimiter)[j])
			self.delArray.append(tempArray)
	def printDelArray(self):
		for i in range(len(self.delArray)):
			print self.delArray[i]
	def searchArray(self,value):
		for i in range(len(self.linus)):
			if (self.linus[i][0] == value):
				return i
	def searchDelArray(self,value):
		for i in range(len(self.delArray)):
			if (self.delArray[i][0] == value):
				return i

