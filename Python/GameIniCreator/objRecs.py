## ObjRecs Handles the creation of Objects records when given a map section
from ini import *
from IniNoSects import *
from objectClass import *
from scriptClass import *
class objRecs:
	def __init__(self,Section = None):		
		self.rex = []
		#OBJ File Locs=================================================================
		self.lFL = ININoSects("gameGenFiles/OBJ/ObjFileLocations.ini") #lFL stands for locFile Location		
		#grabbing information from Objects.ini=========================================
		#items: ObjectID,ObjectName,MapID,LocCode,SpriteSet,ScriptID,ObjDescID
		if (Section is None):
			Section = "test.rmp"
		self.Sect=Section
		fn = self.getLoc("fn")
		self.obj = INI(fn)
		self.obj.getSectionItems(self.Sect)
		self.itemsList1 = self.obj.getDelimedList('=')
		
		for index in range(len(self.itemsList1)):
			tempRec = obj(ID=self.itemsList1[index][0],Name=self.itemsList1[index][1][0],MapNum=self.itemsList1[index][1][1],LocCode=self.itemsList1[index][1][2],SpriteSet=self.itemsList1[index][1][3],ScriptCode=self.itemsList1[index][1][4],ObjDescID=self.itemsList1[index][1][5])
			self.rex.append(tempRec)
		self.obj = None		
		#MAP==========================================================================
		#grabbing mapName
		fnMaps= self.getLoc("fnMaps")
		self.Maps=  ININoSects(fnMaps) 
		#obj2.printArray()
		self.Maps.delArray('|')
		#obj2.printDelArray()
		for record in self.rex:
			record.MapName= self.Maps.delArray[self.Maps.searchDelArray(record.MapNum)][1]
			record.MapDescID= self.Maps.delArray[self.Maps.searchDelArray(record.MapNum)][2]
		self.Maps=None
		#Location====================================================================
		fnLoc = self.getLoc("fnLoc")
		self.Locs=  ININoSects(fnLoc) 
		self.Locs.delArray(',')
		for record in self.rex:
			record.LocList= [self.Locs.delArray[self.Locs.searchDelArray(record.LocCode)][1],self.Locs.delArray[self.Locs.searchDelArray(record.LocCode)][2]]
		self.Locs = None
		#Scripts=====================================================================
		#1.Checks ScriptSets (',' Delimiter) for scriptlinks associated with Record
		#   values in file: 
		#	-*[record.ScriptCode]ID (ScriptSetSets ID (Labelled ScriptCode) (link to Obj.ini)
		# 	-*[record.ScriptSetCode]ScriptSet Code (link to Scribbles.ini)
		#2.Checks Scribbles ('|' Delimiter) for each scriptlink 
		#  values in file:
		#        -[record.ScriptSetCode]ScriptSetID (link to ScriptSet.ini)
		#	 -*[script.ID]ScriptID (link to Scripts.ini)
		#	 -*[script.EventID]EventID (link to Events.ini)
		#        -*[script.flagID]FlagID (link to to FlagID)
		#3.For each scriptLink, checks script.ini
		#	 -[script.ID]ScriptID
		#	 -*[script.Script]Script
 		#4.For each scriptLink, checks events.ini
		#	-[script.eventID]eventID
		#	-*[script.event]event
		#5.For each scriptLink, checks flags.ini
		#	-[script.flagID]FlagID
		#	-*[script.flag]Flag
		#============================================================================
		#Script Set		
		fnScrSet=self.getLoc("fnScrSet")
		self.scrSets = ININoSects(fnScrSet)
		self.scrSets.delArray(",")
		for record in self.rex:
			tempArray = []
			
			for i in range(1,(len(self.scrSets.delArray[self.scrSets.searchDelArray(record.ScriptCode)]))):
				tempArray.append(self.scrSets.delArray[self.scrSets.searchDelArray(record.ScriptCode)][i])
			record.ScriptSetCodes = tempArray
		self.scrSets = None	
		#============================================================================
		#ScriptLink Set + ScriptClassCreation
		fnScrLink = self.getLoc("fnScrLink")
		self.ScrLink = ININoSects(fnScrLink)
		self.ScrLink.delArray("|")
		for record in self.rex:
			tempScriptSet = []
			for i in range(len(record.ScriptSetCodes)):
				tempScriptSet = self.ScrLink.delArray[self.ScrLink.searchDelArray(record.ScriptSetCodes[i])]
				script = Script(ID = tempScriptSet[1],EventID = tempScriptSet[2],FlagID = tempScriptSet[3])
				record.ScriptSets.append(script)	
		self.ScrLink = None
		#============================================================================
		#Setting Script and ScriptDescID
		fnScript = self.getLoc("fnScript")
		self.Script = ININoSects(fnScript)
		self.Script.delArray("<>?|")
		for record in self.rex:
			for script in record.ScriptSets:
				script.Script = (self.Script.delArray[self.Script.searchDelArray(script.ID)][1]).replace("!~?","\n")
				script.DescID = self.Script.delArray[self.Script.searchDelArray(script.ID)][2]
		self.Script = None
		#=============================================================================		
		#Setting Event
		fnEvent = self.getLoc("fnEvent")
		self.Event = ININoSects(fnEvent)
		for record in self.rex:
			for script in record.ScriptSets:
				script.Event = self.Event.linus[self.Event.searchArray(script.EventID)][1]
		self.Event = None		
		#=============================================================================
		#Fetching Flags
		fnFlag = self.getLoc("fnFlag")
		self.Flag = ININoSects(fnFlag)
		self.Flag.delArray("?$~")
		for record in self.rex:
			for script in record.ScriptSets:
				script.Flag = self.Flag.delArray[self.Flag.searchDelArray(script.FlagID)][1]
				script.FlagDescID = self.Flag.delArray[self.Flag.searchDelArray(script.FlagID)][2]
		self.Flag = None
		#=============================================================================
		#Setting Descriptions for Flag and Script
		fnDesc = self.getLoc("fnDesc")
		self.Desc = ININoSects(fnDesc)
		for record in self.rex:
			record.ObjDesc = self.Desc.linus[self.Desc.searchArray(record.ObjDescID)][1].replace("!~?","\n")
			record.MapDesc = self.Desc.linus[self.Desc.searchArray(record.MapDescID)][1].replace("!~?","\n")
			for script in record.ScriptSets:
				script.FlagDesc = self.Desc.linus[self.Desc.searchArray(script.FlagDescID)][1].replace("!~?","\n")
				script.Desc = self.Desc.linus[self.Desc.searchArray(script.DescID)][1].replace("!~?","\n")

			#record.printAll()
		self.Desc = None
		#=============================================================================
	def getLoc(self,fn):
		return self.lFL.linus[self.lFL.searchArray(fn)][1]

	def printRecs(self):
		for record in self.rex:
			record.printAll()
			raw_input()

	def getDex(self):
		list1 = []
		for i in range(len(self.obj.getDexes())):
			list1.append(self.obj.getDexes()[i][1])
		return list1


if __name__ == '__main__':
	ob1=objRecs()
