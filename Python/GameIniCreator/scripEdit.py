import Tkinter as tk
import copy
from IniNoSects import *
from objRecs import *
from GEditWin import *
from copyFromGui import *
class scrEdit:
	def __init__(self,master,script):
		self.tkool = master
		self.scr = script
		self.defaultScr =  copy.deepcopy(script)
		self.lFL = ININoSects("gameGenFiles/OBJ/ObjFileLocations.ini")
		
		## Frame1 Script Code
		self.frame1 = tk.Frame(self.tkool)
		self.label1 = tk.Label(self.frame1,text="Script Code:")
		self.text1 = tk.Text(self.frame1,width=10,height=1)
		self.text1.insert(tk.END,self.scr.ID)
		self.text1.config(state=tk.DISABLED)

		## Frame 2 Event
		self.frame2 = tk.Frame(self.tkool)
		self.label2 = tk.Label(self.frame2,text="EVENT:")
		
		### Parse EVENTS FROM Events.ini here
		fnEvent = self.getLoc("fnEvent")
		events = ININoSects(fnEvent)
		self.EVENTS = []
		
		for i in range(len(events.linus)):
			 self.EVENTS.append(events.linus[i][1])	
		
		##################
		self.variable = tk.StringVar(self.frame2)
		self.variable.set(self.scr.Event)
		self.pullDown1 = apply(tk.OptionMenu,(self.frame2,self.variable)+ tuple(self.EVENTS))
		#Frame3+4 Flag
		self.frame3 = tk.Frame(self.tkool)
		self.frame4 = tk.Frame(self.tkool)
		self.label3 = tk.Label(self.frame3,text="Flag:")
		self.text2 = tk.Text(self.frame4,width=45,height=5)
		self.text2.insert(tk.END,self.scr.Flag)
		self.button1 = tk.Button(self.frame3,text= "Copy...",command= lambda: self.copyFromFlags())
		self.button6 = tk.Button(self.frame3,text= "Desc...",command= lambda: self.editDesc())

		#Frame 5+6 Script
		self.frame5 = tk.Frame(self.tkool)
		self.label4 = tk.Label(self.tkool,text="Script:")
		self.frame6 = tk.Frame(self.tkool)
		self.button2 =tk.Button(self.frame5,text="Copy...",command= lambda: self.copyFromScripts())
		self.text3scrollbar = tk.Scrollbar(self.frame6)
		self.text3 = tk.Text(self.frame6,width=45,height=5,yscrollcommand=self.text3scrollbar.set)
		self.text3scrollbar.config(command=self.text3.yview)
		self.text3.insert(tk.END,self.scr.Script)

		#Frame 7+8 Description
		self.frame7 = tk.Frame(self.tkool)
		self.label5 = tk.Label(self.frame7,text="Description:")
		self.button3 = tk.Button(self.frame7,text="Copy...",command= lambda: self.copyFromDescription())
		self.frame8 = tk.Frame(self.tkool)
		self.text4scrollbar = tk.Scrollbar(self.frame8)
		self.text4 = tk.Text(self.frame8,width= 45,height=5,yscrollcommand=self.text4scrollbar.set)
		self.text4scrollbar.config(command=self.text4.yview)
		self.text4.insert(tk.END,self.scr.Desc)

		#Frame 9 Confirmation Buttons
		self.frame9 = tk.Frame(self.tkool)
		self.button4 = tk.Button(self.frame9,text="OK",command= lambda: self.oaks())
		self.button5 = tk.Button(self.frame9,text="APPLY",command=lambda:self.save())
		self.button7 = tk.Button(self.frame9,text="Cancel",command=lambda:self.cancel())
		#PACKS

		#	Frame1
		self.frame1.pack()
		self.label1.pack(side=tk.LEFT)
		self.text1.pack(side=tk.RIGHT)

		#	Frame2
		self.frame2.pack()
		self.label2.pack(side=tk.LEFT)
		self.pullDown1.pack(side=tk.RIGHT)

		#	Frame3
		self.frame3.pack()
		self.label3.pack(side=tk.LEFT)
		self.button1.pack(side=tk.RIGHT)
		self.button6.pack(side=tk.RIGHT)

		#	Frame4
		self.frame4.pack()
		self.text2.pack()

		#	Frame5 
		self.frame5.pack()
		self.label4.pack(side=tk.LEFT)
		self.button2.pack(side=tk.RIGHT)

		#	Frame6
		self.frame6.pack()
		self.text3.pack(side=tk.LEFT)
		self.text3scrollbar.pack(side=tk.RIGHT)

		#	Frame 7
		self.frame7.pack()
		self.label5.pack(side=tk.LEFT)
		self.button4.pack(side=tk.RIGHT)

		#	Frame 8
		self.frame8.pack()
		self.text4.pack(side=tk.LEFT)
		self.text4scrollbar.pack(side=tk.RIGHT)
		#	Frame 9
		self.frame9.pack()
		self.button4.pack(side=tk.LEFT)
		self.button5.pack(side=tk.RIGHT)
		self.button7.pack(side=tk.RIGHT)
	def copyFromFlags(self):
		print "COPY!!"
		
		##Generate List Of Flag IDs!!!!!
		fnFlag = self.getLoc("fnFlag")
		Flag = ININoSects(fnFlag)
		fnDesc = self.getLoc("fnDesc")
		Desc = ININoSects(fnDesc)
		Flag.delArray("?$~")
		IDList= []
		AbridgedFlagList = []
		Flag_DescList = [] 
		for i in range(len(Flag.delArray)):
			IDList.append(Flag.delArray[i][0])
			AbridgedFlagList.append(Flag.delArray[i][1][:15])
			Flag_DescList.append("Description:"+(Desc.linus[Desc.searchArray(Flag.delArray[i][2])][1]).replace("!~?","\n")+"\nFlag:" + Flag.delArray[i][1])

		#Default value
		copyFlag = [self.scr.Flag,self.scr.FlagDesc]

		#Open Copy Gui
		copyVar = copyFromGUI([IDList,AbridgedFlagList],Flag_DescList)

		#Results Check
		if (copyVar > -1):
			copyFlag = [Flag.delArray[copyVar][1],(Desc.linus[Desc.searchArray(Flag.delArray[copyVar][2])][1]).replace("!~?","\n")]
 		
		#Edit Text Box
		self.text2.delete(1.0,tk.END)
		self.text2.insert(tk.END,copyFlag[0])

		#Apply to Global vars for later use
		self.scr.Flag = copyFlag[0]
		self.scr.FlagDesc = copyFlag[1]
		
	def copyFromScripts(self):
		#Make a script list
		fnScript = self.getLoc("fnScript")
		scripts = ININoSects(fnScript)
		fnDesc = self.getLoc("fnDesc")
		Desc = ININoSects(fnDesc)
		scripts.delArray("<>?|")
		IDList= []
		AbridgedScriptList = []
		Script_DescList = [] 
		for i in range(len(scripts.delArray)):
			IDList.append(scripts.delArray[i][0])
			AbridgedScriptList.append(scripts.delArray[i][1].replace("!~?","")[:20])
			Script_DescList.append("Description:"+(Desc.linus[Desc.searchArray(scripts.delArray[i][2])][1]).replace("!~?","\n")+"\nScripts:" + scripts.delArray[i][1].replace("!~?","\n"))
		#Default Value
		copyScript = [self.scr.Script,self.scr.Desc]
		#Open Gui
		copyVar = copyFromGUI([IDList,AbridgedScriptList],Script_DescList)
		#Check results and apply values
		if (copyVar > -1):
			 copyScript= [scripts.delArray[copyVar][1].replace("!~?","\n"),(Desc.linus[Desc.searchArray(scripts.delArray[copyVar][2])][1]).replace("!~?","\n")]

		#Edit Text Box
		self.text3.delete(1.0,tk.END)
		self.text3.insert(tk.END,copyScript[0])

		#Apply to Global Vars
		self.scr.Script = copyScript[0]

		#Edit Text Box
		self.text4.delete(1.0,tk.END)
		self.text4.insert(tk.END,copyScript[1])

		#Apply to Global Vars
		self.scr.Desc = copyScript[1]

	def editDesc(self):
		self.scr.FlagDesc = GEditDesc(self.scr.FlagDesc)
	
	
	def oaks(self):
		self.tkool.destroy()

	def save(self):
		self.scr.Event = self.variable.get()
		self.scr.Script = self.text3.get("1.0",'end-1c')
		#print self.scr.Script
		self.scr.Flag = self.text2.get("1.0",'end-1c')
		self.scr.Desc = self.text4.get("1.0",'end-1c')
		#print self.scr.Desc

	def cancel(self):
		self.scr = self.defaultScr	
		self.tkool.destroy()
	

	def getLoc(self,fn):
		return self.lFL.linus[self.lFL.searchArray(fn)][1]
		
		
	
#------------------------------------------------------------------
if __name__ == '__main__':
	master = tk.Tk()
	records = objRecs("test.rmp")
	record = records.rex[0]
	script = record.ScriptSets[0]
	app = scrEdit(master,script)
	master.mainloop()
	app.scr.printAll()
