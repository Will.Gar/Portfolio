class Script:
	def __init__(self,ID = None,Script = None,EventID = None,Event = None, DescID = None,Desc = None, FlagID = None, Flag = None,FlagDescID = None, FlagDesc = None):
		#Default Values====================================================		
		if (ID is None):
			ID = 0
		if (Script is None):
			Script = "Print(\"Hello World\")"
		if (EventID is None):
			EventID = 0
		if (Event is None):
			Event = ""
		if (DescID is None):
			DescID = 0
		if (Desc is None):
			Desc = "This is a placeholder for an Actual Description"
		if (FlagID is None):
			FlagID = 0
		if (Flag is None):
			Flag = "1 = 1"
		if (FlagDescID is None):
			FlagDescId = 0
		if (FlagDesc is None):
			FlagDesc = "This is a placeholder for an Actual Description"
		#Class Public Values==============================================
		self.ID = ID
		self.Script = Script
		self.EventID = EventID
		self.Event = Event
		self.DescID = DescID
		self.Desc = Desc
		self.FlagID = FlagID
		self.Flag = Flag
		self.FlagDescID = FlagDescID
		self.FlagDesc = FlagDesc 
	def getAll(self):
		temp = [self.ID,self.Script,self.EventID,self.Event,self.DescID,self.Desc,self.FlagID,self.Flag,self.FlagDescID,self.FlagDesc]
		return temp
	def printAll(self):
		print "ID: %s\nScript: %s\nEventID: %s\nEvent: %s\nDescID: %s\nDescription: %s\nFlagID: %s\nFlag: %s\nFlag Description ID: %s\nFlag Description: %s" % (self.ID,self.Script,self.EventID,self.Event,self.DescID,self.Desc,self.FlagID,self.Flag,self.FlagDescID,self.FlagDesc)


