import Tkinter as tk

##Example of Multiple listbox yscroll from here a dynamic aproach is necessary

class TestGUI:
	def __init__(self,master,listoflist):
		self.master=master
		self.lol = listoflist
		self.frame1 = tk.Frame(self.master)
				
##the shared scrollbar#####################################################################################
		self.scrollbar = tk.Scrollbar(self.frame1, orient='vertical')
		self.lbs = []
		#note that yscrollcommand is set to a custom method for each list
		for i in range(len(self.lol)):
			#print i
			if (i == 0):		
				self.list1 = tk.Listbox(self.frame1)
				self.lbs.append(self.list1)
				filler='y'
			else:
				self.list2 = tk.Listbox(self.frame1)	
				self.lbs.append(self.list2)
				filler='both'
			#self.lbs[i].config(yscrollcommand=self.yscroll1())				
			self.lbs[i].pack(fill=filler, side='left')
			self.lbs[i].config(yscrollcommand=self.scrollbar.set)	
		

		self.sB = tk.Scrollbar(self.frame1)
		self.textB = tk.Text(self.frame1,height = 5,width = 50)
		self.scrollbar.config(command=self.onVsb)
		self.scrollbar.pack(side='right',fill='y')
		self.descList = []
		for i in range(len(self.lbs)):
			self.lbs[i].bind('<<ListboxSelect>>',lambda e,uno=i: self.immediately(uno))
		self.sel = 0
		#self.frame1.pack()
##fill the listboxes with stuff#####################################################################################
	def testPop(self):
		for i in range(len(self.lbs)):
			for x in xrange(30):
				self.lbs[i].insert('end',x)
				
		
			print i
	def pop(self):
		for i in range(len(self.lbs)):
			print ("lbs",i)
			for x in range(len(self.lol[i])):
				print ("lol",x)
				self.lbs[i].insert('end',self.lol[i][x])
				self.lbs[i].pack()		
##Cursor Matching#####################################################################################
	def immediately(self,uno,*args):
		self.sel = self.lbs[uno].curselection()
		self.textB.destroy()
		self.sB.destroy()
			
		self.sB = tk.Scrollbar(self.frame1)
		self.textB = tk.Text(self.frame1,height = 5,width = 50)
		self.sB.config(command = self.textB.yview)
		self.textB.insert(tk.END,self.descList[int(self.lbs[uno].curselection()[0])])
		self.textB.config(yscrollcommand = self.sB.set,state=tk.DISABLED)
		print(self.lbs[uno].curselection())
		self.textB.pack(side='right',fill='y')		
		self.sB.pack(side='right',fill='y')
		
		tempUno = uno
		for i in range(len(self.lbs)):
			if i != uno:		
				self.lbs[i].selection_set(self.lbs[tempUno].curselection())
				tempUno = i

		#self.lbs[uno].selection_set(self.lbs[tempUno].curselection())
##Scroller Matching#####################################################################################
	def onVsb(self, *args):
		for lb in self.lbs:
			lb.yview(*args)
	def getFrame(self):
		return self.frame1
##Main#####################################################################################
if __name__ == '__main__':
	tkool = tk.Tk()
	listb=[]
	listb.append([1,2,4,5,67,8,9,2,3,4,5,5,6,7])
	listb.append([1,2,3,4,5,6,7,8,9,10,11,12,13,14])	
	listb.append([1,2,3,4,5,6,7,8,9,0,1,2,3,4])
	app = TestGUI(tkool,listb)
	app.pop()
	frame2 = app.getFrame()
	frame2.pack()	
	tkool.mainloop()
