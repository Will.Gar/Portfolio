import csv
class INI:
	def __init__(self,fileName):
		self.fN = fileName
		self.linus = []
		self.dexes = []
		temp = []
		with open(self.fN,'r') as f:
			for line in f:
				self.linus.append(line.rstrip())		
		for index in range(len(self.linus)):
			if ((self.linus[index][0])=='#'):
				temp = [index,self.linus[index][1:]]
				self.dexes.append(temp)
		self.linus = []

	def getSectionItems(self,section):
		self.linus = []
		temp = -1
		start = 0
		end = 0
		#print (self.dexes)
		for index in range(len(self.dexes)):
			if(self.dexes[index][1] == section):
				start = (self.dexes[index][0])
				end = self.getEnd(start)
				#print ("boop")
		count = 0
		#print start,end
		with open(self.fN,'r') as f:
			for line in f:
				if ((count > start)and(count < end)):
					self.linus.append(line)
					#print (line)
				count = count+1
		return self.linus
	

		
	def getEnd(self,start):
		end = self.endLength()
		#print ("boop")
		for index in range(len(self.dexes)):
			 if((self.dexes[index][0]) == start):
				if(index <> len(self.dexes)-1):
					end = (self.dexes[index+1][0])
		return end

	def endLength(self):
		count= 0
		with open(self.fN,'r') as f:
			for line in f:
				count += 1 
		return count

	def getDexes(self):
		return self.dexes

	def clearList(self):
		self.linus = []
	
	def getDelimedList(self,delimiter1):
		delimmed = []
		for index in range(len(self.linus)):
			temp = []
			s1 = ((self.linus[index].split(delimiter1,1)[1].rstrip('\n')))
			for line in csv.reader([s1],skipinitialspace=True):
				temp = line
			delimmed.append([self.linus[index].split(delimiter1,1)[0],temp])
		return delimmed
