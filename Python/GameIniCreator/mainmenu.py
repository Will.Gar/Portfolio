import Tkinter as tk
from ini import *						

class Menu1:
	def __init__(self,master):
		self.master=master
		fn = "gameGenFiles/opts.ini"
		obj = INI(fn)
		self.val1 = None
		self.list1 =obj.getSectionItems("MainMenu")
		self.list1 =obj.getDelimedList('=')
		
		
		self.frame= tk.Frame(self.master)
		self.b1 = tk.Button(self.frame, text=self.list1[0][1][0], command=self.callOBJ, height=self.list1[0][1][1], width = self.list1[0][1][2])
		self.b2 = tk.Button(self.frame, text=self.list1[1][1][0], command=self.callENE, height=self.list1[1][1][1], width = self.list1[1][1][2])
		self.b3 = tk.Button(self.frame, text=self.list1[2][1][0], command=self.callITEM, height=self.list1[2][1][1], width = self.list1[2][1][2])
		self.b4 = tk.Button(self.frame, text=self.list1[3][1][0], command=self.callQuit, height=self.list1[3][1][1], width = self.list1[3][1][2])

		self.b1.pack()	
		self.b2.pack()
		self.b3.pack()
		self.b4.pack()
		self.frame.pack()
	def callOBJ(self):
		print ("OBJECTION!")
		self.val1="quigs"
		self.master.destroy()		
	def callENE(self):
		print ("Enemy Creation!")
		self.val1="Enemy"
		self.master.destroy()
	def callITEM(self):
		self.val1="Item"
		self.master.destroy()
		print ("Item Creation!")
	def callQuit(self):
		self.val1="quit"
		self.master.destroy()
		print ("Bye!")
def run():
	root = tk.Tk()	
	app = Menu1(root)	
	root.mainloop()
	return app.val1
def main():
	print "game INI Generator program!"
	raw_input()
	root = tk.Tk()	
	app = Menu1(root)	
	root.mainloop()
	print (root.val1)
if __name__ == '__main__':
	main()


