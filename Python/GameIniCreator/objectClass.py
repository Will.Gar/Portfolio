class obj:
	def __init__(self,ID = None,Name = None, MapNum = None, MapName = None,LocCode = None, LocList = None, SpriteSet = None,ScriptCode = None,ScriptList = None,ObjDescID = None,ObjDesc = None,MapDesc = None,MapDescID = None,ScriptSetCodes = None,ScriptSets = None):
		if (ID is None):
			ID = '000000'
		if (Name is None):
			Name = 'unknown'
		if (MapNum is None):
			MapNum = '0'
		if (MapName is None):
			MapName = 'Test'
		if (MapDesc is None):
			MapDesc = "This is a place holder for an Actual Description"
		if (MapDescID is None):
			MapDescID = "0"
		if (LocCode is None):
			LocCode = '00'
		if (LocList is None):
			LocList = [0,0]
		if (SpriteSet is None):
			Spriteset = 'test.rss'
		if (ScriptList is None) :
			ScriptList = []
		if (ScriptCode is None):
			ScriptCode = '0'
		if (ObjDesc is None):
			ObjDesc = "This is a place holder for an Actual Description"
		if (ObjDescID is None):	
			ObjDesc= 0
		if (ScriptSetCodes is None):
			ScriptSetCodes = []
		if (ScriptSets is None):
			ScriptSets = []
		self.ID = ID
		self.Name = Name
		self.MapNum = MapNum
		self.MapName = MapName
		self.LocCode = LocCode
		self.LocList = LocList
		self.SpriteSet = SpriteSet
		self.ScriptList = ScriptList
		self.ScriptCode = ScriptCode
		self.ObjDesc = ObjDesc
		self.ObjDescID = ObjDescID
		self.MapDescID = MapDescID
		self.MapDesc = MapDesc
		self.ScriptSetCodes = ScriptSetCodes
		self.ScriptSets = ScriptSets
	def getAll(self):
		list1 = [self.ID,self.Name,self.MapNum,self.MapName,self.LocCode,self.LocList,self.SpriteSet,self.ScriptCode,self.ObjDescID,self.ObjDesc,self.ScriptList,self.MapDescID,self.MapDesc,self.ScriptSetCodes]
		return list1
	def printAll(self):
		listy = self.getAll()
		print "---%s---\nName: %s \nMap number: %s\n MapName: %s\n Location Code: %s\nCoordinates: %s\nSpriteSet: %s\nScript Code: %s\nObject Description ID: %s\n Object Desription: %s\nScript List %s\nMap Number %s\nMapDescription: %s\nScript Set Code:%s" % (listy[0],listy[1],listy[2],listy[3],listy[4],listy[5],listy[6],listy[7],listy[8],listy[9],listy[10],listy[11],listy[12],listy[13])
		print '-Record Scripts-'
		raw_input()		
		for script in self.ScriptSets:
			script.printAll()
