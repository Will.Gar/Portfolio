import Tkinter as tk
from ini import *
from testscroller import *
from objRecs import *
class Object:
	def __init__(self,fileList,master):
		#ideally list of files
		fn = fileList[0]
		
		#master frame
		self.tkool = master	
		
		#creates item List from ini filename
		self.obj = INI(fn)
		self.list1 = []		
		self.list1 = self.getDex()
		#print self.list1
		self.obiRex = objRecs(self.list1[0])
		self.sectList = self.getDispList()
		#print self.sectList
		#self.obj.getSectionItems(self.list1[0])
		ObjIni = TestGUI(self.tkool,self.sortDispList(self.sectList))
		ObjIni.pop()
		self.frame2 = ObjIni.getFrame()
		ObjIni.descList = self.getDescList()
		#pull down frame 
		self.frame1= tk.Frame(self.tkool)
		#buttons frame
		self.frame3= tk.Frame(self.tkool)
		
		#buttons
		#button 1: add item 
		self.button1 = tk.Button(self.frame3,text= "Add", command = self.buttonBack)
		#button 2: edit selected item
		self.button2 = tk.Button(self.frame3,text= "Edit", command = self.buttonBack)
		
		#pull down menu [List of maps]
		self.variable = tk.StringVar(self.tkool)
		self.variable.set(self.list1[0])
		self.variable.trace('w',self.callb)
		w = apply(tk.OptionMenu, (self.frame1,self.variable) + tuple(self.list1))
		

		#Packs for items
		w.pack( side = "top" )
		self.frame1.pack()
		self.frame2.pack()
		self.frame3.pack()
		self.button1.pack(fill = 'y', side = 'left')
		self.button2.pack(fill = 'both', side = 'left')
##==================================================================================================
# callb - repopulates lists if pulldown menu changes
	def callb(self,*args):
		print self.variable.get()
		self.obiRex = objRecs(self.variable.get())
		ObjIni = TestGUI(self.tkool,self.sortDispList(self.sectList))
		ObjIni.pop()
		self.frame2.destroy()
		self.frame3.destroy()
		self.frame2 = ObjIni.getFrame()
		self.frame3= tk.Frame(self.tkool)
		self.frame2.pack()
		self.frame3.pack()
		self.button1 = tk.Button(self.frame3,text= "Add", command = self.buttonBack)
		#button 2: edit selected item
		self.button2 = tk.Button(self.frame3,text= "Edit", command = self.buttonBack)
		self.button1.pack(fill = 'y', side = 'left')
		self.button2.pack(fill = 'both', side = 'left')
##==================================================================================================
	def buttonBack(self):
		print "click!"
##==================================================================================================

	def getDispList(self):
		listy = []
		for record in self.obiRex.rex:
			temp = [record.ID,record.Name,record.LocList,record.SpriteSet]
			listy.append(temp)
		return listy
	def getDescList(self):
		listy = []
		for record in self.obiRex.rex:
			listy.append(record.ObjDesc)
		return listy
	def getDex(self):
		list1 = []
		for i in range(len(self.obj.getDexes())):
			list1.append(self.obj.getDexes()[i][1])
		return list1
##==================================================================================================
	def sortIni(self,list1):	
		tempList=[]
		rList=[]
		for i in range(len(list1)):
			tempList.append(list1[i][0])
		rList.append(tempList)
		tempList = []
		for j in range(len(list1[0][1])):
			for i in range(len(list1)):
				tempList.append(list1[i][1][j])		
			rList.append(tempList)
			tempList = []
		return rList
	def sortDispList(self,list1):
		tempList=[]
		rList=[]
		for i in range(len(list1[0])):
			for j in range(len(list1)):
				tempList.append(list1[j][i])
			rList.append(tempList)
			tempList = []
		return rList
##==================================================================================================
if __name__ == '__main__':
	root = tk.Tk()
	fileList=['gameGenFiles/OBJ/Object.ini']
	ob1=Object(fileList,root)
	root.mainloop()
	
