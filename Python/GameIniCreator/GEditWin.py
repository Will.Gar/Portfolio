import Tkinter as tk


class editDesc:
	def __init__(self,masterG,script):
		self.tkool = masterG
		self.tkool.title("Edit Flag Description")
		#frame1: DescriptionFrame
		self.frame1= tk.Frame(self.tkool)
		self.label1= tk.Label(self.frame1,text="Description:")
		self.ObjName = tk.StringVar()
		self.text1scrollbar = tk.Scrollbar(self.frame1)
		self.entry1 = tk.Text(self.frame1, bd =5,width=35,height= 5,yscrollcommand=self.text1scrollbar.set)
		self.entry1.insert(tk.END,script)
		self.text1scrollbar.config(command=self.entry1.yview)
		self.Results = script
		#frame2:Confirmation Buttons
		self.frame2 = tk.Frame(self.tkool)
		self.button1 = tk.Button(self.frame2,text="OK",command= lambda: self.oaks())
		self.button2 = tk.Button(self.frame2,text="Apply",command= lambda: self.applyChanges())

		#Packs
		#self.tkool.pack()
		#Frame1
		self.frame1.pack()
		self.label1.pack()
		self.entry1.pack(side=tk.LEFT)
		self.text1scrollbar.pack(fill='y',side=tk.RIGHT)
		#frame2
		self.frame2.pack()
		self.button1.pack(side=tk.RIGHT)
		self.button2.pack(side=tk.LEFT)
	def oaks(self):
		#print self.Results
		self.tkool.quit()
	def applyChanges(self):
		self.Results = self.entry1.get("1.0",'end-1c')
			
def GEditDesc(script):
	masterG = tk.Tk()
	editWindow =  editDesc(masterG,script)
	masterG.mainloop()
	rezis = editWindow.Results
	masterG.destroy()
	return rezis


if __name__ == '__main__':
	master = tk.Tk()
	script = "Holy Moly, this is a silly description of something.\n You know, like, when you let \n yer mind ramble. \n And the world blows up. \n some how monkeys survive. \n Humans are dead. \n Luckily, the monkeys were too busy singing to put anyone down."
	editWindow =  editDesc(master,script)
	master.mainloop()
	print editWindow.Results
