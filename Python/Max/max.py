#max.py - contains two functions: max and take_two. The first compares two ints and returns greater int while the latter function
# prompts for input
# Will Gardner
# 10/2/2017


def take_two():
	a=int(input("Enter value 1:"))
	b=int(input("Enter value 2:"))
	return a,b

def max(a,b):
	return a if (a > b) else b
	

	
if __name__ == "__main__":
	int1,int2 = take_two()
	print("Greater value:" + str(max(int1,int2)))

