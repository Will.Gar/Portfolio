#twentyNumbers - asks for twenty numbers and adds them to an array
#Will Gardner
#CPT 101
#10/30/2017


#inputList -- creates a list of user input at a specified amount
#input- num-size of list
#output-    list of input
def inputList(num):
    listy = []
    for i in range(num):
        temp = int(input("enter number" +str(i) + ":"))
        listy.append(temp)
    return listy
#avg -- gets the average of values within a list
#input- listy- list of ints
#output- double value representing average
def avg(listy):
    pool=0.
    for num in listy:
        pool+=num
    return (pool/len(listy))


def main():
    print("Twenty numbers game!")
    #input
    listy=inputList(20)
    #lowest
    print("Lowest number in list:"+str(min(listy)))
    #highest
    print("Highest number in list:"+str(max(listy)))
    #total number
    print("Total number of elements in the list:"+str(len(listy)))
    #average
    print("Average of numbers in list:"+str(avg(listy)))

if __name__=="__main__":
    main()
