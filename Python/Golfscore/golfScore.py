#golfScore.py - write the golf score of a player to a file
#Will Gardner
#10/16/17
import json as j
# writeToFile write input data to file
# input-    path-path of file to write to
#           name-string representing player name
#           score-integer representing score
def writeToFile(path,name,score):
    w = open(path,'a')
    golfScore = {'player_name':name,'score':score}
    j.dump(golfScore,w)
    w.write("\n")

# getVarIn - gets name and int inputs with try block on int
# output -      name - string representing name
#               score - integer representing score
def getVarIn():
    name = str(input("Enter Player Name:"))
    intFlag=True
    while intFlag:
        try:
            score = int(input("Enter Player score:"))
            intFlag=False
        except ValueError:
            print ("please Enter an integer.")
    return name,score

#menu - gets input using menu array object
#input -    array - string array of menu elements
#output -   choice - integer value representing user choice
def menu(array):
    for i in range(len(array)):
        print(str(i)+"."+array[i])
    choice = int(input("Enter number of option you would like:"))
    return choice

#dispAllScores - displays all scores within a file
# input -   path- path of a file
def dispAllScores(path):
    f=open(path)
    i=0
    for line in f:
        jL = j.loads(str(line))
        print("Player "+str(i))
        dispScoreElement(jL)
        i+=1
#dispScoreElement - displays score within a file
#input -    jsonObj - display parts of the json object
def dispScoreElement(jsonObj):
    print("Player Name:" +jsonObj['player_name'])
    print("Player Score:" + str(jsonObj['score']))

def main():
    print("Golf Score Keeper")
    path = "golfScore.txt"
    quit = False
    menuArr=["Add a score","Print Current names","Quit"]
    while not quit:
            choice = menu(menuArr)
            if (choice == 0):
                writeFlag=True
                while writeFlag:
                    name,score = getVarIn()
                    writeToFile(path,name,score)
                    again= str(input("would you like to write another name? (y or  n)"))
                    if (again=='n'):
                        writeFlag=False
            if (choice == 1):
                dispAllScores(path)
            if (choice == 2):
                quit = True
    print("Thanks for playin'")

if __name__ == "__main__":
    main()

