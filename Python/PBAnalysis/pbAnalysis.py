#pBNumbers.py - search for the ten most common number sequences throughout a list of powerball numbers
#Copyright <2017> <Will Gardner>

#Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import copy
import json as j

# writeToFile write input data to file
# input-    path-path of file to write to
#           name-string representing player name
#           score-integer representing score
def writeToFile(path,frequency,sequence):
    w = open(path,'a')
    sequenceStat = {'frequency':frequency,'sequence':sequence}
    j.dump(sequenceStat,w)
    w.write("\n")
    


#creates a matrix of powerball scores
#path - path of file to read from
#returns list of a sequence of numbers
def createList(path):
    retList=[]
    f=open(path)
    rawList = f.readlines()
    #each line of the raw list
    for line in rawList:
        tempElem=[]
        tempList=line.split()
        #convert each elem to to int
        for elem in tempList:
            tempElem.append(int(elem))
        retList.append(tempElem)
    return retList


#frequencyMatrix- create a list with one colomn with index+1 and 2nd zero filled up to x amount of rows
def frequencyMatrix(x):
    retList=[]
    for i in range(1,x+1):
        retList.append([i,0])
    return retList

#counts frequency of each possible number in sequence matrix
def freqAnalysis(sequenceMatrix,freqMat,place):
    for elemB in sequenceMatrix:
        for i in range(len(freqMat)):
            if freqMat[i][0]==elemB[place]:
                freqMat[i][1]+=1
                i=len(freqMat)
    return freqMat
def freqMAnalysis(sequenceMatrix,freqMat,place):
    i=0# iterator of sequence matrix
    n=0# iterator freqmat
    #print (len(freqMat))
    tempList=[]
    while(i < len(sequenceMatrix)):
        #print (n)
        matchFlag=True
        tempElem=[]
        tempList.append(sequenceMatrix[i]);
        ##print (freqMat[n]) 
        for k in range(place):
            tempElem.append(freqMat[n][k])
        if (i < len(sequenceMatrix)-1):
            for k in range(place): 
                if (matchFlag):
                    matchFlag = (tempElem[k]==sequenceMatrix[i+1][k]);
                    #print (tempElem[k])
                    #print (sequenceMatrix[i+1][k])
                    #print (tempElem[k]==sequenceMatrix[i+1][k]);
                    #print (freqMat[n])
        if (i == (len(sequenceMatrix)-1)or(not matchFlag)):
            freqMat[n][place]=copy.deepcopy(freqAnalysis(tempList,freqMat[n][place],place))
            n+=1
            tempList=[]
        i+=1
    return freqMat
        
#detirmines top 10ish frequent number used at a particular sequence and snips the rest
def freq0MaxSnip(freqMat,maxnum):
    sortedFrequency=sorted(freqMat,key=lambda x: x[1],reverse=True)
    for i in range(maxNum):
        snippedList.append(sortedFrequency[i])
    n=maxNum
    while (snippedList[-1][1]==sortedFrequency[n][1]):
        snippedList.append(sortedFrequency[n])
        i+=1
    return snippedList
#remove all 0 counts
def freq0Snip(freqMat):
    snippedList=[]
    sortedFrequency=sorted(freqMat,key=lambda x: x[1],reverse=True)
    i=0
    #print (sortedFrequency[i])
    while (sortedFrequency[i][1]>0):
        snippedList.append(sortedFrequency[i])
        i+=1
    return snippedList


#remove all 0 counts
def freqSnips(freqMat,optSnipAmt):
    snippedList=[]
    sortedFrequency=sorted(freqMat,key=lambda x: x[1],reverse=True)
    i=0
    while (sortedFrequency[i][1]>optSnipAmt):
        snippedList.append(sortedFrequency[i])
        i+=1
    return snippedList

#multi zero snipper
def mFreq0Snip(freqMat,place):
    sortedFrequency=sorted(freqMat,key=lambda x:x[place],reverse=True)
    i=0#outer iterator
    for elem in sortedFrequency[:]:
        k=0
        for elem2 in copy.deepcopy(elem[place]):
            deleteFlag=False
            #print (elem2)
            #print ("i="+str(i))
            if (elem2[1]==0):
               #print("delete" + str(i)+str(sortedFrequency[i][place][k]))
                sortedFrequency[i][place].pop(k)
                deleteFlag=True
            if (not deleteFlag):
                k+=1
                
        i+=1
    return sortedFrequency
#recursive frequency sort
def frequencySort(freqMat,place):
    sortedList=freqMat
    for i in range((place-1),-1,-1):
        print (str(i)+"freq")
        sortedList=sorted(sortedList,key=lambda x:x[i])
    return sortedList
    
#optimize multi array


#arrModification - sort and snip non essentials and collate like amount
#def arrModification(rawList,frequency,place,optSnipAmt):
 #   sortedList=sortLoop(rawList,place)
    

def opt0Snip(rawList,frequency,optSnipAmt):
    #create array of item in place that fall under the frequency thresh
    snippedFrequency=[]
    for elem in frequency:
        if (elem[1]<=optSnipAmt):
            snippedFrequency.append(elem)
    snippedFrequency=sorted(snippedFrequency,key=lambda x:x[0])
    #loop through list
    i = 0
    for elem in rawList[:]:
        for k in range(len(snippedFrequency)):
            if elem[0]==snippedFrequency[k][0]:
                rawList.pop(i)
                i-=1
                k=len(snippedFrequency)
        i+=1;
    return rawList
def genFreqMat(grandFreqMat,freqMat,place):
    tempFreqMat=copy.deepcopy(freqMat)
    freqMat=[]
    tempFreqEl=[]
    if (place==1):
        for i in range(len(grandFreqMat)):
            printFreqMat([grandFreqMat[i][0],copy.deepcopy(tempFreqMat)]) 
            freqMat.append([grandFreqMat[i][0],copy.deepcopy(tempFreqMat)])
    else:
        for i in range(len(grandFreqMat)):
            
            tempFreqEl=[]
            for k in range(place-1):
                tempFreqEl.append(grandFreqMat[i][k])
            tempFreqEl.append(0)
            tempFreqEl.append(tempFreqMat[0:])
            for k in range(len(grandFreqMat[i][place-1])):
                    tempFreqEl[place-1]=grandFreqMat[i][place-1][k][0]
                    freqMat.append(copy.deepcopy(tempFreqEl[0:]))
    return freqMat;

def finFreqMat(grandFreqMat,place):
    freqMat=[]
    tempFreqEl=[]
    for i in range(len(grandFreqMat)):
        tempFreqEl=[]
        for k in range(place-1):
            tempFreqEl.append(grandFreqMat[i][k])
        
        tempFreqEl.append(0)
        tempFreqEl.append(0)
        for k in range(len(grandFreqMat[i][place-1])):
                tempFreqEl[place-1]=grandFreqMat[i][place-1][k][0]
                tempFreqEl[place]=grandFreqMat[i][place-1][k][1]
                freqMat.append(tempFreqEl[:])
    return freqMat;



def optM0Snip(rawList,frequency,place,optSnipAmt):
    #create array of item in place that fall under the frequency thresh
    snippedFrequency=[]
    
    i=0#iterator  for frequency
    for elem in frequency: 
        k=0 #inner array iterator
        for elem2 in  copy.deepcopy(elem[place]):#iterate through inner array
            tempelem=[]
            if (elem2[1]<=optSnipAmt):
                for j in range(len(elem)-1):
                    tempelem.append(elem[j])
                tempelem.append(elem2[0])
                snippedFrequency.append(tempelem)
                frequency[i][place].pop(k)
                k-=1
            k+=1
        i+=1
    snippedFrequency=sortLoop(snippedFrequency,place)
    #loop through list
    i = 0
    #print (snippedFrequency)
    
    for elem in rawList[:]:
        match=True
        for k in range(len(snippedFrequency)):
            match=True
            for j in range(len(snippedFrequency[k])):
                if ((match)and(elem[j]!=snippedFrequency[k][j])):
                    match=False
            if (match):
                rawList.pop(i)
                i-=1
                j=len(snippedFrequency[k])
                k=len(snippedFrequency)
        i+=1;
        
    return rawList,frequency

def print0FreqMat(freqMat):
    for i in freqMat:
        print(i)
    

#sortLoop-recursive sort loop Based on collation place
def sortLoop(rawList,place):
    sortedList=rawList
    for i in range((place),-1,-1):
        print(str(i)+"here")
        sortedList=sorted(sortedList,key=lambda x:x[i])
    return sortedList
def printFreqMat(freqMat):
    for elem in freqMat:
        print(elem)
def main():
    optSnipAmt=0 #problem implies number sequences reoccur, program can be optimized by removing group amounts less than this number
    #import list
    path="pbnumbers.txt"
    rawList=createList(path)
    grandFreqMat=[]
    #loop through sequence number
    for i in range(len(rawList[0])):
        #check if last number
        if (i == len(rawList[0])-1):
                #if yes
                #genfreq=26
                freqMat=frequencyMatrix(26)
            #if not
        else:
                #genFreq=69
                freqMat=frequencyMatrix(69)
        #first loop
        if (i==0):
            sortedList=sorted(rawList,key=lambda x:x[0]) #sorted List
            freqMat=freqAnalysis(sortedList,freqMat,i) #check frequency in sequence matrix
            #print0FreqMat(freqMat)
            freqMat=freq0Snip(freqMat)  #snip frequency matrix
            print0FreqMat(freqMat)
            sortedList=opt0Snip(sortedList,freqMat,optSnipAmt) #sort and snip non essentials and collate like amount
            freqMat=frequencyMatrix(69)
            freqMat=freqAnalysis(sortedList,freqMat,i)#wastefully update frequency
            freqMat=freq0Snip(freqMat)#zero snip update
        #second through fifth loop
        else:    
            sortedList=sortLoop(sortedList,i) #recursive sort list
            #print (len(freqMat))
            freqMat=genFreqMat(grandFreqMat,freqMat,i) #loop through previous places and generate temp list for each alt grouping
            #printFreqMat(freqMat)
            freqMat=frequencySort(freqMat,i) #sortFrequency
            printFreqMat(freqMat)
            freqMat=freqMAnalysis(sortedList,freqMat,i) #generate frequency matrix count for each element       
            #freqMat[0][1][1][1]=460
            #print("BEFORE")
            #printFreqMat(freqMat)
            #printFreqMat(sortedList)
            #printFreqMat(freqMat)
            freqMat=mFreq0Snip(freqMat,i) #snip 0 frequencey for each element 
            #print("AFTER")
            printFreqMat(freqMat)
            #print(len(freqMat))
            sortedList,freqMat=optM0Snip(sortedList,freqMat,i,optSnipAmt)#optimize list and update freqmat   
        grandFreqMat=copy.deepcopy(freqMat);
        print (len(grandFreqMat))
        input("pass: "+ str(i))
    freqMat=[]
    sortedList=[]
    input("finalrun!")
    grandFreqMat=finFreqMat(grandFreqMat,len(rawList[0]))
    grandFreqMat=sorted(grandFreqMat,key=lambda x:x[-1],reverse=True)[0:9]
    for elem in grandFreqMat:
        print (elem)
if __name__=="__main__":
    main()


