#optimusPrime.py -- program that checks if a number is prime or not.
#Will Gardner
#10/2/17
#CPT 101

def isPrime(number):
    if(number > 2):
        for i in range (2,(number // 2)+1):
            if (number // i) == (float(number) / float(i)):
                return False
    return True

if __name__ == "__main__":
    num = int(input("Enter a number:"))
    if isPrime(num):
        string ="prime" 
    else:
        string ="not prime"
    print(string)
