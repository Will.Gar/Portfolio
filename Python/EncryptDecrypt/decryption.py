#decryption.py-decrypt encrypted file
import encryption as crypt

def decrypt(line):
    retLine = line
    cipher = crypt.getCipher()
    for ciphItem in list(cipher.items()):
        retLine = retLine.replace(ciphItem[1],ciphItem[0])
    return retLine

def main():
    print("decryption Program")
    ogPath="encrypted.txt"
    newFilePath="decrypted.txt"
    fileLines = crypt.getLines(ogPath)
    newFile=open(newFilePath,'w+')
    for line in fileLines:
        newFile.write(decrypt(line)+"\n")
    print("File " + newFilePath + " decrypted and created.")
if __name__ == "__main__":
    main()
