#encryption.py - takes a specified file and uses a cipher to obfuscate the lower case letters that are copied to an
#output file
#Will Gardner 2017


def getLines(path):
    OGFile=open(path,'r')
    fileLines=[word.strip() for word in OGFile.readlines()]
    OGFile.close()
    return fileLines

def encrypt(line):
    retLine = line
    cipher = getCipher()
    for ciphItem in list(cipher.items()):
        retLine = retLine.replace(ciphItem[0],ciphItem[1])
    return retLine
def getCipher():
    cipher = {
            'a': '%',
            'b': '1',
            'c': '$',
            'd': '2',
            'e': '?',
            'f': '9',
            'g': '|',
            'h': ')',
            'i': '4',
            'j': '!',
            'l': '(',
            'm': '0',
            'n': '3',
            'o': '=',
            'p': '#',
            'q': '@',
            'r': '7',
            's': '^',
            't': '8',
            'u': '-',
            'v': '_',
            'w': '*',
            'x': '+',
            'y': '<',
            'z': '>'}
    return cipher

def main():
    print("Encryption Program")
    ogPath="state_capitals.txt"
    newFilePath="encrypted.txt"
    fileLines = getLines(ogPath)
    newFile=open(newFilePath,'w+')
    for line in fileLines:
        newFile.write(encrypt(line)+"\n")
    print("File " + newFilePath + " crypted and created.")

if __name__ == "__main__":
    main()
