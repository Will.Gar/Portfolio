import tkinter as tk
import tkinter.messagebox
import copy

class Service:
    def __init__(self,name,price):
        self.n=name #name of service
        self.p=price #price of service

def serviceList():
    retList = []
    retList.append(Service("Oil Change",30.))
    retList.append(Service("Lube Job",20.))
    retList.append(Service("Radiator Flush",40.))
    retList.append(Service("Transmission Flush",100.)) 
    retList.append(Service("Inspection",35.)) 
    retList.append(Service("Muffler Replacement",200.))
    retList.append(Service("Tire Rotation",20.))
    return retList


class AutoShop:
    def __init__(self,sL):
        #main window
        self.sL=sL #service list
        self.main_window = tk.Tk()
        #frames
        self.top_frame = tk.Frame(self.main_window)
        self.bottom_frame= tk.Frame(self.main_window)
        
        #create intVar list
        self.cbVarList=[]
        #checkbox list
        self.cbList=[]
        self.cbFrameList=[]
        #bring it together
        for i in range(len(self.sL)):
            self.cbVarList.append(tk.IntVar())
            self.cbVarList[i].set(0)
            self.cbFrameList.append(tk.Frame(self.main_window))
            self.cbList.append(tk.Checkbutton(self.cbFrameList[i],text=(self.sL[i].n+'--'+'${:,.2f}'.format(self.sL[i].p)),variable=self.cbVarList[i]))
            self.cbList[i].pack(side='left')
            self.cbFrameList[i].pack()
        
        #ok/quit buttons
        self.ok_button = tk.Button(self.bottom_frame,text='OK',command=self.calculate_Choice)
        self.quit_button = tk.Button(self.bottom_frame,text='Quit',command=self.main_window.destroy)
        self.ok_button.pack(side='left')
        self.quit_button.pack(side='left')

        #packing frames
        self.top_frame.pack()
        self.bottom_frame.pack()
        
        #main
        tk.mainloop()

    def calculate_Choice(self):
        self.message='Total:'
        total=0.
        for i in range(len(self.sL)):
            total+=(self.sL[i].p*self.cbVarList[i].get())
        self.message+=('${:,.2f}'.format(total))
        tkinter.messagebox.showinfo('TOTAL',self.message)

def main():
    sL=serviceList()
    gui = AutoShop(sL)

if __name__=='__main__':
    main()
