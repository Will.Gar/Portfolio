# cashMachine - prints out total order base on desired tip and food charg
# Will Gardner 9/16/2017 CPT 101

tax=.08 #Tax on food
foodCharge=0. #Total food charge
tip=0. #Desire tip percentage


## Intro
print("Welcome to the cash register.")
##input gets
foodCharge = float(input("Enter the charge on food:"))
tip = float(input("Enter the desired tip percentage:"))
##output
print('The total, with a $', format(((tip * foodCharge)/100),".2f"), 'tip, is $',format(((tip * foodCharge)/100) + (1.0+tax)*foodCharge,".2f"))

