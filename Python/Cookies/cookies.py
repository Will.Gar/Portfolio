#cookies.py -- divide amount by base and applies that ratio to ingredients

#variables
base=48. #base amount of cookies for recipe
sugar=1.5 	#base sugar for recipe
butter=1.  #base butter for recipe
flour=2.75  #base flour for recipe
ratio=1.  # ration for amount/base

#input
amount=float(input("enter desired amount of resulting cookies:"))
#calculations
ratio=amount/base

print("Cookie Recipe!")
print("Sugar: ", sugar * ratio, "cups")
print("Butter: ", butter * ratio, "cups")
print("Flour: ", flour * ratio, "cups")
