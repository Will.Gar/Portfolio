#pBNumbers.py - search for the ten most common number sequences throughout a list of powerball numbers

import json as j

# writeToFile write input data to file
# input-    path-path of file to write to
#           name-string representing player name
#           score-integer representing score
def writeToFile(path,frequency,sequence):
    w = open(path,'a')
    sequenceStat = {'frequency':frequency,'sequence':sequence}
    j.dump(sequenceStat,w)
    w.write("\n")
    


#creates a matrix of powerball scores
#path - path of file to read from
#returns list of a sequence of numbers
def createList(path):
    retList=[]
    f=open(path)
    rawList = f.readlines()
    #each line of the raw list
    for line in rawList:
        tempElem=[]
        tempList=line.split()
        #convert each elem to to int
        for elem in tempList:
            tempElem.append(int(elem))
        retList.append(tempElem)
    return retList


#frequencyMatrix- create a list with one colomn with index+1 and 2nd zero filled up to x amount of rows
def frequencyMatrix(x):
    retList=[]
    for i in range(1,x+1):
        retList.append([i,0])
    return retList

#counts frequency of each possible number in sequence matrix
def freqAnalysis(sequenceMatrix,frequencyMatrix,place):
    
    for i in range(len(frequencyMatrix)):
        for elemB in sequenceMatrix:
            if frequencyMatrix[i][0]==elemB[place]:
                frequencyMatrix[i][1]+=1


#detirmines top 10ish frequent number used at a particular sequence and snips the rest
def freqSnip(frequencyMatrix,maxNum):
    sortedFrequency=sorted(frequencyMatrix,key=lambda x: x[1],reverse=True)
    for i in range(maxNum):
        snippedList.append(sortedFrequency[i])
    n=maxNum
    while (snippedList[-1][1]==sortedFrequency[n][1]):
        snippedList.append(sortedFrequency[n])
        n+=1
    return snippedList

#arrSnip - snip non essentials
def arrSnip(rawList,frequencyList,maxNum):
    retlist=[]
    for i in range(len(frequencyList)):
        for elem in rawList:
           if frequencyList[i][0] == elem[0]:
                retlist.append(elem)
def arrCollate(rawList,freqList,place):
    retList=[]
    arrElem=[]
    for elem in sorted(rawList,key=lambda x: x[place]):
        arrElem.append(elem)



def main():
    #import list
    path="pbNumbers.txt"
    rawList=createList(path)
    #loop through sequence number
    for i range(len(rawList[0])):
        #check if last number
        if (i == len(rawList[0])-1):
                #if yes
                #genfreq=26
                freqMat=frequencyMatrix(26)
            #if not
        else:
                #genFreq=69
                freqMat=frequencyMatrix(69)
        #first loop
        if (i==0):
            #sorted List
            sortedList=sorted(rawList,key=lambda x:x[0])
            #check frequency in sequence matrix
            freqAnalysis(rawList,freqMat,i)
            #snip frequency matrix
            freqSnip(freqMatrix,10) 
            #sort and snip non essentials and collate like amount
        #second through fifth loop
            #generate frequency matrix count for each element           
            #snip frequencey for each element
            #sort list element
            #collate like amount
            #sort all element by len of inner take top ten ish
        

