#pbNumbersUpdated - count the frequency of powerball numbers
import copy as c

def getFileInput(path):
	file=open(path,'r')
	numberList=[]
	tempList=[]
	for elem in file.readlines():
		tempList = []
		for elem2 in elem.split():
			tempList.append(int(elem2))
		numberList.append(c.deepcopy(tempList))
	#print (numberList)
	return numberList
	

def getFrequencyList(numList):
	freqMat = []
	for elem in numList:
		for elem2 in elem:
			match=False
			for i in range(len(freqMat)):
				if elem2==freqMat[i][0]:
					freqMat[i][1]+=1
					match=True
					i=len(freqMat)
			if (not match):
				tempList=[]
				tempList.append(elem2)
				tempList.append(1)
				freqMat.append(c.deepcopy(tempList))
	freqMat=sorted(freqMat,key=lambda x: x[1], reverse=True)
	return freqMat
def main():
	path = "pbnumbers.txt"
	numList=getFileInput(path)
	freqMat=getFrequencyList(numList)
	print("the top 10 number frequencies are:")
	for i in range(10):
		print (str(freqMat[i][0]) + " with " + str(freqMat[i][1]) + " occurrences.")
		
if __name__ == "__main__":
	main()
