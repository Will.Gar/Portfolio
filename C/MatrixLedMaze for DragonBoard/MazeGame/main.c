// Example: Turn on every other led
#include <hidef.h>      /* common defines and macros */
#include <mc9s12dg256.h>     /* derivative information */
#include "queue.h"
#pragma LINK_INFO DERIVATIVE "mc9s12dg256b"

#include "main_asm.h" /* interface to the assembly module */


#define uchar unsigned char
#define uint  unsigned int
//globs
unsigned char matrix[8];
char c;
long int i,j,val,thing,piece;
int pause=0;
int menu=0;
const int startX=10;
const int startY=0;
int maxCount = 2000;
int count=0;
int currentX=10;
int currentY=0;
int nB,eB,sB,wB = 0;
int move=0;
int mapper=0;
int time=6;
int timer=6;
int health=5;
int score=0;
int modifier=1;
int moveButtonPressed=0;
char c;
const sizeX=35,sizeY=35;
const char maze[35][36]={
  "++++++++++S++++++++++++++++++++++++",
  "++++++++++OOOOOOOOOOOOOOOOOOOOOOOOO",
  "++++++++++O++++++++++++++++++O+++++",
  "OOOOOOOOOOOOOOOOOOOOOOOOOO+++O++OOE",
  "O+++++++++O++++++++++++++O+++O++O++",
  "OOOOOO++++O++++++++++++++O+++O++O++",
  "O++++O++++OOOOOOOOOOO++++O+OOO++O++",
  "O++++O++++O+++++++++OOOO+O+++O++O++",
  "OOO++OOOO+OOOOOO+++++++++++OOO++OOO",
  "O+O+++++O++++++OOOOOOOOOO++O++++++O",
  "O+OOOO++O++++++O++++++++O+++OOO+++O",
  "O++++O++OOOOOOOO++++++++O+++O+O+++O",
  "OOO++O++++++++++++++++++OOOOO+O+++O",
  "++O++OOOOOOOOOOOOOOOO+++++++++OO++O",
  "OOO+++++++++++++++++OOOOOO++++++++O",
  "O++++++++++++++++++++++++O++OOOOOOO",
  "+++++++++++++++++++++++++O++O++++++",
  "OOOOOOOOOOOOOOOOOOOOOOOOOO++OOOOO++",
  "O++++++++++++++++++++++++O++++++O++",
  "OOOOOOO+++++++++++++++OOOOOOO+++O++",
  "++++++++++++++++++++++O+++++OO++O++",
  "OOOOOOOOOOOOOOOOOOOOOOO++++++O++O++",
  "O++++++++++++++++++++++++++++O+OOOO",
  "OOOO++++++++++++++++++++OOOOOO+O+++",
  "+++OOOOOOOOO+++++++++++++++++++O+++",
  "+++++O+++++OOOOOOOOOO++++++++OOO+++",
  "+O+++OOOOO++++++O++++++++++++O+++++",
  "+OOOOO+++O++++++OOOOOO+++++++O+++++",
  "+++++++++++++++++++++OOOOOOOOO+++++",
  "OOOOOOOOOOOOOOOOOOOOOO+++++++++++++",
  "+++++++++++++++++++++++++++++++++++",
  "+++++++++++++++++++++++++++++++++++",
  "+++++++++++++++++++++++++++++++++++",
  "+++++++++++++++++++++++++++++++++++",
  "+++++++++++++++++++++++++++++++++++"};

void interrupt 20 handler(){
  mapper = 1;
  SCI0SR1;
  SCI0DRL;  
}


void interrupt 56 handlerus(){
  if((PIFP & 0x80)==0x80) {
    pause = pause ^ 1;
  }
  
  PIFP = 0xFF;
 } 
//writes byte to matrix
//void interrupt 7 handler(){
 //if ((count <  maxCount)&(health>0)) {
 // count++;
 //}else{
  //mapper=1;
  //count=0;
 //}
 //clear_RTI_flag();
  //}
//}
void Write_M7219_byte(uchar DATA)         
{    
		  //PORTB=(PORTB&0b11111110); // CS Bit low		
	    //for(i=8;i>=1;i--)
          //{		  
            //PTP &= 0b11111011;//CLK=0;
       c=send_SPI1(DATA);
            //PTP |= 0b000001000;//Clk=1;
           //}                                 
}
//writes data to matrix at address
void Write_M7219(uchar address,uchar dat)
{ 
  PORTB=(PORTB&0b11111110); // CS Bit low
	 Write_M7219_byte(address);
   Write_M7219_byte(dat);
   PORTB|=0b00000001; // CS Bit High                        
}
//Initialize code IMPORTANT
void Init_M7219(void)
{
 Write_M7219(0x09, 0x00);       //decoding :BCD
 Write_M7219(0x0a, 0x03);       //brightness 
 Write_M7219(0x0b, 0x07);       //scanlimit;8 LEDs
 Write_M7219(0x0c, 0x01);       //power-down mode:0,normal mode:1
 Write_M7219(0x0f, 0x00);       //test display:1;EOT,display:0
}
//Update matrix based on matrix array
void updateMatrix(void) {
   for(i=1;i<9;i++) { 
      Write_M7219(i,matrix[(i-1)]);//DATA
   }
}
//clear matrix
void clearMatrix(void) {
  for(i=0;i<8;i++){
    matrix[i]=0x00;
  }
}
int direction(int x,int max){
    piece = max/4;
    if ( x <= (3*piece)) {
        if (x <=(2*piece)){
           if (x<=piece) {
              return 0; //N
           } else{
            return 1; //W
           }
        } else {
          return 2;//S
        }
    }else{
      return 3;//E
    }
}


void matrixDirection(int dir) {
  //N
  if (dir==0) {
    //matrix[0]|=0x00;
    //matrix[1]|=0x00;
    //matrix[2]|=0x00;
    matrix[3]|=0x60;
    matrix[4]|=0x60;
    //matrix[5]|=0x00;
    //matrix[6]|=0x00;
    //matrix[7]|=0x00;
  }
  //W
  if (dir==1) {
    //matrix[0]|=0x00;
    //matrix[1]|=0x00;
    //matrix[2]|=0x00;
    //matrix[3]|=0x00;
    //matrix[4]|=0x00;
    matrix[5]|=0x18;
    matrix[6]|=0x18;
    //matrix[7]|=0x00;
  }
  //S
  if (dir==2) {
    //matrix[0]|=0x00;
    //matrix[1]|=0x00;
    //matrix[2]|=0x00;
    matrix[3]|=0x06;
    matrix[4]|=0x06;
    //matrix[5]|=0x00;
    //matrix[6]|=0x00;
    //matrix[7]|=0x00;
  }
  //E
  if (dir==3) {
    //matrix[0]|=0x00;
    matrix[1]|=0x18;
    matrix[2]|=0x18;
    //matrix[3]|=0x00;
    //matrix[4]|=0x00;
   // matrix[5]|=0x00;
    //matrix[6]|=0x00;
    //matrix[7]|=0x00;
  }
}
void matrixBounds(void){
  if (nB==1){
    matrix[0]|=0x80;
    matrix[1]|=0x80;
    matrix[2]|=0x80;
    matrix[3]|=0x80;
    matrix[4]|=0x80;
    matrix[5]|=0x80;
    matrix[6]|=0x80;
    matrix[7]|=0x80;
  }
  if (wB==1){
    matrix[7]=0xFF;
    //matrix[6]=0xFF;
  }
  if (sB==1){
    matrix[0]|=0x01;
    matrix[1]|=0x01;
    matrix[2]|=0x01;
    matrix[3]|=0x01;
    matrix[4]|=0x01;
    matrix[5]|=0x01;
    matrix[6]|=0x01;
    matrix[7]|=0x01;
  }
  if (eB==1){
    matrix[0]=0xFF;
  }
}
int boundsCheck(int dir){
  if (dir==0) {
    return nB;
  }
  if (dir==1){
    return wB;
  }
  if (dir==2) {
    return sB;
  }
  if (dir ==3){
    return eB;
  }
  
    
    
}
void movement(int dir){
  int validMove = boundsCheck(dir);
  if (validMove==0) {
    if (dir==0) {
      currentY-=1;
    }
    if(dir==1) {
      currentX-=1;
    }
    if(dir==2){
      currentY+=1;
    }
    if(dir==3){
      currentX+=1;
    }
    score+=modifier;
    
  }else {
    health-=1;
  }
  
}
void checkBounds(){
  if ((currentX==0)|(maze[currentY][(currentX-1)]=='+')) {
    wB=1;
  }else{
    wB=0;
  }
  if ((currentY==0)|(maze[(currentY-1)][(currentX)]=='+')) {
    nB=1;
  }else{
    nB=0;
  }
  if ((currentX==sizeX-1)|(maze[currentY][(currentX+1)]=='+')) {
    eB=1;
  }else{
    eB=0;
  }
  if ((currentY==sizeY-1)|(maze[currentY+1][(currentX)]=='+')) {
    sB=1;
  }else{
    sB=0;
  }
}
void map(void){
  for (i=0;i<sizeY;i++) {
    for(j=0;j<sizeX;j++){
      if((i==currentY)&(j==currentX)){
        outchar0('C');
      }else{
        outchar0(maze[i][j]);
      }
      if(j==sizeX-1){  
        outchar0('\r');
        outchar0('\n');
      }
    }
  }
}
void main(void) {
  /* put your own code here */
  PLL_init();
  DDRB  = 0x0f;
  DDRA  = 0x00;
  DDRP  = 0x7F;
  PIEP = 0x80;
 
  ad0_enable();
  lcd_init();
  SPI1_init();
  //RTI_init();
  //initq();
  //asm(cli);
  SCI0_int_init(9600);
  Init_M7219();
  asm(CLI);
  //set_lcd_addr(0x40);
        // set system clock frequency to 24 MHz 
         // Port B lower nibble is output
  // turn on every other led
   
  while(1) { 
    while (menu==0) {      
      while (health>0){
        while (pause==1) {
          set_lcd_addr(0x00);
          type_lcd("PAUSE MODE, Fool?");
          set_lcd_addr(0x40);
          type_lcd("Aces dont pause");
          ms_delay(100);
          if (pause ==0) {
            clear_lcd();
          }
          }
        clearMatrix();
        val = ad0conv(7);
        //write_int_lcd(val);
        val*=1000;
        val/=1024;
        val = val >> 1;
        
        set_lcd_addr(0x00);
        type_lcd(HP:)
        set_lcd_addr(0x03);
        write_int_lcd(health);
        set_lcd_addr(0x09);
        type_lcd("
        write_int_lcd(timer);
        thing=direction(val,500);
        set_lcd_addr(0x40);
        write_int_lcd(score);
        matrixDirection(thing);
        checkBounds();
        matrixBounds();  
        updateMatrix();
        if(maze[currentY][currentX]=='E') {
          currentX=startX;
          currentY=startY;
          modifier+=1;
        }
        if (mapper==1){
          mapper=0;
          map();
          timer= 5;
          //map();
          
        }
        if(timer==0){
          movement(thing);
          timer=time-modifier;
          move=0;
        
        }
        ms_delay(100);
        timer-=1;
      }
     set_lcd_addr(0x00);
     type_lcd("Final Score:");
     //if (qempty()!=1){
      //c=getq();
     // map();
     }
    }
  }
  //for(;;) {} /* wait forever */

